/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/* Just add the config variable needed in that file */

/* In this folder the following files are not created, using the default configuration of GREZI (if you need customization, create them) :
	- title.js
	- js/_data_typing.js
	- img/card.webp
*/

var graphVersion = "V_X_X_X";

var templateVersion = "V_X_X_X";

var featuresGraph = {
	"featureSearchBar": true
};

var featuresButtons_FORCING = {
	"toggle_modeEditor": "",
	"toggle_summary": ""
};

var dataGraph_GET = {"csv": [{"data": "data/_template.csv", "config": "data/_template.json"}]};

var dataGraph_GET_ALTERNATE = { /* An alternative to set data : an objet with nodes and edges declared separately => Rename in "dataGraph_GET" and remove the previous one if you want to test it */
	"csv": [ 
		{"nodes": "data/_templateAlternate_Nodes.csv", "config": "data/_templateAlternate_Nodes.json"},
		{"edges": "data/_templateAlternate_Edges.csv", "config": "data/_templateAlternate_Edges.json"} /* If no config files are set for the edges, "from" and "to" are amndotories */
	]
};