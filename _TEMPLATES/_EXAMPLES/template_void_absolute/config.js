/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/* Just add the config variable needed in that file */

/* In this folder the following files are not created, using the default configuration of GREZI (if you need customization, create them) :
	- title.js
	- js/_data_typing.js
	- img/card.webp
*/

var graphVersion = "V_X_X_X";

var templateVersion = "V_X_X_X";