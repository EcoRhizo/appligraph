/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

var graphVersion = "V_X_X_X";

var templateVersion = "V_X_X_X";

var featuresGraph = {
	"animation": false,
	"sound": false,
	"physics": false,
	"static": false,
	"compaction": 1,
	"padding": 50,
	"fcoseOnLoad": false,
	"editorOnLoad": false,
	"mapOnLoad": false,
	"featureReload": false,
	"featureClustering": false,
	"featureSearchBar": false,
	"tooltips": false,
	"modalSelection": false,
	"infoBubbles": false,
	"infoTags": false,
	"backgroundCanvas": "",
	"defaultIcons": true,
	"customIcons": {
		"page_icon": "",
		"page_title": ""
	},
	"initialClustering": [],
	"description": "HTML_DESCRIPTION"
};

var featuresGraph_SearchBarOptions = {
	"zoom": false,
	"selected": false,
	"and": false,
	"or": false
};

var featuresColors_FORCING = {
	"--main_color_01": "",
	"--main_color_02": "",
	"--main_color_03": "",
	"--main_color_04": "",
	"--main_color_05": "",
	"--main_color_01_faded": "",
	"--main_color_03_faded": "",
	"--main_color_node": "",
	"--main_color_editor_01" :"",
	"--main_color_editor_02" :""
};

var featuresColors_FILTER = "";

var featuresButtons_FORCING = {
	"toggle_fullscreen": "",
	"toggle_graphs": "false",
	"toggle_information": "",
	"toggle_map": "false",
	"toggle_legend": "false",
	"toggle_tags": "false",
	"toggle_stabilization": "false",
	"toggle_physics": "false",
	"toggle_fcose": "false",
	"toggle_ori_pos": "false",
	"toggle_ind_link": "false",
	"toggle_highlighter": "false",
	"toggle_lateralScreen": "false",
	"toggle_modeEditor": "false",
	"toggle_summary": "false",
	"toggle_save": "false",
	"toggle_bug": ""
};

var featuresLoading_FORCING = {
	"eulerComputation": "",
	"showInformation": "false",
	"clickToBegin": "",
	"debug": "false",
	"blank": "false"
};

var scriptGraph_GET = [];

var viewsButtons = [];

var options_Modifier = [];

var options_Groups = [];

var dictColorsTags_custom = {};

var dataGraph_LEGEND = "";

var dataGraph_POSITIONS = {};

var dataGraph_GET = [];