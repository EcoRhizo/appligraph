/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

var graphVersion = "V_X_X_X";

var templateVersion = "V_X_X_X";

var featuresGraph = {
	"animation": false, /* activate the animation based on the _data_typing.js script */
	"sound": false, /* activate the sound module of the app */
	"physics": false, /* activate the physics engine of the graph (except for hierarchical layout) (if no positions are set, perform a cola layout) */
	"static": false, /* lock all the nodes (even if the physics option is activated) */
	"compaction": 1, /* set the compaction of the graph when physics is activated or when a cola layout occurs */
	"padding": 50, /* set the padding when a zoom is activated by double-tap of the stabilization button */
	"fcoseOnLoad": false, /* launch a fcose layout onload of the graph */
	"editorOnLoad": false, /* Open the editor mode onload of the graph */
	"mapOnLoad": false, /* Toggle the cartography onload of the graph */
	"featureReload": true, /* show or not the button to reload the graph at his opening state */
	"featureClustering": true, /* allow the user to cluster the nodes following the parents nodes with group all button and the contextual menu */
	"featureSearchBar": true, /* show or not the search bar */
	"tooltips": true, /* show the title attribute of the nodes in a tooltip (if titleComplete is set and title undefined, the first will be show */
	"modalSelection": true, /* show the titleComplete attribute of the nodes in a modal screen */
	"infoBubbles": true, /* create bubbles nodes around the nodes that have tooltips or info modal screen */
	"infoTags": true, /* create tags nodes based on the searchTag attribute of the nodes (and an activation button and complete the legend of the graph */
	"backgroundCanvas": "IMG_PATH", /* show a background image */
	"defaultIcons": true, /* make the EcoRhizo appear in the different modal screens */
	"customIcons": {
		"page_icon": "", /* image .ico of the page */
		"page_title": "" /* custom title of the page */
	},
	"initialClustering": [], /* cluster the nodes on load based of the groups list in this parameter */
	"description": "HTML_DESCRIPTION" /* description that appears in the information modal screen of the graph */
};

var featuresColors_FORCING = { /* modify the colors of the graph */
	"--main_color_01": "",
	"--main_color_02": "",
	"--main_color_03": "",
	"--main_color_04": "",
	"--main_color_05": "",
	"--main_color_01_faded": "",
	"--main_color_03_faded": "",
	"--main_color_node": "",
	"--main_color_editor_01" :"",
	"--main_color_editor_02" :""
};

var featuresColors_FILTER = ""; /* custom colorization of the images of the buttons (if featuresColors_FORCING is different of default, the console will produce the appropriate value to associate with this variable */

var featuresButtons_FORCING = { /* if "false" is set to the attributes of this dict, it will force to desactivate the association button. To see the functions linked to these buttons, look at the information modal screen of the graph */
	"toggle_fullscreen": "",
	"toggle_graphs": "",
	"toggle_information": "",
	"toggle_map": "",
	"toggle_legend": "",
	"toggle_tags": "",
	"toggle_stabilization": "",
	"toggle_physics": "",
	"toggle_fcose": "",
	"toggle_ori_pos": "",
	"toggle_ind_link": "",
	"toggle_highlighter": "",
	"toggle_lateralScreen": "",
	"toggle_modeEditor": "",
	"toggle_summary": "",
	"toggle_save": "",
	"toggle_bug": ""
};

var featuresLoading_FORCING = { /* force the url parameters to be "true" or "false" regardless of the url paramets that are set */
	"eulerComputation": "",
	"showInformation": "",
	"clickToBegin": ""
};

var scriptGraph_GET = []; /* list of the customs scripts to load */

var viewsButtons = [ /* list of custom views button to create */
	{
		"title": "ELEMENTS",
		"img": "IMG_PATH",
		"id": "title_00",
		"selector": "[name=value]",
		"groups": "element",
		"hierarchical": false
	}
];

var options_Modifier = []; /* option of the graph to be modified (see the cytoscape documentation to see those options) */

var options_Groups = [ /* set the style to apply on the nodes and edges, espcacially to custom the differents groupes of nodes (see the cytoscape documentation to see those options) */

	{
		"selector": "node[group = " + '"' + "elementCOMPOUND" + '"' + "]",
		"style": {
			"shape": "round-rectangle",
			"background-opacity": 0.5,
			"border-width": "0px",
			"text-max-width": "400px",
			"text-halign": "center",
			"text-valign": "top"
		}
	},

	{
		"selector": "node[group = " + '"' + "elementCENTRAL" + '"' + "]",
		"style": {
			"width": "30px",
			"height": "30px",
			"color": "red",
			"text-valign": "bottom",
			"text-margin-y": "3rem",
			"background-image": "data(image)",
			"background-fit": "cover",
			"background-clip": "node"
		}
	},
	
	{
		"selector": "node[group = " + '"' + "element" + '"' + "]",
		"style": {}
	},
	
	{
		"selector": "edge[group = " + '"' + "flux_white_SEGMENTS" + '"' + "]",
		"style": {
			"curve-style": "segments",
			"line-color": "red",
			"target-arrow-color": "red",
			"target-arrow-shape": "triangle",
			"width": "6rem"
		}
	}

	
];

var dictColorsTags_custom = {"tag_1": "#c9c9c9", "tag_2": "#079fd5"}; /* custom colors for tags */

var dataGraph_LEGEND = ""; /* html written legend */
dataGraph_LEGEND += "<p>"+"<svg width=\"45\" height=\"30\"><polygon points=\"0,30 30,30 45,15 30,0 0,0\"  style=\"fill:"+"#fcc300"+"\"/></svg>"+" : "+"Legend_A"+"</p>";
dataGraph_LEGEND += "<p>"+"<svg width=\"45\" height=\"30\"><polygon points=\"0,30 30,30 45,15 30,0 0,0\"  style=\"fill:"+"#079fd5"+"\"/></svg>"+" : "+"Legend_B"+"</p>";
dataGraph_LEGEND += "<p>"+"<svg width=\"45\" height=\"30\"><polygon points=\"0,30 30,30 45,15 30,0 0,0\"  style=\"fill:"+"#ffffff"+"\"/></svg>"+" : "+"Legend_C"+"</p>";

var dataGraph_POSITIONS = { /* dictionnary of preset node positions (also set the height and width for the compounds) */
	"compound_00": {
		"x": 0,
		"y": 0,
		"height": 700,
		"width": 1000
	},
	"title_00": {
		"x": 0,
		"y": 0
	},
	"elem_00": {
		"x": 200,
		"y": 200
	}
};

var dataGraph_GET = [ /* list of the nodes with their configuration options */

	/*
		Keep in mind that you can also use this variable with :
			* A string containing a url of a php file that sending a GREZI formated nodes list description,
			* A dictionnary of different format (only "csv" is working for now). For each format, there is a list of files,
			* A dictionnary of a unique element with **___CUSTOM_TEMPLATES___** for the key and a config file in the element (that can be empty). It loads an upload file menu when GREZI loads the template,
			* A string equal to **___PASSWORD_TEMPLATES___** that load the password file menu when GREZI loads the template.
	*/

	{
		"id": "compound_00",
		"group": "elementCOMPOUND",
		"label": "COMPOUND",
		"style": {"text-margin-y": "-10px", "text-max-width": "600px"},
		"sourcesNodes": []
	},

	{
		"id": "title_00",
		"lat": 43.59594438557529,
		"lng": 1.4525441351628603,
		"group": "elementCENTRAL",
		"label": "TITLE",
		"title": "TITLE",
		"titleComplete": "TITLE_COMPLETE",
		"other": "Any other custom object",
		"image": "IMG_PATH",
		"selectable": false,
		"grabbable": false,
		"pannable": false,
		"insensitiveNode": false,
		"parent": "compound_00",
		"sourcesNodes": [],
		"sourcesNodesIndirect": []
	},
	
	{
		"id": "elem_00",
		"lat": 43.605360942883834,
		"lng": 1.4240912610897487,
		"group": "element",
		"label": "LABEL",
		"searchTag": {"tag_1": ["value_1"], "tag_2": ["value2_1","value2_2"]},
		"sourcesNodes": [
			"title_00",
			{"id": "title_00", "data": {"group": "flux_white_SEGMENTS"}, "style": {"segment-distances": [100], "segment-weights": [0.55]}}
		]
	}
	
];

var dataGraph_GET_ALTERNATE = { /* An alternative to set data : an objet with nodes and edges declared separately => Rename in "dataGraph_GET" and remove the previous one if you want to test it */

	"nodes": [

		{
			"id": "compound_00",
			"group": "elementCOMPOUND",
			"label": "COMPOUND",
			"style": {"text-margin-y": "-10px", "text-max-width": "600px"}
		},

		{
			"id": "title_00",
			"lat": 43.59594438557529,
			"lng": 1.4525441351628603,
			"group": "elementCENTRAL",
			"label": "TITLE",
			"title": "TITLE",
			"titleComplete": "TITLE_COMPLETE",
			"other": "Any other custom object",
			"image": "IMG_PATH",
			"selectable": false,
			"grabbable": false,
			"pannable": false,
			"insensitiveNode": false,
			"parent": "compound_00"
		},
		
		{
			"id": "elem_00",
			"lat": 43.605360942883834,
			"lng": 1.4240912610897487,
			"group": "element",
			"label": "LABEL",
			"searchTag": {"tag_1": ["value_1"], "tag_2": ["value2_1","value2_2"]}
		}

	],

	"edges": [

		{
			"edge_id": "edge_0000", /* Optional edge id */
			"from": "elem_00",
			"to": "title_00"
		},

		{
			"from": "elem_00",
			"to": "title_00",
			"data": {"group": "flux_white_SEGMENTS"},
			"style": {"segment-distances": [100], "segment-weights": [0.55]}
		}

	]

	
};