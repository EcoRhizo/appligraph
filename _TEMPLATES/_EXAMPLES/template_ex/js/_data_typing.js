/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

var animationOnLaunch = false; /* if true, launch the animation when the app is launched (false by default) */

var animationCredentials = {
	"activate": false, /* if true, show credentials on launch of the script with a default "Powered by EcoRhizo" (false by default) */
	"title": "", /* Custom title of the credentials */
	"img": "", /* Custom image of the credentials */
};

var possibilityToFullScreen = true; /* show the fullscreen button of the animation (true by default) */

var possibilityToPass = true; /* show the pass button of the animation (true by default) */

var possibilityToSuspend = true; /* show the suspend button of the animation (true by default) */

var dictIntro = [ /* list of block strings to deploy in the talking frame. Each block can be associated with some options */

	{
		"img": "", /* OPTIONAL : image to show in the talking frame */
		"title": "", /* OPTIONAL : title to show in the talking frame */
		"isFadeOut": false, /* OPTIONAL : if false, it will type back before go to the next sentence (true by default) */
		"pauseOncomplete": true, /* OPTIONAL : if true it will wait for a user interaction before go to the next sentence (false by default) */
		"suspendOnComplete": true, /* OPTIONAL : if true, stop the animation at this point and begin on the next step when the launch button will be press the next time (false by default) */
		"onBeginInstructions": "", /* OPTIONAL : some code to apply at the beginning of this block strings */
		"onCompleteInstructions": "", /* OPTIONAL : */
		"strings_to_type": [ /* MANDATORY : list of the sentences to deploy in the talking frame */
			"What you want to say.",
			"Set some ^1000pause.",
			"And 'bulk typing'"
		]
	}

];