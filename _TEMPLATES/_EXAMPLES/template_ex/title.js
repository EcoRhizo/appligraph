/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

var title_GRAPH = "TITLE"; /* title that appears in the graph list of the app */

var subtitle_GRAPH = "SUBTITLE"; /* description that appears in the graph list of the app */
