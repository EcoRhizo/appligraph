<?php

	/*
		* Copyright : GREZI
		* Author : Adrien Solacroup
		* Encoding : UTF-8
		* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
		* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
	*/

	/*
		* Crawl '_TEMPLATES' folder and outputs the path of all template folders with a coma sepator
		* An optional variable, 'f' limit the crawling on a specifi subfolder of '_TEMPLATES' folder
		* e.g. : '../_graph_data.php?f=_EXAMPLES' => '_EXAMPLES\template_csv,_EXAMPLES\template_ex,_EXAMPLES\template_password,_EXAMPLES\template_upload,_EXAMPLES\template_void,_EXAMPLES\template_void_absolute'
		* N.B. : This script is used in debug mode of grezi (not in a standalone mode) to retrieve all the templates in a row
	*/
	
	// To be modified to allow just the grezi domain
	
	// Allow from any origin
	if (isset($_SERVER['HTTP_ORIGIN'])) {
		// should do a check here to match $_SERVER['HTTP_ORIGIN'] to a
		// whitelist of safe domains
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
	};

	// Access-Control headers are received during OPTIONS requests
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
			header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
			header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	};
	
	$f = $_REQUEST["f"];
	if ($f === "none") {
		$f_bis = "../_TEMPLATES";
	} else {
		$f_bis = "../_TEMPLATES" . "/" . $f;
	};
	
	$g_output = "";
	
	$g_path = "";
		
	$it = new RecursiveTreeIterator(new RecursiveDirectoryIterator($f_bis, RecursiveDirectoryIterator::SKIP_DOTS));
	
	foreach($it as $path) {
		if (preg_match('/\\\\config\.js$/',$path)) {
			$g_path = substr($path,strpos($path,'_TEMPLATES')+11);
			$g_path = str_replace("\\config.js","",$g_path);
			$g_path = str_replace("/config.js","",$g_path);
			$g_output = $g_output . $g_path . ",";
		};
	};
	
	$g_output = substr($g_output,0,-1);
	
	echo $g_output;
	
?>