<?php

	/*
		* Copyright : GREZI
		* Author : Adrien Solacroup
		* Encoding : UTF-8
		* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
		* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
	*/

	/*
		* Send a mail using webmail servor and output a confirmation message as a string
		* Recipiend and headers are hard coded in the following variables : 'to', 'headers'
		* Subject and message are set with the following variables : 'page_title', 'form_report'
		* If 'form_report' is not send, an error message the script outputs an error message as a string
		* N.B. : 'page_title' corresponds to the template name and is automatically set by grezi
	*/
	
	echo "<html><body style=\"font-family:Arial;letter-spacing:0.0625em;\">";

	error_reporting(0);
	
	$to = "contact@grezi.fr";
	/* $to = "incoming+ecorhizo-appligraph-14344801-issue-@incoming.gitlab.com"; */
	
	$subject = "[BUG_REPORT]" . " " . $_POST["page_title"];
	
	$message = $_POST["form_report"];

	$headers = "From: contact@grezi.fr \r\n" . "Content-type: text/html; charset=UTF-8 \r\n";
	
	if (empty($message)) {
		
		echo "<p>Veuillez renseigner la description des dysfonctionnements observés.</p>";
		
	} else {
		
		mail($to, $subject, $message, $headers);
		
		echo "<p>Dysfonctionnement envoyé. Nous ferons de notre mieux pour améliorer l'application.</p>";
		
	};
	
	echo "</body></html>";
	
?>