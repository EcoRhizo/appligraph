<?php

	/*
		* Copyright : GREZI
		* Author : Adrien Solacroup
		* Encoding : UTF-8
		* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
		* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
	*/
	
	/*
		* Output csv data in a string (coma and double quote separators and \n back line) from an url corresponding to the correct password and template list in 'passwd_dict' array
		* Password is set with 'passwd' variable and template with 'template' variable
		* 'passwd' and 'template' variables are mandatories
		* e.g. : '../_password_data.php?passwd=demo&template=_template_ex_php' => col1,col2\n"Hello !","World"\n
		* e.g. : the example above will work if 'passwd_dict' array is set like '$passwd_dict = array ("demo" => array("_template_ex_php","url_to_csv_data"));'
	*/

	$passwd = $_REQUEST["passwd"];
	
	$template = $_REQUEST["template"];
	
	$passwd_dict = array ();
	
	if (array_key_exists($passwd, $passwd_dict)) {
		if (($passwd_dict[$passwd][0] === "no_template") || ($passwd_dict[$passwd][0] === $template)) {
			$output = "";		
			$header = NULL;
			if (($handle = fopen($passwd_dict[$passwd][1], "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
					$num = count($data);
					if(!$header) {
						$header = true;
						for ($c=0; $c < $num; $c++) {
							$output = $output . $data[$c] . ",";
						};
					} else {
						for ($c=0; $c < $num; $c++) {
							$output = $output . '"' . $data[$c] . '"' . ",";
						};
					};
					$output = substr($output,0,-1);
					$output = $output . "\n";
				};
				fclose($handle);
			};
			echo $output;
		} else {
			echo "wrong_template";
		};
	} else {
		echo "error";
	};
	
?>