<?php

	/*
		* Copyright : GREZI
		* Author : Adrien Solacroup
		* Encoding : UTF-8
		* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
		* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
	*/

	/*
		* Crawl '_TEMPLATES' folder and outputs the path of the template folder (set in the 'g' variable with a string)
		* If no folder is found, it will just outputs the string set for the 'g' variable
		* 'g' variable is mandatory
		* e.g. : '../_graph_data.php?g=template_ex' => 'PATH_TO_TEMPLATE_EX\template_ex'
	*/
	
	$g = $_REQUEST["g"];
	
	$g = explode("/",$g);
	
	$g = $g[count($g)-1];
	
	$g = explode("\\",$g);
	
	$g = $g[count($g)-1];
	
	$g_path = "";
		
	$it = new RecursiveTreeIterator(new RecursiveDirectoryIterator("../_TEMPLATES", RecursiveDirectoryIterator::SKIP_DOTS));
	
	foreach($it as $path) {
		if (preg_match('/'.$g.'$'.'/',$path)) {
			$g_path = substr($path,strpos($path,'_TEMPLATES')+11);
		};
	};
	
	if ($g_path == "") {
		echo $g;
	} else {
		echo $g_path;
	};
	
?>