/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
___CUSTOM_GRAPH.JS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
SCRIPT STRUCTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
	* CGS.A : PREPROCESSING
		* CGS.A.01 : Global variables definition
		* CGS.A.02 : Body revealing function
		* CGS.A.03 : Disable some mouse functions
		* CGS.A.04 : Responsive behaviour
		* CGS.A.05 : Alert if not a computer device
	*
	* CGS.B : GET PARAMETERS FROM URL
		* CGS.B.01 : Features buttons to keep
		* CGS.B.02 : Activate or not euler computation
		* CGS.B.03 : Showing the information modal screen or not
		* CGS.B.04 : Click to begin or not
	*
	* CGS.C : MAIN FUNCTIONS
		* CGS.C.01 : Function to seek params in URL
		* CGS.C.02 : Function to resolve a promise after loading a js script
		* CGS.C.03 : Function to load the application with the proper data
		* CGS.C.04 : Functions for the progress bar
	*
	* CGS.D : ON LOAD OPERATIONS
		* CGS.D.01 : Anticipate the colors of the spinner if directId set
		* CGS.D.02 : Firstly check if there is a click to begin before downloading the graph
		* CGS.D.03 : Php dict reading (activated on web)
		* CGS.D.04 : Show an alter if the navigator is incompatible
		* CGS.D.05 : Then load the graph
			* CGS.D.05.01 : Directly load the graph
			* CGS.D.05.02 : Show a graph selection screen
				* CGS.D.05.02.01 : Create the templates button access
				* CGS.D.05.02.02 : Create the preview for each graph
				* CGS.D.05.02.03 : Link the event to load the corresponding graph
			*
		*
	*
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CGS.A : PREPROCESSING
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
CGS.A.01 : Global variables definition
--------------------------------
*/

var greziVersion = "V_X_X_X";

var StandAloneVersion; /* Set a template path to directly show it */

var modulesList = [];

if (typeof(modulesList_AlreadyLoaded) === "undefined") {
	var modulesList_AlreadyLoaded = [];
};

var bar;

var barProgress = 0;

var barStepsNumber = 18;

var idGraph;

var nameGraph;

var idsGraphs;

var idsGraphs_templates;

var directIdGraphOption;

var idGraph_path = "";

var defaultIDS; /* List of templates (comma separation) to show on the main screen */

var editorModeActivated = false;

var alreadyLoaded = false;

var listClusters = {};

var featuresGraph;

var featuresColors_FORCING;

let chain_ScriptInitialisation = Promise.resolve();

let chain_LoadData = Promise.resolve();

let chain_MainLateralScreen = Promise.resolve();

let introSteps;

let introHints;

var idleCursor = "default";

/*
--------------------------------
CGS.A.01 : Blank mode
--------------------------------
*/

if (getParamValue("blank") !== "true") {
	$("#appli_graph_cc").css("display", "flex");
	$("#appli_graph_credentials").css("display", "initial");
	$(".logo_title").css("display", "initial");
} else {
	$(".logo_title").css("display", "none");
};

/*
--------------------------------
CGS.A.02 : Body revealing function
--------------------------------
*/

function revealBody() {
	$("body").css("display", "initial");
};

/*
--------------------------------
CGS.A.03 : Disable some mouse functions
--------------------------------
*/

$("html").bind("contextmenu",function()
{
	return false;
});

/*
--------------------------------
CGS.A.04 : Responsive behaviour
--------------------------------
*/

function windowCustomResize() {
    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    $("html, body").css({"width":w,"height":h});
    var elem = document.getElementById("appli_graph_main_screen");
	if ($(document).width() > $(document).height()) {
		var currentOrientation = "landscape";
	} else {
		var currentOrientation = "portrait";
	};
	if (currentOrientation === "landscape") {
		if (elem.style.height === "60%") {
			elem.style.height = "100%";
			elem.style.width = "60%";
		};
	} else {
		if (elem.style.width === "60%") {
			elem.style.width = "100%";
			elem.style.height = "60%";
		};
	};
	if (typeof cy !== undefined) {cy.resize();};
}

$(window).resize(function () {
	setTimeout(function() {windowCustomResize();},500);
});

$(".modal").on("hidden.bs.modal", function() {
    setTimeout(function() {windowCustomResize();}, 500);
});

/*
--------------------------------
CGS.A.05 : Alert if not a computer device
--------------------------------
*/


if ( navigator.userAgent.match(/iPhone/i)
|| navigator.userAgent.match(/webOS/i)
|| navigator.userAgent.match(/Android/i)
|| navigator.userAgent.match(/iPad/i)
|| navigator.userAgent.match(/iPod/i)
|| navigator.userAgent.match(/BlackBerry/i)
|| navigator.userAgent.match(/Windows Phone/i)
) {
	$("#otherModal_body").html("<p><b>L\'usage de cette application est optimisé pour ordinateur. Pour une navigation confortable, fluide et interactive, il est conseillé de privilégier ce mode d’accès plutôt qu\'un smartphone ou une tablette.</b></p>"); 
	$("#otherModal_QuitButton").html("Poursuivre"); 
	$("#otherModal").modal("show");
};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CGS.B : GET PARAMETERS FROM URL
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/


/*
--------------------------------
CGS.B.01 : Features buttons to keep
--------------------------------
*/

var featuresButtons = {
	"toggle_fullscreen": getParamValue("toggle_fullscreen"),
	"toggle_graphs": getParamValue("toggle_graphs"),
	"toggle_lateralScreen": getParamValue("toggle_lateralScreen"),
	"toggle_map": getParamValue("toggle_map"),
	"toggle_legend": getParamValue("toggle_legend"),
	"toggle_tags": getParamValue("toggle_tags"),
	"toggle_stabilization": getParamValue("toggle_stabilization"),
	"toggle_physics": getParamValue("toggle_physics"),
	"toggle_fcose": getParamValue("toggle_fcose"),
	"toggle_ori_pos": getParamValue("toggle_ori_pos"),
	"toggle_ind_link": getParamValue("toggle_ind_link"),
	"toggle_highlighter": getParamValue("toggle_highlighter"),
	"toggle_modeEditor": getParamValue("toggle_modeEditor"),
	"toggle_summary": getParamValue("toggle_summary"),
	"toggle_save": getParamValue("toggle_save"),
	"toggle_information": getParamValue("toggle_information"),
	"toggle_bug": getParamValue("toggle_bug")
};

if (getParamValue("directId") !== undefined) {
	featuresButtons["toggle_graphs"] = "false";
};

/*
--------------------------------
CGS.B.02 : Activate or not euler computation
--------------------------------
*/

var featuresEulerComputation = getParamValue("eulerComputation");

/*
--------------------------------
CGS.B.03 : Showing the information modal screen or not
--------------------------------
*/

var featuresInformationShowing = getParamValue("showInformation");

/*
--------------------------------
CGS.B.04 : Click to begin or not
--------------------------------
*/

var featuresClickToBegin = getParamValue("clickToBegin");

if (featuresClickToBegin === "true") {
	document.getElementById("appli_graph_mask").style.visibility="visible";
};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CGS.C : MAIN FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/


/*
--------------------------------
CGS.C.01 : Function to seek params in URL
-------------------------------- 
*/

function getParamValue(paramName) {
	var url = window.location.search.substring(1);
	var qArray = url.split('&');
	for (var i = 0; i < qArray.length; i++) {
		var pArr = qArray[i].split('=');
		if (pArr[0] === paramName) {
			return pArr[1];
		};
	};
};

/*
--------------------------------
CGS.C.02 : Function to resolve a promise after loading a js script
--------------------------------
*/

function F_ScriptLoading(script_to_load,keepGoing=false,isModule=false) {
	return new Promise((resolve) => {
		let script = document.createElement('script');
		script.src = script_to_load;
		if (isModule === true) {script.type = "module";}
		script.onload = function () {
			setTimeout(function() {resolve('');},5);
		};
		script.onerror = function() {
			if (keepGoing === false) {
				$("#bug_report_appli").html("<p><b>/!\\</b></p><p>Le graphe recherché n\'existe pas. Le script suivant n\'a pas été chargé : <b>"+script_to_load+"</b></p><p><b>/!\\</b></p>"); 
				$("#graphChoiceModal").empty();
				$("#bugModal").modal("show"); 
			} else {
				resolve('');
			};
		};
		document.head.appendChild(script);
	});
};

/*
--------------------------------
CGS.C.03 : Function to load the application with the proper data
--------------------------------
*/

function loadAppliGraph(idGraph_entry){
	
	let chain_LoadAppliGraph = Promise.resolve();
	
	/* Php dict reading */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			function reqListener () {
				idGraph = this.responseText;
				resolve_LoadAppliGraph("");
			};
			var oReq = new XMLHttpRequest();
			oReq.onload = reqListener;
			oReq.open("get","php/_graph_data.php"+"?___VERSION_CACHE___"+"&"+"g="+idGraph_entry,true);
			oReq.send();
		});
	});
	
	/* Get the graph name */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			nameGraph = idGraph;
			nameGraph = nameGraph.split("\\");
			nameGraph = nameGraph[nameGraph.length-1];
			nameGraph = nameGraph.split("/");
			nameGraph = nameGraph[nameGraph.length-1];
			resolve_LoadAppliGraph("");
		});
	});
	
	/* Change the url (activated on web) */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			if (StandAloneVersion === undefined || StandAloneVersion === "" || StandAloneVersion.length === 0) {
				var url = new URL(window.location.href);
				var search_params = url.searchParams;
				search_params.set("directId",nameGraph);
				url.search = search_params.toString();
				window.history.replaceState({}, "", url.toString());
				resolve_LoadAppliGraph("");
			} else {
				resolve_LoadAppliGraph("");
			};
		});
	});
	
	/* Preprocessing */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			$('#graphChoiceModal').modal('hide');
			$("#appli_graph_spinner").fadeIn("slow");
			idGraph_path = "_TEMPLATES/"+idGraph+"/";
			resolve_LoadAppliGraph("");
		});
	});

	/* _void_config.js loading */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			let chain_ScriptLoading = Promise.resolve();
			chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,"js/_void_config.js?___VERSION_CACHE___"));
			chain_ScriptLoading = chain_ScriptLoading.then(resolve_LoadAppliGraph.bind(null,""));
		});
	});

	/* cytoscape.js and config.js loading */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			let chain_ScriptLoading = Promise.resolve();
			if (alreadyLoaded === false) {chain_ScriptLoading.then(F_ScriptLoading.bind(null,"dist/cytoscape/3.19.0/cytoscape.min.js"));};
			chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,"_TEMPLATES/"+idGraph+"/config.js?___VERSION_CACHE___"));
			chain_ScriptLoading = chain_ScriptLoading.then(resolve_LoadAppliGraph.bind(null,""));
		});
	});
	
	/* _void_config_completion.js loading */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			let chain_ScriptLoading = Promise.resolve();
			chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,"js/_void_config_completion.js?___VERSION_CACHE___"));
			chain_ScriptLoading = chain_ScriptLoading.then(resolve_LoadAppliGraph.bind(null,""));
		});
	});
	
	/* Checking data version */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			if ((greziVersion !== graphVersion) && (getParamValue("debug") === undefined)) {
				$("#bug_report_appli").html("<b><p>/!\\</p><p>Le graphe chargé ne correspond pas à la version actuelle de GREZI ("+greziVersion+") !</p><p>/!\\</p></b>"); 
				$("#graphChoiceModal").empty();
				$("#bugModal").modal("show");
				$("#bugModal").on("hide.bs.modal", function () {
					resolve_LoadAppliGraph("");
				});
			} else {
				resolve_LoadAppliGraph("");
			};
		});
	});
	
	/* Force the blank mode */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			if (featuresLoading_FORCING["blank"] === "true") {
				$("#appli_graph_cc").css("display", "none");
				$("#appli_graph_credentials").css("display", "none");
				$(".logo_title").css("display", "none");
			};
			resolve_LoadAppliGraph("");
		});
	});
	
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
		
			/* --------------------------------------------- */
			/* Creating the loading dictionnary */
			/* --------------------------------------------- */
			
			modulesList = [];
			
			/* --- EXTERNAL LIBS --- */
			
			/* Intro core JavaScript */
			if (((featuresLoading_FORCING["showInformation"] === "true") || (featuresInformationShowing === "true")) && (!(modulesList_AlreadyLoaded.includes("intro/2.9.3/js/intro.min.js")))) {
				modulesList.push("intro/2.9.3/js/intro.min.js");
				$("head").append("<link rel=\"stylesheet\" href=\"dist/intro/2.9.3/css/introjs.min.css\">");
				$("head").append("<link rel=\"stylesheet\" href=\"css/_mod_intro_graph.css?___VERSION_CACHE___\">");
			};
			/* Typed core JavaScript */
			if ((featuresGraph["animation"] === true) && (!(modulesList_AlreadyLoaded.includes("typed/2.0.11/typed.min.js")))) {modulesList.push("typed/2.0.11/typed.min.js");};
			/* Howler core JavaScript */
			if (((featuresGraph["animation"] === true)||(featuresGraph["sound"] === true)) && (!(modulesList_AlreadyLoaded.includes("howler/2.1.2/howler.min.js")))) {modulesList.push("howler/2.1.2/howler.min.js");};
			/* Lodash core JavaScript */
			if ((featuresButtons_FORCING["toggle_modeEditor"] !== "false") || (featuresGraph["editorOnLoad"] === true) && (!(modulesList_AlreadyLoaded.includes("lodash/4.17.15/lodash.min.js")))) {modulesList.push("lodash/4.17.15/lodash.min.js");};
			/* Autocomplete core JavaScript */
			if ((featuresGraph["featureSearchBar"] === true) && (!(modulesList_AlreadyLoaded.includes("autocomplete/0.3.0/autocomplete.min.js")))) {
				modulesList.push("autocomplete/0.3.0/autocomplete.min.js");
				$("head").append("<link rel=\"stylesheet\" href=\"dist/autocomplete/0.3.0/autocomplete.min.css\">");
			};
			/* Mapbox-gl core JavaScript */
			if ((featuresButtons_FORCING["toggle_map"] !== "false") && (!(modulesList_AlreadyLoaded.includes("mapbox-gl/1.13.0/js/mapbox-gl.js")))) {
				modulesList.push("mapbox-gl/1.13.0/js/mapbox-gl.js");
				$("head").append("<link rel=\"stylesheet\" href=\"dist/mapbox-gl/1.13.0/css/mapbox-gl.css\">");
			};

			/* --- CYTOSCAPE PLUGINS --- */
			
			/* Popper */
			if ((((featuresButtons_FORCING["toggle_modeEditor"] !== "false") || (featuresGraph["editorOnLoad"] === true)) || ((featuresGraph["tooltips"] === true) && (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/popper.min.js"))))) && (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/cytoscape-popper.min.js")))) {
				modulesList.push("cytoscape/3.19.0/plugins/popper.min.js");
				modulesList.push("cytoscape/3.19.0/plugins/cytoscape-popper.min.js");
			};
			/* Canvas */
			if ((featuresGraph["backgroundCanvas"] !== "") && (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/cytoscape-canvas.min.js")))) {modulesList.push("cytoscape/3.19.0/plugins/cytoscape-canvas.min.js");};
			/* Edgehandlees & Dbclick */
			if ((featuresButtons_FORCING["toggle_modeEditor"] !== "false") || (featuresGraph["editorOnLoad"] === true)) {
				$("head").append("<link rel=\"stylesheet\" href=\"css/_mod_editor_mode.css?___VERSION_CACHE___\">");
				if (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/cytoscape-edgehandles.min.js"))) {modulesList.push("cytoscape/3.19.0/plugins/cytoscape-edgehandles.min.js");};
				if (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/cytoscape-dblclick-master.js"))) {modulesList.push("cytoscape/3.19.0/plugins/cytoscape-dblclick-master.js");};
			};
			/* Euler */
			if (((featuresLoading_FORCING["eulerComputation"] === "true") || (featuresEulerComputation === "true")) && (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/cytoscape-euler.min.js")))) {modulesList.push("cytoscape/3.19.0/plugins/cytoscape-euler.min.js");};
			/* Ctxmenu */
			if (((featuresGraph["featureClustering"] === true) || (featuresGraph["initialClustering"].length > 0)) && (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/cytoscape-cxtmenu.min.js")))) {modulesList.push("cytoscape/3.19.0/plugins/cytoscape-cxtmenu.min.js");};
			/* Cola */
			if (((featuresButtons_FORCING["toggle_physics"] !== "false") || ((featuresGraph["physics"] === true) && (featuresGraph["static"] === false))) && (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/cola.v3.min.js"))) && (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/cytoscape-cola.min.js")))) {
				modulesList.push("cytoscape/3.19.0/plugins/cola.v3.min.js");
				modulesList.push("cytoscape/3.19.0/plugins/cytoscape-cola.min.js");
			};
			/* Fcose */
			if (((featuresButtons_FORCING["toggle_fcose"] !== "false") || (featuresGraph["fcoseOnLoad"] === true)) && (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/cytoscape-fcose.js")))) {
				modulesList.push("cytoscape/3.19.0/plugins/numeric.min.js");
				modulesList.push("cytoscape/3.19.0/plugins/layout-base.min.js");
				modulesList.push("cytoscape/3.19.0/plugins/cytoscape-layout-utilities.min.js");
				modulesList.push("cytoscape/3.19.0/plugins/cose-base.min.js");
				modulesList.push("cytoscape/3.19.0/plugins/cytoscape-fcose.min.js");
			};
			/* Mapbox-gl core JavaScript */
			if ((featuresButtons_FORCING["toggle_map"] !== "false") && (!(modulesList_AlreadyLoaded.includes("cytoscape/3.19.0/plugins/cytoscape-mapbox-gl.min.grezi.js")))) {
				modulesList.push("cytoscape/3.19.0/plugins/cytoscape-mapbox-gl.min.grezi.js");
			};
			
			modulesList_AlreadyLoaded = modulesList_AlreadyLoaded.concat(modulesList);
			
			let chain_ScriptLoading = Promise.resolve();
			modulesList.forEach(function(elem){
				chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,"dist/"+elem));
			});
			chain_ScriptLoading = chain_ScriptLoading.then(resolve_LoadAppliGraph.bind(null,""));

		});
	});
	
	/* core js loading */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			if (alreadyLoaded === false) {
				let chain_ScriptLoading = Promise.resolve();
				chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,"js/_core_functions.js?___VERSION_CACHE___"));
				chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,"js/_core_invert_color.js?___VERSION_CACHE___"));
				chain_ScriptLoading = chain_ScriptLoading.then(resolve_LoadAppliGraph.bind(null,""));
			} else {
				resolve_LoadAppliGraph('');
			};
		});
	});
	
	/* post-processing */
	chain_LoadAppliGraph = chain_LoadAppliGraph.then(function(){
		return new Promise((resolve_LoadAppliGraph) => {
			progressBarIncrement();
			setTimeout(function() {
				var script = document.createElement('script');
				script.src = "js/___appli_graph.js?___VERSION_CACHE___";
				document.head.appendChild(script);
			},10);
		});
	});
	
};

/*
--------------------------------
CGS.C.04 : Functions for the progress bar
--------------------------------
*/

let chain_ProgressBar = Promise.resolve();

function progressBarIncrement() {
	chain_ProgressBar = chain_ProgressBar.then(function(){
		return new Promise((resolve_ProgressBar) => {
			barProgress += 1/barStepsNumber;
			bar.animate(Math.min(1,barProgress));
			resolve_ProgressBar("Done");
		});
	});
};

function progressBarReinitialize(text,steps,addingSteps=false) {
	chain_ProgressBar = chain_ProgressBar.then(function(){
		return new Promise((resolve_ProgressBar) => {
			if (addingSteps === true) {
				barProgress=barProgress*barStepsNumber;
				barStepsNumber+=steps;
				barProgress=barProgress/barStepsNumber;
			} else {
				barStepsNumber=steps;
				barProgress=0;
				bar.set(0);
			};
			if (text !== "") {bar.setText(text)};
			resolve_ProgressBar("Done");
		});
	});
};

function progressBarSetText(text) {
	chain_ProgressBar = chain_ProgressBar.then(function(){
		return new Promise((resolve_ProgressBar) => {
			bar.setText(text);
			resolve_ProgressBar("Done");
		});
	});
};

function progressBarStop() {
	chain_ProgressBar = chain_ProgressBar.then(function(){
		return new Promise((resolve_ProgressBar) => {
			barStepsNumber=0;
			barProgress=0;
			bar.set(0);
			bar.setText("");
			resolve_ProgressBar("Done");
		});
	});
};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CGS.D : ON LOAD OPERATIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

$(window).on('load',function(){
	
	let chain_LoadingOperations = Promise.resolve();

	/* progressbar.js loading */
	chain_LoadingOperations = chain_LoadingOperations.then(function(){
		return new Promise((resolve_LoadingOperations) => {
			let chain_ScriptLoading = Promise.resolve();
			chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,"dist/progressbar/progressbar.min.js"));
			chain_ScriptLoading = chain_ScriptLoading.then(function(){
				return new Promise((resolve_ScriptLoading) => {
					bar = new ProgressBar.Line("#appli_graph_progessbar", {
						"strokeWidth": 2,
						"duration": 100,
						"easing": "easeInOut"
					});
					bar.setText("Initialisation");
					progressBarIncrement();
					resolve_ScriptLoading(resolve_LoadingOperations("Done"));
				});
			});
		});
	});

	/*
	--------------------------------
	CGS.D.01 : Anticipate the colors of the spinner if directId set
	--------------------------------
	*/	

	chain_LoadingOperations = chain_LoadingOperations.then(function(){
		return new Promise((resolve_LoadingOperations) => {
			progressBarIncrement();
			var directId_TEMP;
			if (StandAloneVersion !== undefined && StandAloneVersion !== "" && StandAloneVersion.length > 0) {
				directId_TEMP = StandAloneVersion;
			} else {
				if (getParamValue("directId") !== undefined) {directId_TEMP = getParamValue("directId");};
			};
			if (directId_TEMP !== undefined && directId_TEMP !== "" && directId_TEMP.length > 0) {
				let chain_AnticipateSpinnerColors = Promise.resolve();
				chain_AnticipateSpinnerColors = chain_AnticipateSpinnerColors.then(function(){
					return new Promise((resolve_AnticipateSpinnerColors) => {
						function reqListener () {
							let idGraph_path_anticipate = this.responseText;
							let chain_AnticipateSpinnerColors_BIS = Promise.resolve();
							chain_AnticipateSpinnerColors_BIS = chain_AnticipateSpinnerColors_BIS.then(F_ScriptLoading.bind(null,"_TEMPLATES/"+idGraph_path_anticipate+"/config.js?___VERSION_CACHE___"));
							chain_AnticipateSpinnerColors_BIS = chain_AnticipateSpinnerColors_BIS.then(function(){
								return new Promise((resolve_AnticipateSpinnerColors_BIS) => {
									if (featuresColors_FORCING !== undefined) {
										for (i_color in featuresColors_FORCING) {
											if (featuresColors_FORCING[i_color] !== "") {
												document.documentElement.style.setProperty(i_color,featuresColors_FORCING[i_color]);
											} else {
												document.documentElement.style.setProperty(i_color,getComputedStyle(document.documentElement).getPropertyValue(i_color+"_BACKUP"));
											};
										};
									};
									$("#appli_graph_progessbar > svg > path").attr("stroke",getComputedStyle(document.documentElement).getPropertyValue("--main_color_01"));
									if (featuresGraph !== undefined) {
										if (featuresGraph["customIcons"] !== undefined) {
											if ((featuresGraph["customIcons"]["page_title"] !== undefined) && (featuresGraph["customIcons"]["page_title"] !== "")) {
												$("title").remove();
												$("head").append("<title>"+featuresGraph["customIcons"]["page_title"]+"</title>");
											} else {
												$("title").remove();
												$("head").append("<title>GREZI</title>");
											};
											if ((featuresGraph["customIcons"]["page_icon"] !== undefined) && (featuresGraph["customIcons"]["page_icon"] !== "")) {
												$("#page_icon").remove();
												$("head").append("<link id=\"page_icon\" rel=\"icon\" href=\""+"_TEMPLATES/"+idGraph_path_anticipate+"/"+featuresGraph["customIcons"]["page_icon"]+"\" />");
											} else {
												$("#page_icon").remove();
												$("head").append("<link id=\"page_icon\" rel=\"icon\" href=\"icon/grezi.ico\" />");
											};
									};
									};
									resolve_AnticipateSpinnerColors_BIS("");
								});
							});
							chain_AnticipateSpinnerColors_BIS = chain_AnticipateSpinnerColors_BIS.then(function(){
								return new Promise((resolve_AnticipateSpinnerColors_BIS) => {
									if (featuresGraph["animation"] === true && getParamValue("debug") === undefined) {
										let chain_AnticipateSpinnerColors_TER = Promise.resolve();
										chain_AnticipateSpinnerColors_TER = chain_AnticipateSpinnerColors_TER.then(F_ScriptLoading.bind(null,"_TEMPLATES/"+idGraph_path_anticipate+"/js/_data_typing.js?___VERSION_CACHE___"));
										chain_AnticipateSpinnerColors_TER = chain_AnticipateSpinnerColors_TER.then(function(){
											return new Promise((resolve_AnticipateSpinnerColors_TER) => {
												var link = document.createElement('link');
												link.setAttribute("rel", "stylesheet");
												link.setAttribute("type", "text/css");
												link.onload = resolve_AnticipateSpinnerColors_TER;
												link.setAttribute("href", "css/_mod_talking_circles.css?___VERSION_CACHE___");
												document.getElementsByTagName("head")[0].appendChild(link);
											});
										});
										chain_AnticipateSpinnerColors_TER = chain_AnticipateSpinnerColors_TER.then(function(){
											return new Promise((resolve_AnticipateSpinnerColors_TER) => {
												var div_talking_text = document.getElementById("_talking_introduction");
												if (animationCredentials["title"] !== "") {
													var div_talking_text_TITLE = "<h1>"+animationCredentials["title"]+"</h1><h2 style=\"animation:blinker 2s linear infinite\">Chargement en cours</h2>";
												} else {
													var div_talking_text_TITLE = "<h1>Propulsé par</h1><h1>GRÉSI</h1><h2 style=\"animation:blinker 2s linear infinite\">Chargement en cours</h2>";
												};
												if (animationCredentials["img"] !== "") {
													var div_talking_text_IMG = "_TEMPLATES/"+idGraph_path_anticipate+"/"+animationCredentials["img"];
												} else {
													var div_talking_text_IMG = "../appli_graph/img/grezi.webp";
												};
												div_talking_text.innerHTML = "<div id=\"_talking_introduction_TEXT\">"+div_talking_text_TITLE+"<hr></div>"+"<div id=\"_talking_introduction_IMG\"><img src=\""+div_talking_text_IMG.replace("\\","/").replace("\\","/")+"\"></img></div>";
												$("#_talking_introduction").fadeIn(1000,function(){
													resolve_AnticipateSpinnerColors_TER("");
													resolve_AnticipateSpinnerColors_BIS("");
												});
											});
										});
									} else {
										resolve_AnticipateSpinnerColors_BIS("");
									};
								});
							});
							chain_AnticipateSpinnerColors_BIS = chain_AnticipateSpinnerColors_BIS.then(function(){
								return new Promise((resolve_AnticipateSpinnerColors_BIS) => {
									resolve_AnticipateSpinnerColors_BIS("");
									resolve_AnticipateSpinnerColors("");
									resolve_LoadingOperations("");
								});
							});
						};
						var oReq = new XMLHttpRequest();
						oReq.onload = reqListener;
						oReq.open("get","php/_graph_data.php"+"?___VERSION_CACHE___"+"&"+"g="+directId_TEMP,true);
						oReq.send();
					});
				});
			} else {
				resolve_LoadingOperations("");
			};
		});
	});

	/*
	--------------------------------
	CGS.D.02 : Firstly check if there is a click to begin before downloading the graph
	--------------------------------
	*/
	
	chain_LoadingOperations = chain_LoadingOperations.then(function(){
		return new Promise((resolve_LoadingOperations) => {
			progressBarIncrement();
		
			$('#appli_graph_mask').on('click',function(){
				$("#appli_graph_mask").fadeOut("slow");
				$("#page-top").hover(
					function (){
						/* Nothing */
					},
					function (){
						$("#appli_graph_mask").fadeIn("slow");
					}
				);
				resolve_LoadingOperations('');
			});

			var featuresClickToBegin = getParamValue("clickToBegin");

			if (featuresClickToBegin !== "true") {
				resolve_LoadingOperations('');
			};
		
		});
	});
	
	/*
	--------------------------------
	CGS.D.03 : Php dict reading (activated on web)
	--------------------------------
	*/
	
	chain_LoadingOperations = chain_LoadingOperations.then(function(){
		return new Promise((resolve_LoadingOperations) => {
			progressBarIncrement();
			var idsGraphs_entry = getParamValue("ids");
			directIdGraphOption = getParamValue("directId");
			if ( (idsGraphs_entry === undefined) && (directIdGraphOption === undefined) ) {
				idsGraphs_entry = defaultIDS;
				idsGraphs_templates = true;
			};
			if (idsGraphs_entry !== undefined) {
				idsGraphs = [];
				if (getParamValue("debug") === undefined) {
					/* Using the script _graph_data.php script */
					idsGraphs_entry = idsGraphs_entry.split(',');
					let chain_LoadingOperations_PHP = Promise.resolve();
					for (i_id in idsGraphs_entry) {
						chain_LoadingOperations_PHP = chain_LoadingOperations_PHP.then(function(I_BACKUP){
							return new Promise((resolve_LoadingOperations_PHP) => {
								function reqListener () {
									idsGraphs.push(this.responseText.replace(/\\/g,"/"));
									resolve_LoadingOperations_PHP("");
								};
								var oReq = new XMLHttpRequest();
								oReq.onload = reqListener;
								oReq.open("get","php/_graph_data.php"+"?___VERSION_CACHE___"+"&"+"g="+idsGraphs_entry[I_BACKUP],true);
								oReq.send();
							});
						}.bind("",i_id));
					};
					chain_LoadingOperations_PHP = chain_LoadingOperations_PHP.then(resolve_LoadingOperations.bind(null,""));
				} else {
					/* Using the script _debug_data.php script */
					let chain_LoadingOperations_PHP = Promise.resolve();
					chain_LoadingOperations_PHP = chain_LoadingOperations_PHP.then(function(){
						return new Promise((resolve_LoadingOperations_PHP) => {
							if ( getParamValue("debug") !== "true") {
								var debugFolder = getParamValue("debug");
							} else {
								var debugFolder = "none";
							};
							function reqListener () {
								idsGraphs = this.responseText.replace(/\\/g,"/").split(",");
								resolve_LoadingOperations_PHP("");
							};
							var oReq = new XMLHttpRequest();
							oReq.onload = reqListener;
							oReq.open("get","php/_debug_data.php"+"?f="+debugFolder+"&___VERSION_CACHE___",true);
							oReq.send();
						});
					});
					chain_LoadingOperations_PHP = chain_LoadingOperations_PHP.then(resolve_LoadingOperations.bind(null,""));
				};
			} else {
				if (idsGraphs_entry !== undefined) {idsGraphs = idsGraphs_entry.split(',')};
				resolve_LoadingOperations("");
			};
		});
	});

	/*
	--------------------------------
	CGS.D.04 : Show an alter if the navigator is incompatible
	--------------------------------
	*/

	chain_LoadingOperations = chain_LoadingOperations.then(function(){
		return new Promise((resolve_LoadingOperations) => {
			progressBarIncrement();
			if ((navigator.userAgent.indexOf("Mozilla") !== -1) || (navigator.userAgent.indexOf("Chrome") !== -1)) {
				resolve_LoadingOperations("Done");
			} else {
				$("#otherModal_body").html("<p><b>/!\\</b></p><p>Attention GREZI n'est pleinement compatible qu'avec les dernières versions de <b>Mozilla Firefox</b> et <b>Google Chrome</b>. Si vous utilisez un autre navigateur vous rencontrerez sûrement des soucis techniques. Vous pourrez les renseigner via le rapport de dysfonctionnement de l'application.</p><p><b>/!\\</b></p>"); 
				$("#otherModal").modal("show");
				$('#otherModal').on('hidden.bs.modal', function () {
					resolve_LoadingOperations("Done");
				});
			};
		});
	});

	/*
	--------------------------------
	CGS.D.05 : Then load the graph
	--------------------------------
	*/
	
	chain_LoadingOperations = chain_LoadingOperations.then(function(){
		return new Promise((resolve_LoadingOperations) => {
			progressBarIncrement();
		
			/* CGS.D.05.01 : Directly load the graph */
			
			if ((directIdGraphOption !== undefined) || (StandAloneVersion !== undefined && StandAloneVersion !== "" && StandAloneVersion.length > 0)) {
				if (StandAloneVersion !== undefined && StandAloneVersion !== "" && StandAloneVersion.length > 0) {
					loadAppliGraph(StandAloneVersion);
				} else {
					if (directIdGraphOption !== undefined) {loadAppliGraph(directIdGraphOption);};
				};
			};
			
			/* CGS.D.05.02 : Show a graph selection screen */
			
			if ((idsGraphs !== undefined) && (directIdGraphOption === undefined) && (StandAloneVersion === undefined || StandAloneVersion === "" || StandAloneVersion.length === 0)) {
				
				var modalContent_IdsGraphs_CONTENT = "";
				
				/* CGS.D.05.02.01 : Create the templates button access */
				/*
				if (idsGraphs_templates === true) {
					var modalContent_TemplateGraphs_CONTENT = "";
					modalContent_TemplateGraphs_CONTENT = modalContent_TemplateGraphs_CONTENT + "<div id=\"\" class=\"row justify-content-center no-gutters mt-3 mt-lg-3 mb-3 mb-lg-3\">";
					modalContent_TemplateGraphs_CONTENT = modalContent_TemplateGraphs_CONTENT + "<div style=\"display:flex;justify-content:center;\" class=\"col-4\"><button type=\"button\" class=\"btn btn-primary\" onclick=\"loadAppliGraph('_GREZI/grezi_create_graph')\"><img class=\"lazyload\" data-src=\"icon/grezi_create_graph.svg\"><p>Créer un graphe</p></button></div>";
					modalContent_TemplateGraphs_CONTENT = modalContent_TemplateGraphs_CONTENT + "<div style=\"display:flex;justify-content:center;\" class=\"col-4\"><button type=\"button\" class=\"btn btn-primary\" onclick=\"loadAppliGraph('_GREZI/grezi_upload')\"><img class=\"lazyload\" data-src=\"icon/grezi_upload.svg\"><p>Charger des données</p></button></div>";
					modalContent_TemplateGraphs_CONTENT = modalContent_TemplateGraphs_CONTENT + "<div style=\"display:flex;justify-content:center;\" class=\"col-4\"><button type=\"button\" class=\"btn btn-primary\" onclick=\"loadAppliGraph('_GREZI/grezi_password')\"><img class=\"lazyload\" data-src=\"icon/grezi_password.svg\"><p>Utiliser un mot de passe</p></button></div>";
					modalContent_TemplateGraphs_CONTENT = modalContent_TemplateGraphs_CONTENT + "</div>";
					modalContent_TemplateGraphs_CONTENT = modalContent_TemplateGraphs_CONTENT + "<div class=\"modal-hr\"><hr class=\"my-6\"></div>";
					var modalContent_TemplateGraphs = document.getElementById("appli_graph_TemplateGraphs");
					modalContent_TemplateGraphs.innerHTML = modalContent_TemplateGraphs_CONTENT;
				};
				*/
			
				/* CGS.D.05.02.02 : Create the preview for each graph */
				var promise_IdsGraphs = new Promise(function(resolve, reject) {
					
					var counter_IdsGraphs = 1;
					
					let chain_GraphsPreviews = Promise.resolve();
					
					for (i_id in idsGraphs) {
						
						chain_GraphsPreviews = chain_GraphsPreviews.then(F_ScriptLoading.bind(null,"js/_void_title.js?___VERSION_CACHE___",true));
						
						chain_GraphsPreviews = chain_GraphsPreviews.then(F_ScriptLoading.bind(null,"_TEMPLATES/"+idsGraphs[i_id]+"/title.js?___VERSION_CACHE___",true));
						
						chain_GraphsPreviews = chain_GraphsPreviews.then(function(I_BACKUP){
							return new Promise((resolve_GraphsPreviews) => {
								
								if (getParamValue("debug") !== undefined) {
									modalContent_IdsGraphs_CONTENT = modalContent_IdsGraphs_CONTENT.concat("<div id=\"",idsGraphs[I_BACKUP].replace(/\//g,"___SLASH___"),"\"><p><b>",idsGraphs[I_BACKUP],"</b></p><p>",title_GRAPH,"</p><p>",subtitle_GRAPH,"</p></div>");
								} else {
									modalContent_IdsGraphs_CONTENT = modalContent_IdsGraphs_CONTENT.concat("<div id=\"",idsGraphs[I_BACKUP].replace(/\//g,"___SLASH___"),"\" class=\"inner row justify-content-center no-gutters mb-5 mb-lg-5\"><div style=\"display:flex;align-items:center;\" class=\"col-lg-6\"><img class=\"img-fluid lazyload\" data-src=\"_TEMPLATES/",idsGraphs[I_BACKUP],"/img/card.png\" onerror=\"this.onerror=null; this.src='img/default_card.png'\" title=\"EcoRhizo Graph : ",title_GRAPH,"\" alt=\"",subtitle_GRAPH,"\"></div><div class=\"col-lg-6 modal_block_text\"><div class=\"modal_block_text_CONTENT\"><h2>",title_GRAPH,"</h2><p>",subtitle_GRAPH,"</p></div></div></div>");
								};
								
								/* Original html
								
								<div id=\"",idsGraphs[I_BACKUP],"\" class=\"inner row justify-content-center no-gutters mb-5 mb-lg-5\">
									<div class=\"col-lg-6\">
										<img class=\"img-fluid lazyload\" data-src=\"_TEMPLATES/",idsGraphs[I_BACKUP],"/img/card.png\" onerror=\"this.onerror=null; this.src='img/default_card.png'\" title=\"EcoRhizo Graph : ",title_GRAPH,"\" alt=\"",subtitle_GRAPH,"\">
									</div>
									<div class=\"col-lg-6 modal_block_text\">
										<div class=\"modal_block_text_CONTENT\">
											<h2>",title_GRAPH,"</h2>
											<p>",subtitle_GRAPH,"</p>
										</div>
									</div>
								</div>
								
								*/
								
								modalContent_IdsGraphs_CONTENT = modalContent_IdsGraphs_CONTENT.concat("<br>");
							
								resolve_GraphsPreviews("");
						
							});
						}.bind("",i_id));
						
					};
					
					chain_GraphsPreviews = chain_GraphsPreviews.then(resolve.bind(null,modalContent_IdsGraphs_CONTENT));
					
				});

				/* CGS.D.05.02.03 : Link the event to load the corresponding graph */
				promise_IdsGraphs.then(function(value) {
					
					var modalContent_IdsGraphs_FINAL_TEXT = modalContent_IdsGraphs_CONTENT;
					
					let chain_GraphsPreviews = Promise.resolve();
					
					chain_GraphsPreviews = chain_GraphsPreviews.then(function(){
						return new Promise((resolve_GraphsPreviews) => {
							var modalContent_IdsGraphs = document.getElementById("appli_graph_IdsGraphs");
							modalContent_IdsGraphs.innerHTML = modalContent_IdsGraphs_FINAL_TEXT;
							resolve_GraphsPreviews('');
						});
					});
					
					chain_GraphsPreviews = chain_GraphsPreviews.then(function(){
						return new Promise((resolve_GraphsPreviews) => {
							for (i_id2 in idsGraphs) {
								$('#' + idsGraphs[i_id2].replace(/\//g,"___SLASH___")).click(function () {
									loadAppliGraph(this.id.replace(/___SLASH___/g,"/"));
								});
								$('#' + idsGraphs[i_id2].replace(/\//g,"___SLASH___")).hover(
									function (){document.body.style.cursor = 'pointer';},
									function (){document.body.style.cursor = 'default';}
								);
							};
							resolve_GraphsPreviews('');
						});
					});
					
					chain_GraphsPreviews = chain_GraphsPreviews.then(function(){
						return new Promise((resolve_GraphsPreviews) => {
							$('#graphChoiceModal').modal('show');
						});
					});
					
				});
				
			};
		
		});
	});

});
