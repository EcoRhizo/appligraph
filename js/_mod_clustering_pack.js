/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : CLUSTERING PACK
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/* Cluster functions : group */
function GroupsToCluster(ListGroupsToCluster,layoutActivation) {
	
	var promise_Cluster = new Promise(function(resolveCuster, rejectCluster) {
		for (i_group in ListGroupsToCluster) {
			nodesToCluster_TEMP = cy.nodes("[group = " + '"' + ListGroupsToCluster[i_group] + '"' + "]");
			nodesToCluster_TEMP.forEach(function (ele, i, eles) {
				var collectionToCluster_TEMP = ele.successors();
				if (collectionToCluster_TEMP.length > 0) {
					cy.remove(collectionToCluster_TEMP);
					listClusters[ele.id()] = collectionToCluster_TEMP;
					ele.unselectify();
					ele.addClass("cluster");
					if ((ele.style("border-width") !== "0px") && (ele.style("border-width") !== "0rem")) {
						ele.style("border-color",mainColor_02);
						ele.style("border-width","4rem");
					} else {
						ele.style("color",mainColor_02);
					};
				};
			});
		};
		resolveCuster('');
	});
	
	promise_Cluster.then(function(value) {
		if (layoutActivation === true) {
			if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-cola.min.js")) {
				return InitialLayout("euler_cola",true);
			} else {
				if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-fcose.min.js")) {
					return fcoseLayout();
				};
			};
		};
	});
	
	return promise_Cluster;
	
};

/* Cluster functions : chain */
function ChainClustering(cytoscapeElements,intervalClustering,layoutActivation) {
	
	var cytoscapeElements_EdgesToStore = cytoscapeElements.edges(".indirectEdge").remove();
	
	var promise_Cluster = new Promise(function(resolveCuster, rejectCluster) {
		var clusteringInterval = setInterval(function() {
			var cytoscapeElements_NodesToCluster = cytoscapeElements.nodes().difference(cytoscapeElements.nodes(".insensitiveNode"));
			var nodesRoots_TEMP;
			var nodesLeaves_TEMP = cytoscapeElements_NodesToCluster.leaves();
			nodesLeaves_TEMP.forEach(function (ele, i, eles) {
				if (nodesRoots_TEMP === undefined) {
					nodesRoots_TEMP = cy.getElementById(ele.id());
				} else {
					if (ele.data("rank") > nodesRoots_TEMP.data("rank")) {nodesRoots_TEMP = cy.getElementById(ele.id());};
				};
			});
			if (nodesRoots_TEMP === undefined) {nodesRoots_TEMP = nodesLeaves_TEMP.nodes()[0]};
			var nodesToCluster_TEMP = nodesRoots_TEMP.incomers();
			if (nodesToCluster_TEMP.length > 0) {
				var collectionToCluster_TEMP = nodesToCluster_TEMP.nodes().outgoers();
				if (collectionToCluster_TEMP.length > 0) {
					cy.remove(collectionToCluster_TEMP);
					if (Object.keys(listClusters).includes(nodesToCluster_TEMP.nodes()[0].id())) {
						listClusters[nodesToCluster_TEMP.nodes()[0].id()] = listClusters[nodesToCluster_TEMP.nodes()[0].id()].union(collectionToCluster_TEMP);
					} else {
						listClusters[nodesToCluster_TEMP.nodes()[0].id()] = collectionToCluster_TEMP;
					};
					nodesToCluster_TEMP.nodes()[0].unselectify();
					nodesToCluster_TEMP.nodes()[0].addClass("cluster");
					if ((nodesToCluster_TEMP.nodes()[0].style("border-width") !== "0px") && (nodesToCluster_TEMP.nodes()[0].style("border-width") !== "0rem")) {
						nodesToCluster_TEMP.nodes()[0].style("border-color",mainColor_02);
						nodesToCluster_TEMP.nodes()[0].style("border-width","4rem");
					} else {
						nodesToCluster_TEMP.nodes()[0].style("color",mainColor_02);
					};
				};
			} else {
				clearInterval(clusteringInterval);
				resolveCuster('');
			};
		},intervalClustering);
	});
	
	promise_Cluster.then(function(value) {
		cytoscapeElements_EdgesToStore.restore();
		if (layoutActivation === true) {
			if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-cola.min.js")) {
				return InitialLayout("euler_cola",true);
			} else {
				if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-fcose.min.js")) {
					return fcoseLayout();
				};
			};
		};
	});
	
	return promise_Cluster;
	
};

/* Cluster functions : open a single cluster */
function UndoClustering(varCluster,applyInitialLayout=true,applyForceStabilization=true) {
	mainLayout.stop();
	cy.elements().unselect();
	varCluster.removeClass("cluster");
	if (featuresGraph["infoBubbles"] === true) {informationBubbles(varCluster);};
	if (featuresGraph["infoTags"] === true) {informationTags(varCluster);};
	if ((varCluster.style("border-width") !== "0px") && (varCluster.style("border-width") !== "0rem")) {
		varCluster.style("border-color",mainColor_01);
		varCluster.style("border-width","2rem");
	} else {
		varCluster.style("color",mainColor_01);
	};
	var clusterNodes = listClusters[varCluster.id()].nodes();
	var clusterId = cy.getElementById(varCluster.id());
	var clusterPosition = clusterId.position();
	listClusters[varCluster.id()].restore();
	if (featuresGraph["static"]) {
		cy.$(".defaultNode").lock();
	};
	if ( (hierarchicalEnabling === false) && (featuresGraph["static"] === false) ) {
		clusterNodes.forEach(function(ele){
			ele.position(clusterPosition);
		});
		var clusterRadius = Math.max(clusterId.boundingBox()["w"],clusterId.boundingBox()["h"]);
		var tempLayout = clusterNodes.layout({
			"name": "circle",
			"fit": false,
			"boundingBox": {"x1": clusterPosition["x"], "y1": clusterPosition["y"], "w": 0, "h": 0},
			"radius": clusterRadius,
			"avoidOverlap": false,
			"animate": true,
			"animationDuration": 250,
			"stop": function() {
				setTimeout( function (){varCluster.selectify()},10);
				tempLayout.stop();
				if (applyInitialLayout === true) {
					if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-cola.min.js")) {
						if ((cy.nodes().boundingBox()["h"] < 0.85*cy.extent()["h"]) | (cy.nodes().boundingBox()["w"] < 0.85*cy.extent()["w"])) {
							return InitialLayout("cola",false);
						} else {
							return InitialLayout("cola",true);
						};
					} else {
						if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-fcose.min.js")) {
							return fcoseLayout();
						};
					};
				} else {
					return "done";
				};
			}
		});
		tempLayout.run();
		delete listClusters[varCluster.id()];
	} else {
		setTimeout( function (){varCluster.selectify()},10);
		if (applyForceStabilization === true) {
			if (!((cy.nodes().boundingBox()["h"] < 0.85*cy.extent()["h"]) | (cy.nodes().boundingBox()["w"] < 0.85*cy.extent()["w"]))) {
				ForceStabilization();
			};
		};
		delete listClusters[varCluster.id()];
	};
};

/* "Group all" button */

if (alreadyLoaded === false) {
	document.getElementById("group_ALL").addEventListener("click", function () {
		var promise_GroupAll = new Promise(async function(resolve, reject) {
			$("#appli_graph_spinner").fadeIn("fast");
			resolve(ChainClustering(cy,100,false));
		});
		promise_GroupAll.then(function(value) {
			$("#appli_graph_spinner").fadeOut("fast");
		});
	});
};

/* "Ungroup all" button */

function ChainUndoClustering(dealWithSpinner=true) {
	return new Promise((resolve_FINAL) => {
		cy.nodes().unselect();
		var promise_UngroupAll = new Promise(function(resolve_UngroupAll, reject_UngroupAll) {
			if (Object.keys(listClusters).length > 0) {
				if (dealWithSpinner === true) {$("#appli_graph_spinner").fadeIn("fast");};
				var switchUngroup = false;
				var switchUngroup_BIS = false;
				var ungroupInterval = setInterval( function () {
					if (cy.animated() === false) {
						if (switchUngroup === true) {
							if (Object.keys(listClusters).length === 0) {
								clearInterval(ungroupInterval);
								resolve_UngroupAll('');
							};
							switchUngroup = false;
							switchUngroup_BIS = false;
						} else {
							if (switchUngroup_BIS === false) {
								var promise_UngroupAll_bis = new Promise(function(resolve_UngroupAll_bis, reject_UngroupAll_bis) {
									cy.nodes().forEach(function(elem) {
										if (Object.keys(listClusters).includes(elem.id())) {
											UndoClustering(elem,false);
										};
									});
									switchUngroup_BIS = true;
									var promise_UngroupAll_ter = new Promise(function(resolve_UngroupAll_ter, reject_UngroupAll_ter) {
										if ( (hierarchicalEnabling === false) && (featuresGraph["static"] === false) ) {
											if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-cola.min.js")) {
												resolve_UngroupAll_ter(InitialLayout("cola",true));
											} else {
												if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-fcose.min.js")) {
													resolve_UngroupAll_ter(fcoseLayout());
												};
											};
										} else {
											resolve_UngroupAll_ter("done");
										};
									});
									promise_UngroupAll_ter.then(function(value_UngroupAll_ter) {
										resolve_UngroupAll_bis("done");
									});
								});
								promise_UngroupAll_bis.then(function(value_UngroupAll_bis) {
									switchUngroup = true;
								});
							};
						};
					};
				}, 100);
			} else {
				if (dealWithSpinner === true) {
					$("#appli_graph_spinner").fadeIn("fast", function () {resolve_UngroupAll('')});
				} else {
					resolve_UngroupAll('');
				};
			};
		});
		promise_UngroupAll.then(function(value_UngroupAll) {
			
			if (dealWithSpinner === true) {$("#appli_graph_spinner").fadeOut("fast");};
			
			resolve_FINAL('');
			
		});
	});
};

if (alreadyLoaded === false) {
	document.getElementById("ungroup_ALL").addEventListener("click", function () {
		ChainUndoClustering(true);
	});
};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CONTEXT MENU
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var options_contextualMenu = {
	"selector": "node:grabbable",
	"fillColor" : mainColor_01,
	"activeFillColor": mainColor_01_faded,
	"commands": [
		{
			"content": "<img width=\"50\" height=\"50\" src=\"icon/highlight_ascendency_white.svg\"></img>",
			"select": function(ele) {
				/* First cluster the childs */
				var promise_ContextCluster = new Promise(function(resolve, reject) {
					var collectionToCluster_TEMP = ele.successors();
					if (collectionToCluster_TEMP.nodes().length > 0) {
						var contextualInterval = setInterval(function() {
							collectionToCluster_TEMP = ele.successors();
							if (collectionToCluster_TEMP.nodes().length > 0) {
								ChainClustering(collectionToCluster_TEMP.union(ele),0,false);
							} else {
								clearInterval(contextualInterval);
								resolve('');
							};
						},100);
					} else {
						resolve('');
					};
				});
				/* Then cluster the result to the parent */
				promise_ContextCluster.then(function(value) {
					var collectionToCluster_TEMP = ele.incomers();
					if (collectionToCluster_TEMP.nodes().length > 0) {
						cy.remove(ele);
						if (Object.keys(listClusters).includes(collectionToCluster_TEMP.nodes()[0].id())) {
							listClusters[collectionToCluster_TEMP.nodes()[0].id()] = listClusters[collectionToCluster_TEMP.nodes()[0].id()].union(collectionToCluster_TEMP.union(ele).difference(collectionToCluster_TEMP.nodes()[0]));
						} else {
							listClusters[collectionToCluster_TEMP.nodes()[0].id()] = collectionToCluster_TEMP.union(ele).difference(collectionToCluster_TEMP.nodes()[0]);
						};
						collectionToCluster_TEMP.nodes()[0].unselectify();
						collectionToCluster_TEMP.nodes()[0].addClass("cluster");
						if ((collectionToCluster_TEMP.nodes()[0].style("border-width") !== "0px") && (collectionToCluster_TEMP.nodes()[0].style("border-width") !== "0rem")) {
							collectionToCluster_TEMP.nodes()[0].style("border-color",mainColor_02);
							collectionToCluster_TEMP.nodes()[0].style("border-width","4rem");
						} else {
							collectionToCluster_TEMP.nodes()[0].style("color",mainColor_02);
						};
					};
				});
			}
		},
		{
			"content": "<img width=\"50\" height=\"50\" src=\"icon/highlight_downdency_white.svg\"></img>",
			"select": function(ele) {
				var collectionToCluster_TEMP = ele.successors();
				if (collectionToCluster_TEMP.nodes().length > 0) {
					var contextualInterval = setInterval(function() {
						collectionToCluster_TEMP = ele.successors();
						if (collectionToCluster_TEMP.nodes().length > 0) {
							ChainClustering(collectionToCluster_TEMP.union(ele),1,false);
						} else {
							clearInterval(contextualInterval);
						};
					},100);
				};
			}
		}
	]
};

var contextualMenu = cy.cxtmenu(options_contextualMenu);

