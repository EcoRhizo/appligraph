/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE APPLI GRAPH : SET THE FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var isMoving = true;

cy.on("mouseover", "node.planetNode", function() {
	isMoving = this.id();
});

cy.on("mouseout", "node.planetNode", function() {
	isMoving = true;
});

function makeOrbit(node,attractor,stretchFactor_x,stretchFactor_y,timestamp,duration,interval,direction) {
	var x_orbit = null;
	var y_orbit = null;
	var x_origin = null;
	var y_origin = null;
	var start = null;
	var progress = null;
	var orbitalMovingInterval = setInterval( function () {
		x_origin = attractor.position()["x"];
		y_origin = attractor.position()["y"];
		if (isMoving !== node.id()) {
			if (start === null) {
				if (direction === "hours") {
					start = 0;
				} else {
					start = duration;
				}
			};
			progress = (duration - start) / duration;
			x_orbit = stretchFactor_x * Math.sin(progress * 2 * Math.PI);
			y_orbit = stretchFactor_y * Math.cos(progress * 2 * Math.PI);
			cy.getElementById(node.id()).position({"x": x_orbit+x_origin, "y": y_orbit+y_origin});
			if (direction === "hours") {
				start += duration / timestamp;
				if (progress <= 0) {start = null};
			} else {
				start -= duration / timestamp;
				if (progress >= 1) {start = null};
			};
		};
	},interval);
};


