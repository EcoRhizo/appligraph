/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
___APPLI_GRAPH.JS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
SCRIPT STRUCTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
	* AGS.A : GLOBAL VARIABLES
	* AGS.B : PRE-PROCESSING
		* AGS.B.01 : Editing the information modal screen
		* AGS.B.02 : Forcing the loading options
		* AGS.B.03 : Updating the icons
		* AGS.B.04 : Updating the colors
		* AGS.B.05 : Displaying or not the features buttons
		* AGS.B.06 : Editing the colors of the features buttons
		* AGS.B.07 : Generating the default lateral screen body
		* AGS.B.08 : Creating a default variable for the node colors
	*
	* AGS.C : MAIN STEPS
		* AGS.C.01 : Init events
			* AGS.C.01.01 : Hierarchical network variables
			* AGS.C.01.02 : Activate or not the tooltips
			* AGS.C.01.03 : Initiate the hilighter function
		*
		* AGS.C.02 : Creating views buttons
			* AGS.C.02.01 : "Reload" button
			* AGS.C.02.02 : "Group all" and "Ungroup all" buttons
			* AGS.C.02.03 : Link functions to the buttons
		*
		* AGS.C.03 : Set the network
			* AGS.C.03.01 : Prepare the options variable
			* AGS.C.03.02 : Setting the options of the network (see the cytoscape documentation for more details)
			* AGS.C.03.03 : Create the network
			* AGS.C.03.04 : Changing the options if the graph is static
			* AGS.C.03.05 : Changing the options if specified in the config.js file
			* AGS.C.03.06 : Changing the groups styles if specified in the config.js files
			* AGS.C.03.07 : Initial layout
			* AGS.C.03.08 : Deal with state of buttons
		*
	*
	* AGS.D : LAUNCHING PROMISES
		* AGS.D.01 : Data promise
			* AGS.D.01.01 : Upload custom data
			* AGS.D.01.02 : Loading the datas
			* AGS.D.01.03 : Preparing the datas of the network
		*
		* AGS.D.02 : Dealing with specified positions
			* AGS.D.02.01 : Set the custom positions of the nodes
			* AGS.D.02.02 : Set the custom positions of the compunds
			* AGS.D.02.03 : Lock the nodes if static option is activated
		*
		* AGS.D.03 : Script loading promise
			* AGS.D.03.01 : Creating the loading dictionnary
			* AGS.D.03.02 : Loading the dictionnary
			* AGS.D.03.03 : Loading the custom scripts
			* AGS.D.03.04 : Ending the promise
		*
		* AGS.D.04 : Some post-process promise
			* AGS.D.04.01 : Generating the modal screen summary body
			* AGS.D.04.02 : Postprocessing
			* AGS.D.04.03 : Backup variable
			* AGS.D.04.04 : Creating the search bar
			* AGS.D.04.05 : Make the custom cluters
		*
		* AGS.D.05 : Animation promise
		* AGS.D.06 : Fcose promise
		* AGS.D.07 : Final promise
			* AGS.D.07.01 : Activating the tutorial
			* AGS.D.07.02 : Check the alreadyLoaded variable
			* AGS.D.07.03 : Stabilization safeguard
		*
		* AGS.D.08 : Spinner promise
			* AGS.D.08.01 : Make the graph visible
			* AGS.D.08.02 : Loading the search bar
			* AGS.D.08.03 : Hide the spinner
			* AGS.D.08.04 : Open the editor mode or the map mode on load
			* AGS.D.08.05 : Bug modal opening after 30s to look for comments (remove this step when GREZI is no longer in beta)
			* AGS.D.08.06 : Stabilize the tooltips
			* AGS.D.08.07 : End the promise
		*
		* AGS.D.09 : Script template custom additions
	*
*/
	
/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
AGS.A : GLOBAL VARIABLES
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var cy;

var cy_elementsBackup;

var dictSemApps = {};

var isLateral = "closing";

var listClusters = {};

var dataGraph;

var ouiz; /* TO SUPRESS LATER */

var templateScript_CustomAdditions;

let chain_ScriptLoading = Promise.resolve();

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
AGS.B : PRE-PROCESSING
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

progressBarReinitialize("Chargement du gabarit",11,false);

/*
--------------------------------
AGS.B.01 : Editing the information modal screen
--------------------------------
*/

progressBarIncrement();

var modalContent_Information = document.getElementById("appli_graph_description");

modalContent_Information.innerHTML = featuresGraph["description"];

/*
--------------------------------
AGS.B.02 : Forcing the loading options
--------------------------------
*/

progressBarIncrement();

if (featuresLoading_FORCING["eulerComputation"] === "true") {featuresEulerComputation = featuresLoading_FORCING["eulerComputation"]};

if (featuresLoading_FORCING["showInformation"] === "true") {featuresInformationShowing = featuresLoading_FORCING["showInformation"]};

if (featuresLoading_FORCING["clickToBegin"] === "true") {document.getElementById("appli_graph_mask").style.visibility="visible";};

/*
--------------------------------
AGS.B.03 : Updating the icons
--------------------------------
*/

progressBarIncrement();

if (featuresGraph["defaultIcons"] === false) {
	$(".logo_title").css("display", "none");
	$("title").remove();
	$("#page_icon").remove();
} else {
	$("title").remove();
	$("head").append("<title>GREZI</title>");
	$("#page_icon").remove();
	$("head").append("<link id=\"page_icon\" rel=\"icon\" href=\"icon/grezi.ico\" />");
};

if ((featuresGraph["customIcons"]["page_title"] !== undefined) && (featuresGraph["customIcons"]["page_title"] !== "")) {
	$("title").remove();
	$("head").append("<title>"+featuresGraph["customIcons"]["page_title"]+"</title>");
} else {
	$("title").remove();
	$("head").append("<title>GREZI</title>");
};

if ((featuresGraph["customIcons"]["page_icon"] !== undefined) && (featuresGraph["customIcons"]["page_icon"] !== "")) {
	$("#page_icon").remove();
	$("head").append("<link id=\"page_icon\" rel=\"icon\" href=\""+idGraph_path+featuresGraph["customIcons"]["page_icon"]+"\" />");
} else {
	$("#page_icon").remove();
	$("head").append("<link id=\"page_icon\" rel=\"icon\" href=\"icon/grezi.ico\" />");
};

var div_modal_bug_CONTENT = document.getElementById("page_title_FORM");
if (div_modal_bug_CONTENT !== null) {div_modal_bug_CONTENT.innerHTML = getParamValue("directId");};

/*
--------------------------------
AGS.B.04 : Updating the colors
--------------------------------
*/

progressBarIncrement();

for (i_color in featuresColors_FORCING) {
	if (featuresColors_FORCING[i_color] !== "") {
		document.documentElement.style.setProperty(i_color,featuresColors_FORCING[i_color]);
	} else {
		document.documentElement.style.setProperty(i_color,getComputedStyle(document.documentElement).getPropertyValue(i_color+"_BACKUP"));
	};
};

/*
--------------------------------
AGS.B.05 : Displaying or not the features buttons
--------------------------------
*/

progressBarIncrement();

for (i in featuresButtons) {
	if (featuresButtons[i] === "false") {
		$("#" + i + "_BR").css("display", "none");
		$("#" + i + "_LI").css("display", "none");
		$("#" + i).css("display", "none");
	} else {
		$("#" + i + "_BR").css("display", "initial");
		$("#" + i + "_LI").css("display", "list-item");
		$("#" + i).css("display", "initial");
	};
};

for (i in featuresButtons_FORCING) {
	if (featuresButtons_FORCING[i] === "true") {
		$("#" + i + "_BR").css("display", "initial");
		$("#" + i + "_LI").css("display", "list-item");
		$("#" + i).css("display", "initial");
	};
	if (featuresButtons_FORCING[i] === "false") {
		$("#" + i + "_BR").css("display", "none");
		$("#" + i + "_LI").css("display", "none");
		$("#" + i).css("display", "none");
	};
};

if ($("#toggle_physics").css("display") !== "none") {
	if (featuresGraph["static"] === true) {
		featuresButtons["toggle_physics"] = "false";
		$("#toggle_physics_BR").css("display", "none");
		$("#toggle_physics_LI").css("display", "none");
		$("#toggle_physics").css("display", "none");		
	};
};

/*
--------------------------------
AGS.B.06 : Editing the colors of the features buttons
--------------------------------
*/

progressBarIncrement();

if (featuresColors_FORCING["--main_color_01"] !== "") {
	var mainColor = featuresColors_FORCING["--main_color_01"];
	mainColor = mainColor.replace("#","");
	for (i in featuresButtons) {
		if (featuresButtons[i] !== "false") {
			applyColorTransform("#"+i+" > a > img",mainColor,featuresColors_FILTER);
			applyColorTransform("#"+i+"_LI"+" > img",mainColor,featuresColors_FILTER);
		};
	};
	applyColorTransform("#appli_graph_SEARCH_BUTTON > img",mainColor,featuresColors_FILTER);
} else {
	var defaultFilter = "filter: invert(58%) sepia(34%) saturate(458%) hue-rotate(204deg) brightness(86%) contrast(94%);";
	for (i in featuresButtons) {
		if (featuresButtons[i] !== "false") {
			applyColorTransform("#"+i+" > a > img","",defaultFilter);
			applyColorTransform("#"+i+"_LI"+" > img","",defaultFilter);
		};
	};
	applyColorTransform("#appli_graph_SEARCH_BUTTON > img","",defaultFilter);
};

/*
--------------------------------
AGS.B.07 : Generating the default lateral screen body
--------------------------------
*/

progressBarIncrement();

var textTEMP = "";
textTEMP = textTEMP.concat("<h1>Survolez les éléments pour plus d'informations</h1>");
textTEMP = textTEMP.concat("<div class=\"lateral-hr\"><hr class=\"my-6\"></div>");
var lateralContent = document.getElementById("tooltipLateral_body");
lateralContent.innerHTML = textTEMP;

/*
--------------------------------
AGS.B.08 : Generating the default lateral screen body
--------------------------------
*/

progressBarIncrement();

var mainColor_01 = getComputedStyle(document.documentElement).getPropertyValue("--main_color_01");
var mainColor_02 = getComputedStyle(document.documentElement).getPropertyValue("--main_color_02");
var mainColor_03 = getComputedStyle(document.documentElement).getPropertyValue("--main_color_03");
var mainColor_04 = getComputedStyle(document.documentElement).getPropertyValue("--main_color_04");
var mainColor_05 = getComputedStyle(document.documentElement).getPropertyValue("--main_color_05");
var mainColor_01_faded = getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded");
var mainColor_03_faded = getComputedStyle(document.documentElement).getPropertyValue("--main_color_03_faded");
var mainColor_node = getComputedStyle(document.documentElement).getPropertyValue("--main_color_node");

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
AGS.C : MAIN STEPS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
AGS.C.01 : Init events
--------------------------------
*/

progressBarIncrement();

/* AGS.C.01.01 : Hierarchical network variables */

var hierarchicalEnabling = false;

if (window.screen.width > window.screen.height) {
	var hierarchicalAngle = 0
} else {
	var hierarchicalAngle = -90
};

/* AGS.C.01.02 : Activate or not the tooltips */

if (featuresGraph["tooltips"] === true) {
	$("#info_tooltip").css({"visibility": "visible"});
	$("#info_tooltip").css({"height": "auto"});
	$("#info_tooltip").css({"margin": "0.5em 10% 0px 0px"});
	var tooltipsGraph_BCK = true;
} else {
	$("#info_tooltip").css({"visibility": "hidden"});
	$("#info_tooltip").css({"height": "0"});
	$("#info_tooltip").css({"margin": "0"});
	var tooltipsGraph_BCK = false;
};

if (featuresGraph["modalSelection"] === true) {
	$("#info_tooltipModal").css({"visibility": "visible"});
	$("#info_tooltipModal").css({"height": "auto"});
	$("#info_tooltipModal").css({"margin": "0.5em 10% 0px 0px"});
} else {
	$("#info_tooltipModal").css({"visibility": "hidden"});
	$("#info_tooltipModal").css({"height": "0"});
	$("#info_tooltipModal").css({"margin": "0"});
};

if (featuresGraph["infoBubbles"] === true) {
	$("#info_tooltipModal").html("Cliquez sur les bulles « ? » foncées pour visualiser des informations additionnelles,");
	$("#info_tooltip").html("Survolez les bulles « ? » claires pour visualiser des informations additionnelles,");
} else {
	$("#info_tooltipModal").html("Cliquez sur les bulles « ? » foncées pour visualiser des informations additionnelles,");
	$("#info_tooltip").html("Survolez les bulles « ? » claires pour visualiser des informations additionnelles,");
};

/* AGS.C.01.03 : Initiate the hilighter function */

var modeHighlighter = "";

if(alreadyLoaded === false) {
	var highlightButton = document.getElementById("toggle_highlighter");
	document.getElementById("toggle_highlighter").addEventListener("click", function () {
		if (modeHighlighter === "") {
			highlightButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
			$("#toggle_highlighter > a > img").attr("src","icon/highlight_self.svg");
			$("#toggle_highlighter > a").attr("data-original-title","Mise en avant de l\'élément survolé");
			modeHighlighter = "self";
		} else {
			if (modeHighlighter === "self") {
				highlightButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
				$("#toggle_highlighter > a > img").attr("src","icon/highlight_ascendency.svg");
				$("#toggle_highlighter > a").attr("data-original-title","Mise en avant de l\'ascendance");
				modeHighlighter = "ascendency";
			} else {
				if (modeHighlighter === "ascendency") {
					highlightButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
					$("#toggle_highlighter > a > img").attr("src","icon/highlight_downdency.svg");
					$("#toggle_highlighter > a").attr("data-original-title","Mise en avant de la descendance");
					modeHighlighter = "downdency";
				} else {
					if (modeHighlighter === "downdency") {
						highlightButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
						$("#toggle_highlighter > a > img").attr("src","icon/highlight_both.svg");
						$("#toggle_highlighter > a").attr("data-original-title","Mise en avant de l\'ascendance et de la descendance");
						modeHighlighter = "both";
					} else {
						if (modeHighlighter === "both") {
							highlightButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
							$("#toggle_highlighter > a > img").attr("src","icon/highlight_self.svg");
							$("#toggle_highlighter > a").attr("data-original-title","Activer la mise en avant des éléments");
							modeHighlighter = "";
						};
					};
				};
			};
		};
	})
}

/*
--------------------------------
AGS.C.02 : Creating views buttons
--------------------------------
*/

progressBarIncrement();

/* AGS.C.02.01 : "Reload" button */

if (featuresGraph["featureReload"] === true) {
	if (featuresGraph["featureClustering"] === false) {
		$("#info_reload").css({"visibility": "hidden"});
		$("#info_reload").css({"height": "0"});
		$("#info_reload").css({"margin": "0"});
	} else {
		$("#info_reload").css({"visibility": "visible"});
		$("#info_reload").css({"height": "auto"});
		$("#info_reload").css({"margin": "0.5em 10% 0px 0px"});
	};
};

/* AGS.C.02.02 : "Group all" and "Ungroup all" buttons */

if (featuresGraph["featureClustering"] === false) {
	$("#info_clustering").css({"visibility": "hidden"});
	$("#info_clustering").css({"height": "0"});
	$("#info_clustering").css({"margin": "0"});
	$("#group_ALL").css({"display": "none"});
	$("#group_ALL_br").css({"display": "none"});
	$("#ungroup_ALL").css({"display": "none"});
	$("#ungroup_ALL_br").css({"display": "none"});
} else {
	$("#info_clustering").css({"visibility": "visible"});
	$("#info_clustering").css({"height": "auto"});
	$("#info_clustering").css({"margin": "0.5em 10% 0px 0px"});
	$("#ungroup_ALL").css({"display": "initial"});
	$("#ungroup_ALL_br").css({"display": "initial"});
	$("#group_ALL").css({"display": "initial"});
	$("#group_ALL_br").css({"display": "initial"});
};

/* AGS.C.02.03 : Link functions to the buttons */

var promise_ViewButtons = new Promise(function(resolve, reject) {
	
	var modalContent_ViewButtons = document.getElementById("appli_graph_buttons_CONTENT");

	var text_ViewButtons = "";
	
	if (viewsButtons.length > 0) {
		text_ViewButtons = text_ViewButtons.concat("<div class=\"buttons_accordion\" id=\"graph_buttons_accordion\">");
		text_ViewButtons = text_ViewButtons.concat("<div class=\"card\">");
		text_ViewButtons = text_ViewButtons.concat("<div class=\"card-header\" id=\"headingFilter\">");
		text_ViewButtons = text_ViewButtons.concat("<button class=\"btn mainViewButtons\" style=\"text-align:center;display:initial;\" data-toggle=\"collapse\" data-target=\"#collapseFilter\" aria-expanded=\"true\" aria-controls=\"collapseFilter\">");
		text_ViewButtons = text_ViewButtons.concat("<span><b>Filtres</b></span>");
		text_ViewButtons = text_ViewButtons.concat("<img class=\"lazyload\" width=\"25\" height=\"25\" data-src=\"icon/filter-solid.svg\"></img>");
		text_ViewButtons = text_ViewButtons.concat("</button>");
		text_ViewButtons = text_ViewButtons.concat("</div>");
		text_ViewButtons = text_ViewButtons.concat("<div id=\"collapseFilter\" class=\"collapse\" aria-labelledby=\"headingFilter\" data-parent=\"#graph_buttons_accordion\">");
	};

	for (i in viewsButtons) {
		text_ViewButtons = text_ViewButtons.concat("<button href=\"#\" id=","\"","viewButton_",i,"\""," type=\"button\" class=\"btn btn-primary btn_seeView mb-1\">");
		text_ViewButtons = text_ViewButtons.concat("<span>",viewsButtons[i]["title"],"</span>");
		if (viewsButtons[i]["img"] !== "") {
			text_ViewButtons = text_ViewButtons.concat("<img class=\"lazyload\" width=\"25\" height=\"25\" data-src=","\"",idGraph_path+viewsButtons[i]["img"],"\"","></img>");
		} else {
			text_ViewButtons = text_ViewButtons.concat("<svg width=\"25\" height=\"25\"><circle cx=\"50%\" cy=\"50%\" r=\"10\" fill=\""+mainColor_01+"\"/><text x=\"50%\" y=\"50%\" dominant-baseline=\"middle\" text-anchor=\"middle\" fill=\"white\">"+viewsButtons[i]["title"].slice(0,1)+"</text></svg>");
		};
		text_ViewButtons = text_ViewButtons.concat("</button>");
		text_ViewButtons = text_ViewButtons.concat("<br id=\"viewButton_BR_"+i+"\">");
	};
	
	if (viewsButtons.length > 0) {
		text_ViewButtons = text_ViewButtons.concat("</div>");
		text_ViewButtons = text_ViewButtons.concat("</div>");
		text_ViewButtons = text_ViewButtons.concat("</div>");
	};

	
	if (viewsButtons.length === 0) {
		$("#info_seeView").css({"visibility": "hidden"});
		$("#info_seeView").css({"height": "0"});
		$("#info_seeView").css({"margin": "0"});
	} else {
		$("#info_seeView").css({"visibility": "visible"});
		$("#info_seeView").css({"height": "auto"});
		$("#info_seeView").css({"margin": "0.5em 10% 0px 0px"});
	};

	modalContent_ViewButtons.innerHTML = text_ViewButtons;
	
	resolve('');
	
});

promise_ViewButtons.then(function(value) {

	for (i in viewsButtons) {
		(function () {
			var elem = document.getElementById("viewButton_" + i);
			var idTEMP = viewsButtons[i]["id"];
			var selectorTEMP = viewsButtons[i]["selector"];
			var groupTEMP = viewsButtons[i]["groups"];
			var hierarchicalTEMP = viewsButtons[i]["hierarchical"];
			elem.addEventListener("click", function () {seeView(idTEMP,selectorTEMP,groupTEMP,hierarchicalTEMP)},false);
		}());
	};

});

if (featuresGraph["featureReload"] === true) {
	if (alreadyLoaded === false) {
		document.getElementById("view_ALL").addEventListener("click", function () {seeView("","","",false)});
	};
};

if (featuresGraph["featureReload"] === false) {
	$("#view_ALL").css({"display": "none"});
	$("#view_ALL_br").css({"display": "none"});
} else {
	$("#view_ALL").css({"display": "initial"});
	$("#view_ALL_br").css({"display": "initial"});
};

/*
--------------------------------
AGS.C.03 : Set the network
--------------------------------
*/

progressBarIncrement();

/* AGS.C.03.01 : Prepare the options variable */

var options = {
	"container": document.getElementById('appli_graph_network'),
	/* "headless": true, */
	/* "styleEnabled": false, */
	"selectionType": "additive",
	"wheelSensitivity": 0.1,
	"textureOnViewport": false,
	"hideEdgesOnViewport": true
};

/* AGS.C.03.02 : Setting the options of the network (see the cytoscape documentation for more details) */

options["style"] = [

	{
		"selector": ":locked",
		"style": {
			
			"overlay-padding": 0,
			"overlay-opacity": 0
			
		}
	},
	
	{
		"selector": ".defaultNode",
		"style": {
			
			"opacity": 1,

			"width": "30px",
			"height": "30px",			

			"shape": "ellipse",
			"padding": "20px",
			
			"color": mainColor_node,
			"font-family": "Arial",
			"text-valign": "center",
			"text-wrap": "wrap",
			"text-max-width": "150px",
			
			"background-color": mainColor_04,
			"border-width": "2rem",
			"border-color" : mainColor_node
			
		}
	},

	{
		"selector": ".defaultNode[label]",
		"style": {
			
			"label": "data(label)",
			"width": "label",
			"height": "label"
			
		}
	},
	
	{
		"selector": "node:selected",
		"style": {
			"background-color": mainColor_node,
			"background-blacken": "0.25"
		}
	},
	
	{
		"selector": "node.insensitiveNode",
		"style": {
			"events": "no",
			"text-events": "no",
		}
	},
	
	{
		"selector": ".infoBubble",
		"style": {
			"label": "data(label)",
			"font-family": "Arial",
			"text-valign": "center",
			"width": "20px",
			"height": "20px",
			"padding": "5px",
			"border-width": "0rem"
		}
	},
	
	{
		"selector": "node[group = " + '"' + "ibSimple" + '"' + "]",
		"style": {
			"color": mainColor_node,
			"background-color": "white",
			"background-opacity": 0.1
		}
	},
	
	{
		"selector": "node[group = " + '"' + "ibComplete" + '"' + "]",
		"style": {
			"color": "white",
			"background-color": mainColor_node,
			"background-opacity": 1
		}
	},
	
	{
		"selector": ".infoTag",
		"style": {
			"shape": "tag",
			"label": "data(label)",
			"font-size": "10px",
			"font-family": "Arial",
			"text-valign": "center",
			"width": "label",
			"height": "10px",
			"color": "white",
			"background-color": "red",
			"padding": "5px",
			"border-width": "0rem"
		}
	},
	
	{
		"selector": ".infoTag_hidden",
		"style": {
			"visibility": "hidden"
		}
	},
	
	{
		"selector": "edge",
		"style": {
			"line-color": mainColor_node,
			"color": mainColor_node,
			"font-family": "Arial",
			"text-rotation": "autorotate",
			"text-outline-width": "3px",
			"text-outline-color": mainColor_03,
			"source-arrow-color": mainColor_node,
			"target-arrow-color": mainColor_node,
			"width": "2rem"
		}
	},
	
	{
		"selector": "edge:selected",
		"style": {
			"line-color": mainColor_02,
			"source-arrow-color": mainColor_02,
			"target-arrow-color": mainColor_02,
			"width": "4rem"
		}
	},

	{
		"selector": "edge.indirectEdge",
		"style": {
			"opacity": 0.5,
			"line-style": "dashed"
		}
	},
	
	{
		"selector": "edge.indirectEdge_hidden",
		"style": {
			"visibility": "hidden"
		}
	},
	
	{
		"selector": "edge.insensitiveEdge",
		"style": {
			"events": "no",
			"text-events": "no",
		}
	}
		
];

/* AGS.C.03.03 : Create the network */

cy = cytoscape(options);

/* AGS.C.03.04 : Changing the options if the graph is static */

if (featuresGraph["static"]) {
	options["style"] = options["style"].concat([
		{
			"selector": "edge",
			"style": {
				"target-arrow-color": mainColor_node,
				"target-arrow-shape": "triangle",
				"curve-style": "straight",
				"source-distance-from-node": "5px",
				"target-distance-from-node": "5px"
			}
		},
		{
			"selector": "edge:selected",
			"style": {
				"target-arrow-color": mainColor_02
			}
		}
	]);
	cy.style(options["style"]);
};

/* AGS.C.03.05 : Changing the options if specified in the config.js file */

if (options_Modifier.length > 0) {
	options["style"] = options["style"].concat(options_Modifier);
	cy.style(options["style"]);
};

/* AGS.C.03.06 : Changing the groups styles if specified in the config.js files */

for (i_group in options_Groups) {
	options["style"].push(options_Groups[i_group]);
	cy.style(options["style"]);
};

/* AGS.C.03.07 : Initial layout */

var mainLayout = cy.elements().layout({"name": "null"});;

/* AGS.C.03.08 : Deal with state of buttons */

var tagsButton = document.getElementById("toggle_tags");

if ($("#toggle_tags").css("display") !== "none") {
	tagsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
	$("#toggle_tags > a").attr("data-original-title","Ne plus afficher les étiquettes");
};

if (alreadyLoaded === false) {
	tagsButton.addEventListener("click",function() {
		if (cy.$(".infoTag_hidden").length > 0) {
			cy.$(".infoTag_hidden").removeClass("infoTag_hidden");
			tagsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
			$("#toggle_tags > a").attr("data-original-title","Ne plus afficher les étiquettes");
		} else {
			cy.$(".infoTag").addClass("infoTag_hidden");
			tagsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
			$("#toggle_tags > a").attr("data-original-title","Afficher les étiquettes");
		};
	});
};

var physicsButton = document.getElementById("toggle_physics");

if ($("#toggle_physics").css("display") !== "none") {
	if (featuresGraph["physics"] === true) {
		physicsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
		$("#toggle_physics > a").attr("data-original-title","Physique activée");
	} else {
		physicsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
		$("#toggle_physics > a").attr("data-original-title","Physique désactivée");
	};
};

if (alreadyLoaded === false) {
	physicsButton.addEventListener("click",function() {
		if (featuresGraph["physics"] === false) {
			featuresGraph["physics"] = true;
			physicsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
			$("#toggle_physics > a").attr("data-original-title","Physique activée");
		} else {
			featuresGraph["physics"] = false;
			physicsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
			$("#toggle_physics > a").attr("data-original-title","Physique désactivée");
		};
		InitialLayout("euler_cola",false);
	});
};

var indirectLinksButton = document.getElementById("toggle_ind_link");

if ($("#toggle_ind_link").css("display") !== "none") {
	indirectLinksButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
	$("#toggle_ind_link > a").attr("data-original-title","Afficher les liens indirects");
};

if (alreadyLoaded === false) {
	indirectLinksButton.addEventListener("click",function() {
		if ($("#toggle_ind_link").css("background-color").indexOf("0.5") === -1) {
			cy.$(".indirectEdge").toggleClass("indirectEdge_hidden",false);
			indirectLinksButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
			$("#toggle_ind_link > a").attr("data-original-title","Masquer les liens indirects");
		} else {
			cy.$(".indirectEdge").toggleClass("indirectEdge_hidden",true);
			indirectLinksButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
			$("#toggle_ind_link > a").attr("data-original-title","Afficher les liens indirects");
		};
	});
};

var highlightButton = document.getElementById("toggle_highlighter");

if ($("#toggle_highlighter").css("display") !== "none") {
	highlightButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
	$("#toggle_highlighter > a > img").attr("src","icon/highlight_self.svg");
	$("#toggle_highlighter > a").attr("data-original-title","Activer la mise en avant des éléments");
	modeHighlighter = "";
};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
AGS.D : LAUNCHING PROMISES
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
AGS.D.01 : Data promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		progressBarReinitialize("Chargement des données",4,false);
		progressBarIncrement();
		
		/* AGS.D.01.01 : Upload custom or password data */
		if (typeof dataGraph_GET === typeof {}) {
			
			var switchModeDone = false;
			
			/* ___CUSTOM_TEMPLATES___ mode */
			if (Object.keys(dataGraph_GET)[0] === "___CUSTOM_TEMPLATES___") {
				switchModeDone = true;
				$("#uploadFile").css("display","initial");
				$("#uploadModal").modal("show");
				if (alreadyLoaded === false) {
					const typeSelector = document.getElementById("select_data_type");
					const fileSelectorNodes = document.getElementById("uploadFileNodes");
					const fileSelectorEdges = document.getElementById("uploadFileEdges");
					const descriptorSelector = document.getElementById("upload_information");
					const specsSelector = document.getElementById("upload_specs");
					typeSelector.addEventListener("change", (event) => {
						$("#upload_file").css("display","initial");
						$("#upload_information").css("display","initial");
						$("#upload_button_submit").css("display","none");
						/* if (typeSelector.value === "json_advanced") {fileSelector.accept = ".json"}; */
						/* if (typeSelector.value === "json_simple") {fileSelector.accept = ".json"}; */
						/* if (typeSelector.value === "json") {fileSelector.accept = ".json"}; */
						if (typeSelector.value === "csv") {
							fileSelectorNodes.accept = ".csv";
							fileSelectorEdges.accept = ".csv";
							descriptorSelector.innerHTML = "<p>Le tableur doit être encodé en UTF-8. Si la documentation du gabarit n\'indique pas de format d\'utilisation de cette donnée, la première colonne sera utilisée pour nommer les éléments et les autres seront compilées dans la description des élément.</p><p>Un tableur additionnel listant les liens entre les élémenst peut être également chargé. Si la documentation du gabarit n\'indique pas de format d\'utilisation de cette donnée, il est nécessaire que ce tableur comporte un colonne \"from\" pour indiquer les origines des liens et une colonne \"to\" pour en indiquer les destinations.</p>";
						};
						if (dataGraph_GET["___CUSTOM_TEMPLATES___"]["specs"] !== "") {
							$("#upload_specs").css("display","initial");
							specsSelector.innerHTML = dataGraph_GET["___CUSTOM_TEMPLATES___"]["specs"];
						};
						/* if (typeSelector.value === "php") {fileSelector.accept = ".txt"}; */
						fileSelectorNodes.value = "";
						fileSelectorEdges.value = "";
					});
					fileSelectorNodes.addEventListener("change", (event) => {
						$("#upload_button_submit").css("display","initial");
					});
					document.getElementById("upload_button_submit").addEventListener("click",function () {
						var fileReaderNodes = new FileReader();
						fileReaderNodes.onload = function () {
							let dataNodes = fileReaderNodes.result;
							let configNodes = dataGraph_GET["___CUSTOM_TEMPLATES___"]["config"];
							$("#uploadModal").modal("hide");
							if (fileSelectorEdges.files.length === 0) {
								dataGraph_GET = {"csv": ["alreadyLoaded"]};
								resolve_ScriptInitialisation([{"data": dataNodes, "config": configNodes}]);
							} else {
								var fileReaderEdges = new FileReader();
								fileReaderEdges.onload = function () {
									let dataEdges = fileReaderEdges.result;
									let configEdges = dataGraph_GET["___CUSTOM_TEMPLATES___"]["config"];
									dataGraph_GET = {"csv": ["alreadyLoaded"]};
									resolve_ScriptInitialisation([
										{"nodes": dataNodes, "config": configNodes},
										{"edges": dataEdges, "config": configEdges}
									]);
								};
								fileReaderEdges.readAsText($('#uploadFileEdges').prop('files')[0]);
							};
						};
						fileReaderNodes.readAsText($('#uploadFileNodes').prop('files')[0]);
					});
				};
			};
			
			/* ___PASSWORD_TEMPLATES___ mode */
			if (Object.keys(dataGraph_GET)[0] === "___PASSWORD_TEMPLATES___") {
				switchModeDone = true;
				$("#passwordFile").css("display","initial");
				$("#password_information").css("display","initial");
				$("#password_button_submit").css("display","initial");
				if (getParamValue("passwd") === undefined) {
					$("#uploadModal").modal("show");
				};
				const passwordSelector = document.getElementById("form_password_file_input");
				const passwordInformation = document.getElementById("password_information");
				const specsSelector = document.getElementById("password_specs");
				if (dataGraph_GET["___PASSWORD_TEMPLATES___"]["specs"] !== "") {
					$("#password_specs").css("display","initial");
					specsSelector.innerHTML = dataGraph_GET["___PASSWORD_TEMPLATES___"]["specs"];
				};
				if (alreadyLoaded === false) {
					passwordSelector.addEventListener("keydown", function(event) {
					  if (event.keyCode === 13) {
						event.preventDefault();
						document.getElementById("password_button_submit").click();
					  };
					}); 
					document.getElementById("password_button_submit").addEventListener("click",function () {
						if (passwordSelector.value === "") {
							passwordInformation.innerHTML = "Attention aucun mot de passe saisi !";
						} else {
							var oReq = new XMLHttpRequest();
							oReq.onload = function() {
								if (this.response === "error") {
									$("#uploadModal").modal("show");
									passwordInformation.innerHTML = "Mot de passe incorrect !";
								} else {
									if (this.response === "wrong_template") {
										$("#uploadModal").modal("show");
										passwordInformation.innerHTML = "Le gabarit que vous utilisez n\'est pas adapté à ces données.";
									} else {
										var config = dataGraph_GET["___PASSWORD_TEMPLATES___"]["config"];
										dataGraph_GET = {"csv": ["alreadyLoaded"]};
										$("#uploadModal").modal("hide");
										var dataResponse = this.response;
										resolve_ScriptInitialisation([{"data": dataResponse, "config": config}]);
									};
								};
							};
							oReq.open("get", "php/_password_data.php?passwd="+passwordSelector.value+"&template="+nameGraph+"&___VERSION_CACHE___", true);
							oReq.send();
						};
					});
				};
				if (getParamValue("passwd") !== undefined) {
					passwordSelector.value = getParamValue("passwd");
					document.getElementById("password_button_submit").click();
				};
			};

			/* ___SEMAPPS_TEMPLATES___ mode */
			if (Object.keys(dataGraph_GET)[0] === "___SEMAPPS_TEMPLATES___") {
				switchModeDone = true;
				$("#semappsFile").css("display","initial");
				$("#semapps_information").css("display","initial");
				$("#semapps_button_submit").css("display","initial");
				if ((getParamValue("semapps") === undefined) && (dataGraph_GET["___SEMAPPS_TEMPLATES___"]["url"] === "")) {
					$("#uploadModal").modal("show");
				};
				const semappsSelector = document.getElementById("form_semapps_file_input");
				const semappsInformation = document.getElementById("semapps_information");
				const specsSelector = document.getElementById("semapps_specs");
				if (dataGraph_GET["___SEMAPPS_TEMPLATES___"]["specs"] !== "") {
					$("#semapps_specs").css("display","initial");
					specsSelector.innerHTML = dataGraph_GET["___SEMAPPS_TEMPLATES___"]["specs"];
				};
				if (alreadyLoaded === false) {
					semappsSelector.addEventListener("keydown", function(event) {
					  if (event.keyCode === 13) {
						event.preventDefault();
						document.getElementById("semapps_button_submit").click();
					  };
					}); 
					document.getElementById("semapps_button_submit").addEventListener("click",function () {
						if (semappsSelector.value === "") {
							semappsInformation.innerHTML = "Attention aucune adresse url saisie !";
						} else {
							let chain_SemAppsData = Promise.resolve();
							chain_SemAppsData = chain_SemAppsData.then(F_ScriptLoading.bind(null,"dist/semapps/jsonld/5.0.0/jsonld.esm.min.js"+"?___VERSION_CACHE___"));
							chain_SemAppsData = chain_SemAppsData.then(F_ScriptLoading.bind(null,"dist/semapps/ouiz/start_ouiz.js"+"?___VERSION_CACHE___",false,true));
							chain_SemAppsData = chain_SemAppsData.then(function(){
								return new Promise((resolve_SemAppsData) => {
									if ((dataGraph_GET["___SEMAPPS_TEMPLATES___"]["config"] !== "") && (dataGraph_GET["___SEMAPPS_TEMPLATES___"]["config"] !== undefined)) {
										function processData(allText) {
											let semAppsConfig = JSON.parse(allText.replace(/(\r\n|\n|\r|\t)/gm,""));
											resolve_SemAppsData(semAppsConfig);
										};
										$.ajax({
											type: "GET",
											url: idGraph_path+dataGraph_GET["___SEMAPPS_TEMPLATES___"]["config"]+"?___VERSION_CACHE___",
											dataType: "text",
											success: function(data) {processData(data);}
										});
									} else {
										resolve_SemAppsData("");
									};
								});
							});
							chain_SemAppsData = chain_SemAppsData.then(function(value_SemAppsData){
								return new Promise((resolve_SemAppsData) => {
									let modeSemApps;
									let whitelistSemApps;
									let blacklistSemApps;
									if (value_SemAppsData === "") {
										modeSemApps = "whitelist";
										whitelistSemApps = [];
									} else {
										modeSemApps = value_SemAppsData["mode"];
										whitelistSemApps = value_SemAppsData["whitelist"];
										blacklistSemApps = value_SemAppsData["blacklist"];
									};
									async function loadData(url){
										await ouiz.reset();
										await ouiz.load(url);
										if (modeSemApps === "whitelist") {await ouiz.toCytoscape("whitelist",whitelistSemApps);};
										if (modeSemApps === "blacklist") {await ouiz.toCytoscape("blacklist",blacklistSemApps);};
										dataGraph_GET = ouiz.network;
										resolve_ScriptInitialisation();
										resolve_SemAppsData("");
										$("#uploadModal").modal("hide");
									};
									loadData(semappsSelector.value);
								});
							});
						};
					});
				};
				if (getParamValue("semapps") !== undefined) {
					semappsSelector.value = getParamValue("passwd");
					document.getElementById("semapps_button_submit").click();
				} else {
					if (dataGraph_GET["___SEMAPPS_TEMPLATES___"]["url"] !== "") {
						semappsSelector.value = dataGraph_GET["___SEMAPPS_TEMPLATES___"]["url"];
						document.getElementById("semapps_button_submit").click();
					};
				};
			};

			/* ___SEMAPPS_TEMPLATES_BULK___ mode */
			if (Object.keys(dataGraph_GET)[0] === "___SEMAPPS_TEMPLATES_BULK___") {
				switchModeDone = true;
				if (alreadyLoaded === false) {
					let dataResponse = [];
					let dataTraductor = {};
					let bulk_SemAppsCounter = 0;
					/*
					async function loadData(url_f,modeSemApps_f,whitelistSemApps_f,blacklistSemApps_f,traductorSemApps_f){
						await ouiz.reset();
						await ouiz.load(url_f);
						if (modeSemApps_f === "whitelist") {await ouiz.toCytoscape("whitelist",whitelistSemApps_f,traductorSemApps_f);};
						if (modeSemApps_f === "blacklist") {await ouiz.toCytoscape("blacklist",blacklistSemApps_f,traductorSemApps_f);};
						bulk_SemAppsCounter += 1;
						if (bulk_SemAppsCounter === dataGraph_GET["___SEMAPPS_TEMPLATES_BULK___"]["urls"].length) {
							var config = dataGraph_GET["___SEMAPPS_TEMPLATES_BULK___"]["mainConfig"];
							dataGraph_GET = {"csv": ["alreadyLoaded"]};
							resolve_ScriptInitialisation([{"data": ouiz.network, "config": config}]);
						};
					};
					*/
					function loadData_Light(key_f,url_f,traductorSemApps_f){
						return new Promise((resolve) => {
							function processData(allText) {
								let dataSemApps = JSON.parse(allText.replace(/(\r\n|\n|\r|\t)/gm,""));
								if (key_f === "traductor") {
									if (traductorSemApps_f !== undefined) {
										dataSemApps["ldp:contains"].forEach(function (itemSemApps) {
											dataTraductor[itemSemApps[traductorSemApps_f[0]]] = itemSemApps[traductorSemApps_f[1]];
										});
									};
								} else {
									if (key_f === "dict") {
										let listToRetrieve = [];
										dataSemApps["ldp:contains"].forEach(function (itemSemApps) {
											let attributesToRetrieve = {};
											for (itemConfig in traductorSemApps_f) {
												attributesToRetrieve[traductorSemApps_f[itemConfig]] = itemSemApps[traductorSemApps_f[itemConfig]];
											};
											listToRetrieve.push(attributesToRetrieve);
										});
										dictSemApps[url_f] = listToRetrieve;
									} else {
										if (Object.keys(dataTraductor).length > 0) {
											dataSemApps["ldp:contains"].forEach(function (itemSemApps) {
												for (keySemApps in itemSemApps) {
													if (typeof(itemSemApps[keySemApps]) === typeof([])) {
														for (i_itemSemApps in itemSemApps[keySemApps]) {
															if (dataTraductor[itemSemApps[keySemApps][i_itemSemApps]] !== undefined) {
																itemSemApps[keySemApps][i_itemSemApps] = dataTraductor[itemSemApps[keySemApps][i_itemSemApps]];
															};
														};
													} else {
														if (dataTraductor[itemSemApps[keySemApps]] !== undefined) {
															itemSemApps[keySemApps] = dataTraductor[itemSemApps[keySemApps]];
														};
													};
												};
											});
										};
										if (traductorSemApps_f !== undefined) {
											dataSemApps["ldp:contains"].forEach(function (itemSemApps) {
												for (keySemApps in itemSemApps) {
													if (traductorSemApps_f[keySemApps] !== keySemApps) {
														if (traductorSemApps_f[keySemApps] !== undefined) {
															if (!(Object.keys(itemSemApps[keySemApps]).length === 0 && itemSemApps[keySemApps].constructor === Object)) {
																if (Array.isArray(itemSemApps[keySemApps])) {
																	itemSemApps[keySemApps] = itemSemApps[keySemApps].filter(function (e) {
																		return !(Object.keys(e).length === 0 && e.constructor === Object);
																	});
																};
																if (!(traductorSemApps_f[keySemApps] in itemSemApps)) {
																	itemSemApps[traductorSemApps_f[keySemApps]] = itemSemApps[keySemApps];
																} else {
																	if (Array.isArray(itemSemApps[traductorSemApps_f[keySemApps]])) {
																		if (Array.isArray(itemSemApps[keySemApps])) {
																			for (i_itemSemApps in itemSemApps[keySemApps]) {
																				itemSemApps[traductorSemApps_f[keySemApps]].push(itemSemApps[keySemApps][i_itemSemApps]);
																			};
																		} else {
																			itemSemApps[traductorSemApps_f[keySemApps]].push(itemSemApps[keySemApps]);
																		};	
																	} else {
																		if (Array.isArray(itemSemApps[keySemApps])) {
																			itemSemApps[traductorSemApps_f[keySemApps]] = [itemSemApps[traductorSemApps_f[keySemApps]]];
																			for (i_itemSemApps in itemSemApps[keySemApps]) {
																				itemSemApps[traductorSemApps_f[keySemApps]].push(itemSemApps[keySemApps][i_itemSemApps]);
																			};
																		} else {
																			itemSemApps[traductorSemApps_f[keySemApps]] = [itemSemApps[traductorSemApps_f[keySemApps]],itemSemApps[keySemApps]];
																		};
																	};
																};
															};
															delete itemSemApps[keySemApps];
														};
													};
												};
											});
										};
										let tempDict = {};
										tempDict[key_f] = dataSemApps["ldp:contains"];
										tempDict["config"] = dataGraph_GET["___SEMAPPS_TEMPLATES_BULK___"]["mainConfig"];
										dataResponse.push(tempDict);
									};
								};
								bulk_SemAppsCounter += 1;
								if (bulk_SemAppsCounter === dataGraph_GET["___SEMAPPS_TEMPLATES_BULK___"]["urls"].length) {
									dataGraph_GET = {"semapps": "alreadyLoaded"};
									resolve_ScriptInitialisation(dataResponse);
								};
								resolve("Done");
							};
							$.ajax({
								type: "GET",
								url: url_f,
								dataType: "text",
								error: function() {
									$("#bug_report_appli").html("<p><b>/!\\</b></p><p><b>La base de données SemApps est inacessible. Réessayez plus tard ou contactez l\'administrateur de l\'application.</b></p><p><b>/!\\</b></p>"); 
									$("#bugModal").attr("data-backdrop","static"); 
									$("#bugModal").modal("show"); 
									$("#bugModal_QuitButton").remove(); 
								},
								success: function(data) {processData(data);},
								beforeSend : function() {progressBarSetText("Chargement des données : récupération SemApps (/"+url_f.substring(url_f.lastIndexOf("/")+1)+")");}
							});
						});	
					};

					let chain_SemAppsData = Promise.resolve();
					chain_SemAppsData = chain_SemAppsData.then(F_ScriptLoading.bind(null,"dist/semapps/jsonld/5.0.0/jsonld.esm.min.js"+"?___VERSION_CACHE___"));
					chain_SemAppsData = chain_SemAppsData.then(F_ScriptLoading.bind(null,"dist/semapps/ouiz/start_ouiz.js"+"?___VERSION_CACHE___",false,true));
					dataGraph_GET["___SEMAPPS_TEMPLATES_BULK___"]["urls"].forEach(function (url_SemApps) {
						let tempSemAppsToLoad = url_SemApps;
						chain_SemAppsData = chain_SemAppsData.then(function(){
							return new Promise((resolve_SemAppsData) => {
								let tempSemAppsToLoad_FIX = tempSemAppsToLoad;
								if ((tempSemAppsToLoad_FIX["config"] !== "") && (tempSemAppsToLoad_FIX["config"] !== undefined) && (typeof(tempSemAppsToLoad_FIX["config"]) !== typeof([]))) {
									function processData(allText) {
										let semAppsConfig = JSON.parse(allText.replace(/(\r\n|\n|\r|\t)/gm,""));
										resolve_SemAppsData(semAppsConfig);
									};
									$.ajax({
										type: "GET",
										url: idGraph_path+tempSemAppsToLoad_FIX["config"]+"?___VERSION_CACHE___",
										dataType: "text",
										success: function(data) {processData(data);}
									});
								} else {
									if (typeof(tempSemAppsToLoad_FIX["config"]) === typeof([])) {
										resolve_SemAppsData({"traductor": tempSemAppsToLoad_FIX["config"]});
									} else {
										resolve_SemAppsData("");
									};
								};
							});
						});
						chain_SemAppsData = chain_SemAppsData.then(function(value_SemAppsData){
							return new Promise((resolve_SemAppsData) => {
								let tempSemAppsToLoad_FIX = tempSemAppsToLoad;
								let modeSemApps;
								let whitelistSemApps;
								let blacklistSemApps;
								let traductorSemApps;
								if (value_SemAppsData === "") {
									modeSemApps = "whitelist";
									whitelistSemApps = [];
									blacklistSemApps = [];
									traductorSemApps = {};
								} else {
									modeSemApps = value_SemAppsData["mode"];
									whitelistSemApps = value_SemAppsData["whitelist"];
									blacklistSemApps = value_SemAppsData["blacklist"];
									traductorSemApps = value_SemAppsData["traductor"]
								};
								if (Object.keys(value_SemAppsData)[0] === "traductor") {traductorSemApps = value_SemAppsData["traductor"]};
								/* resolve_SemAppsData(loadData(tempSemAppsToLoad_FIX[Object.keys(tempSemAppsToLoad_FIX)[0]],modeSemApps,whitelistSemApps,blacklistSemApps,traductorSemApps)); */
								resolve_SemAppsData(loadData_Light(Object.keys(tempSemAppsToLoad_FIX)[0],tempSemAppsToLoad_FIX[Object.keys(tempSemAppsToLoad_FIX)[0]],traductorSemApps));
							});
						});
					});
				};
			};
			
			if (switchModeDone === false) {
				resolve_ScriptInitialisation("Done");
			};
			
		} else {
			resolve_ScriptInitialisation("Done");
		};
		
	});
});

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(data_loaded){
	return new Promise((resolve_ScriptInitialisation) => {
		progressBarSetText("Chargement des données : mise en forme");
		
		/* AGS.D.01.02 : Loading the datas */
		var promise_DataPHP = new Promise(function(resolvePHP, rejectPHP) {
			progressBarIncrement();
			
			/* Mode Native */
			if (Array.isArray(dataGraph_GET)) {
				resolvePHP({"jsData": dataGraph_GET});
			};
			
			/* Mode PHP */
			if (typeof dataGraph_GET === typeof "") {
				if (dataGraph_GET.indexOf(".php") !== -1) {
					var oReq = new XMLHttpRequest();
					oReq.onload = function() {
						resolvePHP({"phpData": this.response});
					};
					oReq.open("get", idGraph_path+dataGraph_GET+"?___VERSION_CACHE___", true);
					oReq.send();
				};
			};
			
			/* Mode CSV */
			if (typeof dataGraph_GET === typeof {}) {
				/* Native workaround */
				if ((Object.keys(dataGraph_GET)[0] === "nodes") && (Object.keys(dataGraph_GET)[1] === "edges")) {
					resolvePHP({"jsData": dataGraph_GET});
				};
				/* SemApps workaround */
				if (dataGraph_GET["semapps"] === "alreadyLoaded") {
					resolvePHP({"multiFormatData": data_loaded});
				};
				/* True CSV */
				if (Object.keys(dataGraph_GET)[0] === "csv") {
					var dataCounter = 1;
					var output_data = [];
					for (data_to_load in dataGraph_GET["csv"]) {
						/* Load de csv reading module if needed */
						if (!modulesList_AlreadyLoaded.includes("dist/jquery/_plugins/jquery.csv.min.js")) {
							chain_LoadData = chain_LoadData.then(F_ScriptLoading.bind(null,"dist/jquery/_plugins/jquery.csv.min.js"));
							modulesList_AlreadyLoaded = modulesList_AlreadyLoaded.concat("dist/jquery/_plugins/jquery.csv.min.js");
						};
						/* Get the data */
						chain_LoadData = chain_LoadData.then(function(){
							return new Promise((resolve_LoadData) => {
								let data_to_load_CURRENT = dataCounter-1;
								if (dataGraph_GET["csv"][data_to_load_CURRENT] !== "alreadyLoaded") {
									let keyDataToUse = Object.keys(dataGraph_GET["csv"][data_to_load_CURRENT])[0];
									$.ajax({
										type: "GET",
										url: idGraph_path+dataGraph_GET["csv"][data_to_load_CURRENT][keyDataToUse]+"?___VERSION_CACHE___",
										dataType: "text",
										success: function(data) {processData(data);}
									});
									function processData(allText) {
										let csv_raw = allText;
										let itemDataTemp = {};
										itemDataTemp[keyDataToUse] = $.csv.toObjects(csv_raw);
										itemDataTemp["config"] = dataGraph_GET["csv"][data_to_load_CURRENT]["config"];
										output_data.push(itemDataTemp);
										resolve_LoadData("Done");
										if (dataCounter === dataGraph_GET["csv"].length) {resolvePHP({"multiFormatData": output_data});};
										dataCounter = dataCounter + 1;
									};
								} else {
									if (data_loaded.length === 1) {
										output_data.push({
											"data": $.csv.toObjects(data_loaded[0]["data"]),
											"config": data_loaded[0]["config"]
										});
									} else {
										output_data.push({
											"nodes": $.csv.toObjects(data_loaded[0]["nodes"]),
											"config": data_loaded[0]["config"]
										});
										output_data.push({
											"edges": $.csv.toObjects(data_loaded[1]["edges"]),
											"config": data_loaded[1]["config"]
										});
									};
									resolve_LoadData("Done");
									if (dataCounter === dataGraph_GET["csv"].length) {resolvePHP({"multiFormatData": output_data});};
									dataCounter = dataCounter + 1;
								};
							});
						});
					};
				};
			};
			
		});
		
		/* AGS.D.01.03 : Preparing the datas of the network */
		promise_DataPHP.then(function(value) {
			progressBarIncrement();
			
			/* Mode Native */
			if (value.jsData !== undefined) {
				chain_LoadData = chain_LoadData.then(function(){
					return new Promise((resolve_LoadData) => {
						dataGraph = value.jsData;
						resolve_LoadData(dataGraph);
					});
				});
			};
			
			/* Mode PHP */
			if (value.phpData !== undefined) {
				chain_LoadData = chain_LoadData.then(function(){
					return new Promise((resolve_LoadData) => {
						var phpTEXT = value.phpData;
						dataGraph = JSON.parse(phpTEXT.replace(/(\r\n|\n|\r|\t)/gm,""));
						resolve_LoadData(dataGraph);
					});
				});
			};
			
			/* Mode MULTI FORMAT */
			if (value.multiFormatData !== undefined) {
				let dataCounter = 1;
				let dataGraph_BIS = {"nodes": [], "edges": []};
				for (config_to_load_NUM in value.multiFormatData) {
					chain_LoadData = chain_LoadData.then(function(){
						return new Promise((resolve_LoadData) => {
							let config_to_load = dataCounter-1;
							if (value.multiFormatData[config_to_load]["config"] !== "") {
								
								/* Function to read the config json associate to the data */
								function processData(allText) {
									/* Make a json from data */
									var csvConfig = JSON.parse(allText.replace(/(\r\n|\n|\r|\t)/gm,""));
									/* For each config field push the dataGraph variable */
									if (("data" in value.multiFormatData[config_to_load]) || ("nodes" in value.multiFormatData[config_to_load])) {
										let keyDataToUse;
										if ("data" in value.multiFormatData[config_to_load]) {keyDataToUse = "data";};
										if ("nodes" in value.multiFormatData[config_to_load]) {keyDataToUse = "nodes";};
										for (csvLine in value.multiFormatData[config_to_load][keyDataToUse]) {
											var lineTEMP = {};
											/* nativeFields */
											for (elemConfig in csvConfig["nativeFields"]) {
												if ((csvConfig["nativeFields"][elemConfig] === "") && (elemConfig === "id")) {
													lineTEMP["id"] = "id_"+csvLine;
												};
												if (csvConfig["nativeFields"][elemConfig] !== "") {
													lineTEMP[elemConfig] = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["nativeFields"][elemConfig]];
												};
											};
											/* sourcesNodesFields */
											var sourceFieldsTEMP = [];
											for (elemConfig in csvConfig["sourcesNodesFields"]) {
												var sourceTemp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["sourcesNodesFields"][elemConfig]];
												if (sourceTemp !== "") {sourceFieldsTEMP.push(sourceTemp);};
											};
											if (sourceFieldsTEMP.length > 0) {lineTEMP["sourcesNodes"] = sourceFieldsTEMP;};
											/* dataFields and positionFields */
											var dataFieldsTEMP = {};
											for (elemConfig in csvConfig["dataFields"]) {
												dataFieldsTEMP[csvConfig["dataFields"][elemConfig]] = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["dataFields"][elemConfig]];
											};
											if ((csvConfig["coordFields"]["lng"] !== "") && (csvConfig["coordFields"]["lat"] !== "")) {
												dataFieldsTEMP["lng"] = parseFloat(value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["coordFields"]["lng"]]);
												dataFieldsTEMP["lat"] = parseFloat(value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["coordFields"]["lat"]]);
											};
											if (dataFieldsTEMP !== {}) {
												lineTEMP["additionalData"] = dataFieldsTEMP;
											};
											/* titleFields */
											var titleCompleteTEMP = "";
											for (elemConfig in csvConfig["titleFields"]) {
												if (typeof(csvConfig["titleFields"][elemConfig]) === typeof({})) {
													var title_tmp = csvConfig["titleFields"][elemConfig][1];
													var value_tmp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["titleFields"][elemConfig][0]];
												} else {
													var title_tmp = csvConfig["titleFields"][elemConfig];
													var value_tmp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["titleFields"][elemConfig]];
												};
												titleCompleteTEMP = titleCompleteTEMP + "<p><b>" + title_tmp + " : </b>" + value_tmp + "</p>";
											};
											if (lineTEMP["titleComplete"] === undefined) {
												if (titleCompleteTEMP !== "") {
													lineTEMP["titleComplete"] = titleCompleteTEMP;
												};
											} else {
												lineTEMP["titleComplete"] = lineTEMP["titleComplete"] + "<br><br>" + titleCompleteTEMP;
											};
											var searchTagTEMP = {};
											/* tagFields */
											for (elemConfig in csvConfig["tagFields"]) {
												if (typeof(csvConfig["tagFields"][elemConfig]) === typeof({})) {
													let valueTagTemp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["tagFields"][elemConfig][0]];
													if (typeof(valueTagTemp) === typeof([])) {
														searchTagTEMP[csvConfig["tagFields"][elemConfig][1]] = valueTagTemp;
													} else {
														searchTagTEMP[csvConfig["tagFields"][elemConfig][1]] = [valueTagTemp];
													};
												} else {
													let valueTagTemp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["tagFields"][elemConfig]];
													if (typeof(valueTagTemp) === typeof([])) {
														searchTagTEMP[csvConfig["tagFields"][elemConfig]] = valueTagTemp;
													} else {
														searchTagTEMP[csvConfig["tagFields"][elemConfig]] = [valueTagTemp];
													};
												};
											};
											if (Object.keys(searchTagTEMP).length > 0) {
												lineTEMP["searchTag"] = searchTagTEMP;
											};
											/* positionFields */
											if ((csvConfig["positionFields"]["x"] !== "") && (csvConfig["positionFields"]["y"] !== "")) {
												dataGraph_POSITIONS[lineTEMP["id"]] = {
													"x": parseFloat(value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["positionFields"]["x"]]),
													"y": parseFloat(value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig["positionFields"]["y"]])
												};
											};
											dataGraph_BIS["nodes"].push(lineTEMP);
										};
									};
									if ("edges" in value.multiFormatData[config_to_load]) {
										for (csvLine in value.multiFormatData[config_to_load]["edges"]) {
											var lineTEMP = {};
											/* edgesNativeFields */
											for (elemConfig in csvConfig["edgesNativeFields"]) {
												if ((csvConfig["edgesNativeFields"][elemConfig] === "") && (elemConfig === "edge_id")) {
													lineTEMP["id"] = "id_"+csvLine;
												};
												if (csvConfig["edgesNativeFields"][elemConfig] !== "") {
													lineTEMP[elemConfig] = value.multiFormatData[config_to_load]["edges"][csvLine][csvConfig["edgesNativeFields"][elemConfig]];
												};
											};
											/* edgesDataFields */
											var dataFieldsTEMP = {};
											for (elemConfig in csvConfig["edgesDataFields"]) {
												dataFieldsTEMP[csvConfig["edgesDataFields"][elemConfig]] = value.multiFormatData[config_to_load]["edges"][csvLine][csvConfig["edgesDataFields"][elemConfig]];
											};
											if (dataFieldsTEMP !== {}) {
												lineTEMP["data"] = dataFieldsTEMP;
											};
											/* edgeStyleFields */
											var sourceFieldsTEMP = {};
											for (elemConfig in csvConfig["edgeStyleFields"]) {
												if (typeof(csvConfig["edgeStyleFields"][elemConfig]) === typeof({})) {
													sourceFieldsTEMP[csvConfig["edgeStyleFields"][elemConfig][1]] = value.multiFormatData[config_to_load]["edges"][csvLine][csvConfig["edgeStyleFields"][elemConfig][0]];
												} else {
													sourceFieldsTEMP[csvConfig["edgeStyleFields"][elemConfig]] = value.multiFormatData[config_to_load]["edges"][csvLine][csvConfig["edgeStyleFields"][elemConfig]];
												};
											};
											if (sourceFieldsTEMP !== {}) {
												lineTEMP["style"] = sourceFieldsTEMP;
											};
											/* push step */
											dataGraph_BIS["edges"].push(lineTEMP);
										};
									};
									if (dataCounter === value.multiFormatData.length) {
										resolve_LoadData(dataGraph_BIS);
									} else {
										resolve_LoadData("NextData");
									};
									dataCounter = dataCounter + 1;
								};
								/* Load the config json associate to the data */
								$.ajax({
									type: "GET",
									url: idGraph_path+value.multiFormatData[config_to_load]["config"]+"?___VERSION_CACHE___",
									dataType: "text",
									success: function(data) {processData(data);}
								});
								
							} else {
								
								var switchLine = 0;
								if (("data" in value.multiFormatData[config_to_load]) || ("nodes" in value.multiFormatData[config_to_load])) {
									let keyDataToUse;
									if ("data" in value.multiFormatData[config_to_load]) {keyDataToUse = "data";};
									if ("nodes" in value.multiFormatData[config_to_load]) {keyDataToUse = "nodes";};
									for (csvLine in value.multiFormatData[config_to_load][keyDataToUse]) {
										var lineTEMP = {};
										var dataFieldsTEMP = {};
										var titleCompleteTEMP = "";
										for(lineCell in value.multiFormatData[config_to_load][keyDataToUse][csvLine]) {
											if (switchLine === 0) {
												lineTEMP["id"] = value.multiFormatData[config_to_load][keyDataToUse][csvLine][lineCell];
												lineTEMP["label"] = value.multiFormatData[config_to_load][keyDataToUse][csvLine][lineCell];
											} else {
												dataFieldsTEMP[lineCell] = value.multiFormatData[config_to_load][keyDataToUse][csvLine][lineCell];
												titleCompleteTEMP = titleCompleteTEMP + "<p><b>" + lineCell + " : </b>" + value.multiFormatData[config_to_load][keyDataToUse][csvLine][lineCell] + "</p>";
											};
											switchLine = 1;
										};
										if (dataFieldsTEMP !== {}) {
											lineTEMP["additionalData"] = dataFieldsTEMP;
										};
										if (titleCompleteTEMP !== "") {
											lineTEMP["titleComplete"] = titleCompleteTEMP;
										};
										dataGraph_BIS["nodes"].push(lineTEMP);
										switchLine = 0;
									};
								};
								if ("edges" in value.multiFormatData[config_to_load]) {
									for (csvLine in value.multiFormatData[config_to_load]["edges"]) {
										var lineTEMP = {};
										lineTEMP["from"] = value.multiFormatData[config_to_load]["edges"][csvLine]["from"];
										lineTEMP["to"] = value.multiFormatData[config_to_load]["edges"][csvLine]["to"];
										dataGraph_BIS["edges"].push(lineTEMP);
									};
								};
								if (dataCounter === value.multiFormatData.length) {
									resolve_LoadData(dataGraph_BIS);
								} else {
									resolve_LoadData("NextData");
								};
								dataCounter = dataCounter + 1;
							};
						});
					});
				};
			};
			
			chain_LoadData = chain_LoadData.then(function(value_LoadData){
				return new Promise((resolve_LoadData) => {
					progressBarIncrement();

					importNetwork(value_LoadData,cy);
					
					resolve_LoadData("Done");
					
					resolve_ScriptInitialisation("Done");
			
				});
			});
			
		});
		
	});
});

/*
--------------------------------
AGS.D.02 : Dealing with specified positions
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		progressBarReinitialize("Post-traitements : première phase",9,false);
		progressBarIncrement();

		if (Object.keys(dataGraph_POSITIONS).length === 0) {
			resolve_ScriptInitialisation("");
		} else {
			/* AGS.D.02.01 : Set the custom positions of the nodes */
			$.each(dataGraph_POSITIONS, function(index, value) {
				if (cy.getElementById(index).isParent() !== true) {
					cy.getElementById(index).position(value);
					cy.getElementById(index).data("position",value);
				};
			});
			/* AGS.D.02.02 : Set the custom positions of the compunds */
			cy.$("node:compound").forEach(function(elem) {
				if (dataGraph_POSITIONS[elem.id()] !== undefined) {
					cy.add({
						"group": "nodes",
						"data": {
							"id": elem.id()+"_corner_up_left",
							"position": {
								"x": dataGraph_POSITIONS[elem.id()]["x"] - (dataGraph_POSITIONS[elem.id()]["width"]/2),
								"y": dataGraph_POSITIONS[elem.id()]["y"] - (dataGraph_POSITIONS[elem.id()]["height"]/2)
							},
							"parent": elem.id()
						},
						"position": {
							"x": dataGraph_POSITIONS[elem.id()]["x"] - (dataGraph_POSITIONS[elem.id()]["width"]/2),
							"y": dataGraph_POSITIONS[elem.id()]["y"] - (dataGraph_POSITIONS[elem.id()]["height"]/2)
						}
					});
					cy.getElementById(elem.id()+"_corner_up_left").style({"visibility": "hidden"});
					cy.getElementById(elem.id()+"_corner_up_left").addClass("compoundCorner");
					cy.add({
						"group": "nodes",
						"data": {
							"id": elem.id()+"_corner_bottom_right",
							"position": {
								"x": dataGraph_POSITIONS[elem.id()]["x"] + (dataGraph_POSITIONS[elem.id()]["width"]/2),
								"y": dataGraph_POSITIONS[elem.id()]["y"] + (dataGraph_POSITIONS[elem.id()]["height"]/2)
							},
							"parent": elem.id()
						},
						"position": {
							"x": dataGraph_POSITIONS[elem.id()]["x"] + (dataGraph_POSITIONS[elem.id()]["width"]/2),
							"y": dataGraph_POSITIONS[elem.id()]["y"] + (dataGraph_POSITIONS[elem.id()]["height"]/2)
						}
					});
					cy.getElementById(elem.id()+"_corner_bottom_right").style({"visibility": "hidden"});
					cy.getElementById(elem.id()+"_corner_bottom_right").addClass("compoundCorner");
				};
			});
			/* AGS.D.02.03 : Lock the nodes if static option is activated */
			if (featuresGraph["static"]) {
				cy.$(".defaultNode").lock();
			};
			resolve_ScriptInitialisation("");
		};
		
	});
});

/*
--------------------------------
AGS.D.03 : Script loading promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		progressBarIncrement();
		
		/* --------------------------------------------- */
		/* AGS.D.03.01 : Creating the loading dictionnary */
		/* --------------------------------------------- */
		var modulesList_SubScripts = [];
		/* CORE event functions */
		modulesList_SubScripts.push("_core_events.js");
		/* CORE orbitals functions */
		modulesList_SubScripts.push("_core_orbitals.js");
		/* Info bubbles */
		if (featuresGraph["infoBubbles"] === true) {modulesList_SubScripts.push("_mod_info_bubbles.js");};
		/* Info tags */
		if (featuresGraph["infoTags"] === true) {modulesList_SubScripts.push("_mod_info_tags.js");};
		/* Intro tutorial */
		if (featuresInformationShowing === "true") {modulesList_SubScripts.push("_mod_intro_graph.js");};
		/* Clustering pack */
		if ((featuresGraph["featureClustering"] === true) || (featuresGraph["initialClustering"].length > 0)) {modulesList_SubScripts.push("_mod_clustering_pack.js");};
		/* Editor mode */
		if ((featuresButtons_FORCING["toggle_modeEditor"] !== "false") || (featuresGraph["editorOnLoad"] !== false)) {modulesList_SubScripts.push("_mod_editor_mode.js");};
		/* Search bar */
		if (featuresGraph["featureSearchBar"] === true) {modulesList_SubScripts.push("_mod_search_bar.js");};
		/* Sound design */
		if ((featuresGraph["animation"] === true)||(featuresGraph["sound"] === true)) {modulesList_SubScripts.push("_mod_sound_design.js");};
		/* Background canvas */
		if (featuresGraph["backgroundCanvas"] !== "") {modulesList_SubScripts.push("_mod_background_canvas.js");};
		/* Map mode */
		if (featuresButtons_FORCING["toggle_map"] !== "false") {modulesList_SubScripts.push("_mod_map_mode.js");};
		
		/* AGS.D.03.02 : Loading the dictionnary */
		modulesList_SubScripts.forEach(function(elem){
			chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,"js/"+elem+"?___VERSION_CACHE___"));
		});
		
		/* AGS.D.03.03 : Loading the custom scripts */
		if (scriptGraph_GET.length > 0) {
			for (i_script in scriptGraph_GET) {
				chain_ScriptLoading = chain_ScriptLoading.then(progressBarSetText.bind(null,"Post-traitements : script(s) du gabarit (/"+scriptGraph_GET[i_script].substring(scriptGraph_GET[i_script].lastIndexOf("/")+1)+")"));
				chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,idGraph_path+scriptGraph_GET[i_script]+"?___VERSION_CACHE___"));
			};
		};
		
		/* AGS.D.03.04 : Ending the promise */
		chain_ScriptLoading = chain_ScriptLoading.then(resolve_ScriptInitialisation.bind(null,""));
		
	});
});

/*
--------------------------------
AGS.D.04 : Some post-process promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		progressBarSetText("Post-traitements : seconde passe");
		progressBarIncrement();
		
		/* AGS.D.04.01 : Generating the modal screen summary body */
		if (featuresButtons_FORCING["toggle_summary"] !== "false") {
			var modalContent_Summary = document.getElementById("summaryModal_body");
			modalContent_Summary.innerHTML = readNodes_for_modal(cy);
		};
		
		/* AGS.D.04.02 : Postprocessing */
		if ((featuresButtons_FORCING["toggle_legend"] !== "false") && (dataGraph_LEGEND !== "")) {
			var legendContent = document.getElementById("legendModal_body_CUSTOM");
			legendContent.innerHTML = dataGraph_LEGEND;
			$("#toggle_legend").css("display","initial");
			$("#toggle_legend_BR").css("display","initial");
			$("#toggle_legend_LI").css("display","list-item");
			var modalContent_Legend = document.getElementById("appli_graph_buttons_LEGEND");
			var text_Legend = "";
			text_Legend = text_Legend.concat("<div class=\"buttons_accordion\" id=\"legend_buttons_accordion\">");
			text_Legend = text_Legend.concat("<div class=\"card\">");
			text_Legend = text_Legend.concat("<div class=\"card-header\" id=\"headingLegend\">");
			text_Legend = text_Legend.concat("<button class=\"btn mainViewButtons\" style=\"text-align:center;display:initial;\" data-toggle=\"collapse\" data-target=\"#collapseLegend\" aria-expanded=\"true\" aria-controls=\"collapseFilter\">");
			text_Legend = text_Legend.concat("<span><b>Légende</b></span>");
			text_Legend = text_Legend.concat("<img class=\"lazyload\" width=\"25\" height=\"25\" data-src=\"icon/list-ul-white.svg\"></img>");
			text_Legend = text_Legend.concat("</button>");
			text_Legend = text_Legend.concat("</div>");
			text_Legend = text_Legend.concat("<div id=\"collapseLegend\" class=\"collapse\" aria-labelledby=\"headingLegend\" data-parent=\"#legend_buttons_accordion\">");
			text_Legend = text_Legend.concat("<div id=\"collapseLegend_DIV_CONTROL\">");
			text_Legend = text_Legend.concat("<div id=\"legendAccordion_body_CUSTOM\">");
			text_Legend = text_Legend.concat(dataGraph_LEGEND);
			text_Legend = text_Legend.concat("</div>");
			text_Legend = text_Legend.concat("<div id=\"legendAccordion_body_TAGS\"></div>");
			text_Legend = text_Legend.concat("</div>");
			text_Legend = text_Legend.concat("</div>");
			text_Legend = text_Legend.concat("</div>");
			text_Legend = text_Legend.concat("</div>");
			modalContent_Legend.innerHTML = text_Legend;
		} else {
			$("#toggle_legend").css("display","none");
			$("#toggle_legend_BR").css("display","none");
			$("#toggle_legend_LI").css("display","none");
		};
		if (featuresGraph["infoBubbles"] === true) {informationBubbles(cy.nodes());};
		if (featuresGraph["infoTags"] === true) {
			informationTags(cy.nodes());
		} else {		
			$("#toggle_tags").css("display","none");
			$("#toggle_tags_BR").css("display","none");
			$("#toggle_tags_LI").css("display","none");
		};
		if (Object.keys(dataGraph_POSITIONS).length === 0) {
			$("#toggle_ori_pos").css("display","none");
			$("#toggle_ori_pos_BR").css("display","none");
			$("#toggle_ori_pos_LI").css("display","none");
		};
		if (cy.$("edge.indirectEdge").length === 0) {
			$("#toggle_ind_link").css("display","none");
			$("#toggle_ind_link_BR").css("display","none");
			$("#toggle_ind_link_LI").css("display","none");
		};
		
		/* AGS.D.04.03 : Backup variable */
		cy_elementsBackup = cy.elements();
		
		/* AGS.D.04.04 : Creating the search bar */
		if (featuresGraph["featureSearchBar"] === true) {
			creatingSearchBar();
		};
		
		/* AGS.D.04.05 : Make the custom cluters */
		if (featuresGraph["initialClustering"].length > 0) {
			resolve_ScriptInitialisation(GroupsToCluster(featuresGraph["initialClustering"],false));
		} else {
			resolve_ScriptInitialisation("");
		};
		
	});
});

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		progressBarIncrement();
		
		/* Set the initial layout */
		if (featuresGraph["initialClustering"].length > 0) {
			if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-cola.min.js")) {
				resolve_ScriptInitialisation(InitialLayout("euler_cola",true));
			} else {
				if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-fcose.min.js")) {
					resolve_ScriptInitialisation(fcoseLayout());
				} else {
					resolve_ScriptInitialisation("Done");
				};
			};
		} else {
			if (featuresGraph["physics"] === true) {
				resolve_ScriptInitialisation(InitialLayout("euler_cola",true));
			} else {
				resolve_ScriptInitialisation(ForceStabilization());
			};
		};
		
	});
});

/*
--------------------------------
AGS.D.05 : Animation promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		progressBarIncrement();
		
		if (featuresGraph["animation"] === true) {
			var directId_TEMP;
			if (StandAloneVersion !== undefined) {
				directId_TEMP = StandAloneVersion;
			} else {
				if (getParamValue("directId") !== undefined) {directId_TEMP = getParamValue("directId");};
			};
			if (directId_TEMP !== undefined) {
				chain_ScriptLoading = chain_ScriptLoading.then(function(){
					return new Promise((resolve_ScriptLoading) => {
						var link = document.createElement('link');
						link.setAttribute("rel", "stylesheet");
						link.setAttribute("type", "text/css");
						link.onload = resolve_ScriptLoading;
						link.setAttribute("href", "css/_mod_talking_circles.css?___VERSION_CACHE___");
						document.getElementsByTagName("head")[0].appendChild(link);
					});
				});
				chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,"js/_void_data_typing.js?___VERSION_CACHE___"));
				chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null,"js/_mod_talking_circles.js?___VERSION_CACHE___"));
				chain_ScriptLoading = chain_ScriptLoading.then(resolve_ScriptInitialisation.bind(null,""));
			} else {
				resolve_ScriptInitialisation("Done");
			};
		} else {
			resolve_ScriptInitialisation("Done");
		};
		
	});
});

/*
--------------------------------
AGS.D.06 : Fcose promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		progressBarIncrement();

		if ((featuresGraph["fcoseOnLoad"] === true) && (cy.nodes().length > 0)) {
			resolve_ScriptInitialisation(fcoseLayout(false));
		} else {
			resolve_ScriptInitialisation("Done");
		};
		
	});
});

/*
--------------------------------
AGS.D.07 : Final promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		progressBarIncrement();
		
		/* AGS.D.07.01 : Activating the tutorial */
		if (featuresInformationShowing === "true") {
			startIntro_graph(featuresGraph["featureReload"],featuresGraph["featureClustering"]);
		};
		
		/* AGS.D.07.02 : Check the alreadyLoaded variable */
		alreadyLoaded = true;
		
		/* AGS.D.07.03 : Stabilization safeguard */
		resolve_ScriptInitialisation(ForceStabilization());
		
	});
});

/*
--------------------------------
AGS.D.08 : Spinner promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		progressBarIncrement();

		/* AGS.D.08.01 : Make the graph visible */
		if (featuresGraph["animation"] !== true) {
			$("#appli_graph_network").css("visibility","visible");
			$("#appli_graph_element").css("display","initial");
			$("#appli_graph_cc_element").css("display","initial");
			$("#appli_graph_buttons").css("display","initial");
			$("#appli_graph_SEARCHING").css("display","flex");
		};

		/* AGS.D.08.02 : Loading the search bar */
		if (featuresGraph["featureSearchBar"] === true && featuresGraph["animation"] === false) {
			$("#appli_graph_SEARCHING").css({"visibility": "visible"});
			$("#info_searchBar").css({"visibility": "visible"});
			$("#info_searchBar").css({"height": "auto"});
			$("#info_searchBar").css({"margin": "0.5em 10% 0px 0px"});
		} else {
			$("#appli_graph_SEARCHING").css({"visibility": "hidden"});
			$("#info_searchBar").css({"visibility": "hidden"});
			$("#info_searchBar").css({"height": "0"});
			$("#info_searchBar").css({"margin": "0"});
		};
		
		/* AGS.D.08.03 : Hide the spinner */
		setTimeout(function() {
			progressBarIncrement();
			$("#appli_graph_spinner").fadeOut("fast",function (){progressBarStop();});
		},5);
		
		/* AGS.D.08.04 : Open the editor mode or the map mode on load */
		if ((featuresGraph["editorOnLoad"] !== false) && (featuresGraph["mapOnLoad"] === false)) {
			document.getElementById("toggle_modeEditor").click();
		};
		if ((featuresGraph["mapOnLoad"] !== false) && (featuresGraph["editorOnLoad"] === false)) {
			document.getElementById("toggle_map").click();
		};
		
		/* AGS.D.08.05 : Bug modal opening after 30s to look for comments (remove this step when GREZI is no longer in beta) */
		if ((getParamValue("debug") === undefined) && (featuresLoading_FORCING["debug"] !== "true") && (featuresLoading_FORCING["blank"] !== "true")) {
			setTimeout(function() {
				var is_ModalOpened;
				var id_ModalOpened;
				$(".modal").each(function() {
					if (($(this).data('bs.modal') || {})._isShown) {
						is_ModalOpened = true;
						id_ModalOpened = $(this);
					};
				});
				if (is_ModalOpened === undefined) {
					$("#bug_report_appli").html("<p>GREZI est en phase <b>beta</b>, tous les retours des utilisateur·trice·s sont précieux pour nous aider à améliorer ce commun numérique. Merci de prendre le temps de faire remonter vos remarques et surtout de signaler les problèmes auxquels vous êtes confronté·e·s.</p>"); 
					$("#bugModal").modal("show");
				} else {
					id_ModalOpened.once('hidden.bs.modal', function () {
						setTimeout(function() {
							$("#bug_report_appli").html("<p>GREZI est en phase <b>beta</b>, tous les retours des utilisateur·trice·s sont précieux pour nous aider à améliorer ce commun numérique. Merci de prendre le temps de faire remonter vos remarques et surtout de signaler les problèmes auxquels vous êtes confronté·e·s.</p>"); 
							$("#bugModal").modal("show");
						},100);
					})
				};
			},30000);
		};

		/* AGS.D.08.06 : Stabilize the tooltips */
		for (i in featuresButtons) {
			$("#" + i + "> a").tooltip({
				"title": featuresButtons_dictFun[i]["name"],
				"delay": {"show": 100, "hide": 100},
				"placement": "right",
				"trigger": "hover"
			});
			$("#" + i + "> a > img").attr("data-src",featuresButtons_dictFun[i]["svg"]);
		};

		/* AGS.D.08.07 : End the promise */
		resolve_ScriptInitialisation("Done");
		
	});
});

/*
--------------------------------
AGS.D.09 : Script template custom additions
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		if (templateScript_CustomAdditions !== undefined) {
			resolve_ScriptInitialisation(templateScript_CustomAdditions());
		} else {
			resolve_ScriptInitialisation("Done");
		};
	});
});

/*
--------------------------------
AGS.D.09 : SAVE DATA IN LOCAL SOTAGE
--------------------------------
*/

/*

WORK IN PROGRESS

ALSO SET THE JS CODE CACHE

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function(){
	return new Promise((resolve_ScriptInitialisation) => {
		console.log(cy.nodes().jsons());
		console.log(cy.edges().jsons());
		window.localStorage.setItem("nodes", JSON.stringify(cy.nodes().jsons()));
		window.localStorage.setItem("edges", JSON.stringify(cy.edges().jsons()));
		resolve_ScriptInitialisation("Done");
	});
});

*/
