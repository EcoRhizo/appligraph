/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
_VOID_TEMPLATES_TYPING.JS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var animationOnLaunch = false;

var animationCredentials = {
	"activate": false,
	"title": "",
	"img": "",
};

var possibilityToFullScreen = true;

var possibilityToPass = true;

var possibilityToSuspend = true;

var dictIntro = [

	{
		"img": "",
		"title": "",
		"isFadeOut": false,
		"pauseOncomplete": true,
		"suspendOnComplete": true,
		"onBeginInstructions": "",
		"onCompleteInstructions": "",
		"strings_to_type": [
			"What you want to say.",
			"Set some ^1000pause.",
			"And 'bulk typing'"
		]
	}

];
