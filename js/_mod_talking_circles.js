/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : TALKING CIRCLES
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
INITIALISATION
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
GLOBAL VARIABLES
-------------------------------------------------------------------------------
*/

if (typeof(chain_reading) === 'undefined') {
	var chain_reading = Promise.resolve();
};

var suspendedTyping_ID = 0;

var suspendedTyping_SWITCH = false;

var cy_bck;

/*
-------------------------------------------------------------------------------
PREPROCESSING
-------------------------------------------------------------------------------
*/

/* Display control */
$("#_talking_button_launch").css("display","none");
$("#appli_graph_element").css("display","none");
$("#appli_graph_cc_element").css("display","none");
$("#appli_graph_buttons").css("display","none");
$("#appli_graph_SEARCHING").css("visibility","hidden");
$(".introjs-hints").css("display","none");

/* Options control */
var toggleBubbles;
if (featuresGraph["infoBubbles"] === true) {
	featuresGraph["infoBubbles"] = false;
	toggleBubbles = true;
};
var toggleTags;
if (featuresGraph["infoTags"] === true) {
	featuresGraph["infoTags"] = false;
	toggleTags = true;
};

/*
-------------------------------------------------------------------------------
LOADING JS LIBRARIES
-------------------------------------------------------------------------------
*/

chain_ScriptLoading = chain_ScriptLoading.then(function(){
	return new Promise((resolve_ScriptLoading) => {

		var promise_LoadingScript = new Promise(function(resolve, reject) {
			var script = document.createElement('script');
			script.src = idGraph_path+"js/_data_typing.js?___VERSION_CACHE___";
			script.onerror = function () {
				resolve('');
			};
			script.onload = function () {
				resolve('');
			};
			document.head.appendChild(script);
		});
			
		promise_LoadingScript.then(function(value) {
			
			/*
			-------------------------------------------------------------------------------
			-------------------------------------------------------------------------------
			SCRIPT EXECUTION
			-------------------------------------------------------------------------------
			-------------------------------------------------------------------------------
			*/
			
			/*
			-------------------------------------------------------------------------------
			TYPING FUNCTIONS
			-------------------------------------------------------------------------------
			*/
			
			var typed;
			
			var typed_ArrayPos;
			
			var typed_Resolve = true;
			
			var typed_ClickNext = false;
			
			var typed_DestroyNext = false;
			
			function screenTyping(dictTyping,dictIndex) {
			
				return new Promise((resolve) => {
					
					if (typed_Resolve === true) {
					
						setTimeout(function() {
							
							suspendedTyping_ID = dictIndex;
							
							if (typed !== undefined) {typed.destroy()};
					
							if ((dictTyping["title"] !== undefined) && (dictTyping["title"] !== "")) {
								var div_talking_text = document.getElementById("_talking_title");
								div_talking_text.innerHTML = "<h1>" + dictTyping["title"] + "</h1>";
							}
							
							if ((dictTyping["img"] !== undefined) && (dictTyping["img"] !== "") && (dictTyping["img"] !== false)) {
								$("#_talking_image").css("display","flex");
								document.getElementById("_talking_image").className = "col-2";
								document.getElementById("_talking_body").className = "col-10";
								var div_talking_img = document.getElementById("_talking_image");
								div_talking_img.innerHTML = "<img src=\""+idGraph_path+dictTyping["img"]+"\"></img>";
							}
							
							if ((dictTyping["img"] === false) || (dictTyping["img"] === "")) {
								$("#_talking_image").css("display","none");
								document.getElementById("_talking_image").className = "col-0";
								document.getElementById("_talking_body").className = "col-12";
								var div_talking_img = document.getElementById("_talking_image");
								div_talking_img.innerHTML = "";
							};
							
							if (dictTyping["isFadeOut"] === false) {
								var isFadeOut = false;
							} else {
								var isFadeOut = true;
							};
							
							typed = new Typed("#_talking_typed", {
								typeSpeed: 10,
								fadeOut: isFadeOut,
								fadeOutDelay: 0,
								startDelay: 0,
								backDelay: 1500,
								showCursor: false,
								smartBackspace: true,
								contentType: "html",
								strings: dictTyping["strings_to_type"],
								onBegin: function(self) {
									if (typed_ClickNext === false) {
										$("#_talking_frame").css("pointer-events","auto");
										$("#_talking_next_word").css("display","none");
										if ((dictTyping["onBeginInstructions"] !== undefined) && (dictTyping["onBeginInstructions"] !== "")) {
											var F = new Function (dictTyping["onBeginInstructions"]);
											F();
										};
									} else {
										typed_ClickNext = false;
									};
								},
								onComplete: function(self) {
									if (typed_Resolve === true) {
										if (dictTyping["pauseOncomplete"] === true) {
											$("#_talking_next_word").css("display","initial");
											typed.stop();
										} else {
											$("#_talking_frame").css("pointer-events","none");
											if ((dictTyping["onCompleteInstructions"] !== undefined) && (dictTyping["onCompleteInstructions"] !== "")) {
												let chain_F = Promise.resolve();
												if (dictTyping["onCompleteInstructions"].indexOf("resolve_F") > -1) {
													var F = new Function ("return new Promise((resolve_F) => {"+dictTyping["onCompleteInstructions"]+"});");
												} else {
													var F = new Function ("return new Promise((resolve_F) => {"+dictTyping["onCompleteInstructions"]+"resolve_F(\"\");"+"});");
												};
												setTimeout(function(){
													chain_F = chain_F.then(F);
													resolve(chain_F.then());
												},1400);
											} else {
												setTimeout(function(){resolve("")},1400);
											};
										};
									} else {
										resolve("");
									};
								},
								onDestroy: function(self) {
									if (typed_Resolve === true) {
										if (dictTyping["suspendOnComplete"] === true) {
											typed_Resolve = false;
											suspendedTyping_ID += 1;
											suspendedTyping_SWITCH = true;
										};
										if (typed_DestroyNext === true) {
											typed_DestroyNext = false;
											$("#_talking_frame").css("pointer-events","none");
											if ((dictTyping["onCompleteInstructions"] !== undefined) && (dictTyping["onCompleteInstructions"] !== "")) {
												let chain_F = Promise.resolve();
												if (dictTyping["onCompleteInstructions"].indexOf("resolve_F") > -1) {
													var F = new Function ("return new Promise((resolve_F) => {"+dictTyping["onCompleteInstructions"]+"});");
												} else {
													var F = new Function ("return new Promise((resolve_F) => {"+dictTyping["onCompleteInstructions"]+"resolve_F(\"\");"+"});");
												};
												setTimeout(function(){
													chain_F = chain_F.then(F);
													resolve(chain_F.then());
												},50);
											} else {
												setTimeout(function(){resolve("")},50);
											};
										};
									} else {
										resolve("");
									};
								}
							});
						
						}, 100);
						
					} else {
						resolve("");
					};
					
				});
				
			};
			
			if (alreadyLoaded === false) {
				
				$("#_talking_frame").on('click',function(){
					if (typed.pause.status === true) {
						$("#_talking_next_word").css("display","none");
						if (typed_ArrayPos < typed.strings.length) {
							var typed_array_BCK = typed.strings.slice(typed_ArrayPos);
							typed.strings = typed_array_BCK;
							typed.pause.curString = typed_array_BCK[0];
							typed.start();
						} else {
							typed_DestroyNext = true;
							typed.destroy();
						};
					} else {
						typed.stop();
						typed_ClickNext = true;
						setTimeout(function(){
							typed_ArrayPos = typed.arrayPos+1;
							typed.reset();
							var div_talking_typed_TEXT = document.getElementById("_talking_typed");
							div_talking_typed_TEXT.innerHTML = typed.strings[typed_ArrayPos-1].replace(/\^\d+/g,"");
							$("#_talking_next_word").css("display","initial");
						},50);
					};
				});
				
				$("#_talking_frame").on('mouseover',function(){
					document.body.style.cursor = 'pointer';
				});
				
				$("#_talking_frame").on('mouseout',function(){
					document.body.style.cursor = 'default';
				});
				
			};
			
			/*
			-------------------------------------------------------------------------------
			ENDING FUNCTION
			-------------------------------------------------------------------------------
			*/
			
			function endingTyping(activatingSeeView) {
				return new Promise((resolve) => {
					setTimeout(function() {
						cy.clearQueue();
						$("#_talking_frame").fadeOut("fast");
						$("#_talking_button_fullscreen").css("display","none");
						$("#_talking_button_pass").css("display","none");
						$("#_talking_button_suspend").css("display","none");
						$("#_talking_button_launch").css("display","initial");
						$("#appli_graph_element").css("display","initial");
						$("#appli_graph_cc_element").css("display","initial");
						$("#appli_graph_buttons").css("display","initial");
						$("#appli_graph_SEARCHING").css("visibility","visible");
						$(".introjs-hints").css("display","initial");
						$("#appli_graph_network").css("height","100%");
						$("#appli_graph_network_CONTAINER").css("pointer-events","auto");
						if (toggleBubbles === true) {featuresGraph["infoBubbles"] = true;};
						if (toggleTags === true) {featuresGraph["infoTags"] = true;};
						cy.resize();
						if (suspendedTyping_SWITCH === false) {
							suspendedTyping_ID = 0;
							if (activatingSeeView === true) {
								/* cy_bck.restore(); */
								resolve(ForceStabilization());
								/* resolve(seeView()); */
							} else {
								cy.delay(10,function(){cy.animate({"duration":500,"fit":{"padding":100},"easingFunction":"ease","complete":function(){resolve("")}})});
							};
						} else {
							cy.delay(10,function(){cy.animate({"duration":500,"fit":{"padding":100},"easingFunction":"ease","complete":function(){resolve("")}})});
						};
					}, 100);
				});
			};
			
			/*
			-------------------------------------------------------------------------------
			READING DICT FUNCTION
			-------------------------------------------------------------------------------
			*/
			
			function readingDict(dictTyping) {
				/* Display control */
				if (possibilityToFullScreen === true) {$("#_talking_button_fullscreen").css("display","initial");};
				if (possibilityToPass === true) {$("#_talking_button_pass").css("display","initial");};
				if (possibilityToSuspend === true) {$("#_talking_button_suspend").css("display","initial");};
				$("#_talking_button_launch").css("display","none");
				$("#appli_graph_element").css("display","none");
				$("#appli_graph_cc_element").css("display","none");
				$("#appli_graph_buttons").css("display","none");
				$("#appli_graph_SEARCHING").css("visibility","hidden");
				$(".introjs-hints").css("display","none");
				/* Graph control */
				if (dictTyping[suspendedTyping_ID] === undefined) {
					suspendedTyping_ID = 0;
				};
				if (suspendedTyping_ID === 0) {
					/*
					cy_elementsBackup.restore();
					cy_bck = cy.remove(cy.elements());
					*/
				};
				$("#appli_graph_network_CONTAINER").css("pointer-events","none");
				$("#appli_graph_network").css("visibility","visible");
				$("#appli_graph_network").css("height","75%");
				cy.resize();
				/* Options control */
				if (toggleBubbles === true) {featuresGraph["infoBubbles"] = false;};
				if (toggleTags === true) {featuresGraph["infoTags"] = false;};
				/* Reading the dict */
				$("#_talking_frame").fadeIn("fast",function(){
					suspendedTyping_SWITCH = false;
					dictTyping.forEach(function(elem,index) {
						if (index >= suspendedTyping_ID) {
							chain_reading = chain_reading.then(screenTyping.bind(null,elem,index));
						};
					});
					chain_reading = chain_reading.then(endingTyping.bind(null,true));
				});
			};

			/*
			-------------------------------------------------------------------------------
			MAIN EXECUTION
			-------------------------------------------------------------------------------
			*/
			
			if (animationOnLaunch === true) {
				
				featuresInformationShowing = "false";
			
				var div_talking_text = document.getElementById("_talking_introduction");
				if (animationCredentials["title"] !== "") {
					var div_talking_text_TITLE = "<h1>"+animationCredentials["title"]+"</h1><h2>Application prête</h2>";
				} else {
					var div_talking_text_TITLE = "<h1>Propulsé par</h1><h1>GRÉSI</h1><h2>Application prête</h2>";
				};
				if (animationCredentials["img"] !== "") {
					var div_talking_text_IMG = idGraph_path+animationCredentials["img"];
				} else {
					var div_talking_text_IMG = "../appli_graph/img/grezi.webp";
				};
				div_talking_text.innerHTML = "<div id=\"_talking_introduction_TEXT\">"+div_talking_text_TITLE+"<hr></div>"+"<div id=\"_talking_introduction_IMG\"><img src=\""+div_talking_text_IMG+"\"></img></div>";
				
				var promise_INTRO = new Promise(function(resolveINTRO, rejectINTRO) {
					if (animationCredentials["activate"] === true) {
						setTimeout(function() {
							$("#_talking_introduction").fadeIn( 1000, function() {
								setTimeout(function() {
									$("#_talking_introduction").fadeOut( 1000, function() {
										setTimeout(function() {
											resolveINTRO('');
										});
									});
								}, 1000);
							});
						}, 1000);
					} else {
						resolveINTRO('');
					};
				});
				
				promise_INTRO.then(function(valueINTRO) {
					setTimeout(function() {
						$("#_talking_button").fadeIn(1000);
						readingDict(dictIntro);
					}, 500);
				});
				
			} else {
				$("#_talking_button").fadeIn(1000);
				$("#_talking_frame").fadeOut("fast");
				$("#_talking_button_fullscreen").css("display","none");
				$("#_talking_button_pass").css("display","none");
				$("#_talking_button_suspend").css("display","none");
				$("#_talking_button_launch").css("display","initial");
				$("#appli_graph_element").css("display","initial");
				$("#appli_graph_cc_element").css("display","initial");
				$("#appli_graph_buttons").css("display","initial");
				$("#appli_graph_SEARCHING").css("visibility","visible");
				$(".introjs-hints").css("display","initial");
				$("#appli_graph_network").css("visibility","visible");
				$("#appli_graph_network_CONTAINER").css("pointer-events","auto");
			};
			
			var talkingButton_fullscreen = document.getElementById("_talking_button_fullscreen");
			
			applyColorTransform("#_talking_button_fullscreen > img","","filter: invert(100%)");

			talkingButton_fullscreen.addEventListener("click",function() {
				/* if already full screen; exit */
				/* else go fullscreen */
				if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement) {
					if (document.exitFullscreen) {
					  document.exitFullscreen();
					} else if (document.mozCancelFullScreen) {
					  document.mozCancelFullScreen();
					} else if (document.webkitExitFullscreen) {
					  document.webkitExitFullscreen();
					} else if (document.msExitFullscreen) {
					  document.msExitFullscreen();
					};
				} else {
					element = $('#page-top').get(0);
					if (element.requestFullscreen) {
					  element.requestFullscreen();
					} else if (element.mozRequestFullScreen) {
					  element.mozRequestFullScreen();
					} else if (element.webkitRequestFullscreen) {
					  element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
					} else if (element.msRequestFullscreen) {
					  element.msRequestFullscreen();
					};
				};
			});	

			var talkingButton_pass = document.getElementById("_talking_button_pass");

			talkingButton_pass.addEventListener("click",function() {
				$("#_talking_main_screen").css("pointer-events","none");
				typed_Resolve = false;
				typed.destroy();
			});	

			var talkingButton_suspend = document.getElementById("_talking_button_suspend");

			talkingButton_suspend.addEventListener("click",function() {
				typed_Resolve = false;
				suspendedTyping_ID += 1;
				suspendedTyping_SWITCH = true;
				typed.destroy();
			});	

			/*
			var talkingButton_launch = document.getElementById("_talking_button_launch");

			talkingButton_launch.addEventListener("click",function() {
				typed_Resolve = true;
				readingDict(dictIntro);
			});
			*/

			resolve_ScriptLoading("Done");
			
		});
	
	});
});