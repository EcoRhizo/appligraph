/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : TURORIAL INTRO GRAPH
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

function startIntro_graph(viewALL_step,clusteringSTEP){
	
	var intro = introJs();
	
	let introFirstText = "";

    if ((featuresGraph["customIcons"]["page_title"] !== undefined) && (featuresGraph["customIcons"]["page_title"] !== "")) {
    	if (featuresGraph["animation"] !== true) {introFirstText += "<p>"+"Tutoriel de l\'application " + featuresGraph["customIcons"]["page_title"]+"<p>";};
    	introFirstText += "Voici quelques indications utiles pour faciliter votre prise en main. Vous pouvez cliquer en dehors de la fenêtre pour quitter le tutoriel. Bonne navigation !"+"</p>";
    } else {
        if (getParamValue("blank") === undefined) {
        	if (featuresGraph["animation"] !== true) {introFirstText += "<p>"+"Tutoriel de<br>l\'application GREZI"+"</p>";};
            introFirstText += "<p>"+"Voici quelques indications utiles pour faciliter votre prise en main. Vous pouvez cliquer en dehors de la fenêtre pour quitter le tutoriel. Bonne navigation !"+"</p>";
        } else {
        	if (featuresGraph["animation"] !== true) {introFirstText += "<p>"+"Tutoriel"+"</p>";};
            introFirstText += "<p>"+"Voici quelques indications utiles pour faciliter votre prise en main. Vous pouvez cliquer en dehors de la fenêtre pour quitter le tutoriel. Bonne navigation !"+"</p>";
        };
    };

	var introSteps = [{
		intro: introFirstText
	}];

	introSteps.push({
		intro: "<p>Pour naviguer dans le Panorama, déplacez-vous par \"glisser/déposer\" comme dans une cartographie en ligne.</p><p>Un double-clic vous permet de recenter la vue.</p><p>Vous pouvez également zoomer et dézoomer (molette de votre souris ou zoom du pavé tactile)</p><p>Certains éléments sont interactifs. Vous pouvez les déplacer par \"glisser/déposer\", les survoler pour faire apparaître des informations complémentaires et les sélectionner en cliquant dessus.</p>"
	});
	
	var featuresButtons_INTRO = [];
	
	if (featuresGraph["editorOnLoad"] !== false) {
		introSteps.push({
			intro: "Le mode éditeur est actif au chargement de ce gabarit, double-cliquez pour créer des éléments."
		});
	};

	for (i in featuresButtons) {
		if (($("#" + i).css("display") !== "none") && (featuresButtons[i] !== "false")) {
			featuresButtons_INTRO.push(i);
		};
	};

	for (i in featuresButtons_FORCING) {
		if (featuresButtons_FORCING[i] === "true") {
			if (!(i in featuresButtons_INTRO)) {
				featuresButtons_INTRO.push(i);
			};
		};
		if (featuresButtons_FORCING[i] === "false") {
			var indexTEMP = featuresButtons_INTRO.indexOf(i);
			if (indexTEMP > -1) {
				featuresButtons_INTRO.splice(indexTEMP, 1);
			};
		};
	};

	for (i in featuresButtons) {
		if (($("#" + i).css("display") === "none") || ($("#" + i).length === 0)) {
			var indexTEMP = featuresButtons_INTRO.indexOf(i);
			if (indexTEMP > -1) {
				featuresButtons_INTRO.splice(indexTEMP, 1);
			};
		};
	};

	for (i in featuresButtons_INTRO) {
		/*
		var textTEMP = document.getElementById(featuresButtons_INTRO[i]+"_LI");
		textTEMP = textTEMP.innerHTML;
		textTEMP = textTEMP.split(" : ");
		textTEMP = textTEMP[0];
		textTEMP = textTEMP.substr(0,1).toUpperCase()+textTEMP.substr(1)+".";
		introSteps.push({
			element: document.querySelector('#'+featuresButtons_INTRO[i]),
			intro: textTEMP
		});
		*/
		introSteps.push({
			element: document.querySelector('#'+featuresButtons_INTRO[i]),
			intro: featuresButtons_dictFun[featuresButtons_INTRO[i]]["name"]
		});
	};
	
	if (viewALL_step === true) {
		introSteps.push({
			element: document.querySelector('#view_ALL'),
			intro: "Remettre à zéro la visualisation et relancer le tutoriel."
		});
	};
	
	if (clusteringSTEP === true) {
		introSteps.push({
			element: document.querySelector('#group_ALL'),
			intro: "Regrouper tous les éléments par liens de parenté."
		});
		introSteps.push({
			element: document.querySelector('#ungroup_ALL'),
			intro: "Dégrouper tous les groupes d\'éléments."
		});
	};
	
	if (viewsButtons.length > 0) {
		introSteps.push({
			element: document.querySelector('#graph_buttons_accordion'),
			intro: "Filtrer sur certains éléments du graphe à l\'aide des boutons en haut à droite."
		});
	};
	
	if (featuresButtons_INTRO.includes("toggle_legend") !== -1) {
		introSteps.push({
			element: document.querySelector('#headingLegend'),
			intro: "Cliquer sur le bandeau \"Légende\" pour ouvrir ou fermer le cadre. Choisir les éléments à faire apparaître à l’écran en cochant ou décochant les cases grises."
		});
	};

	if (featuresGraph["featureSearchBar"] === true) {
		introSteps.push({
			element: document.querySelector('#appli_graph_SEARCH_BAR'),
			intro: "<p>Barre de recherche pour choisir les éléments affichés.</p><p>Étape 1 : sélectionner une catégorie de recherche (par Nom ou par Thématique d’intervention) dans \"Nouvelle recherche\".</p><p>Étape 2 : saisir ou sélectionner le ou les éléments à rechercher.</p>"
		});
		introSteps.push({
			element: document.querySelector('#appli_graph_BUTTONS_BOX'),
			intro: "Étape 3 : choisir un mode (zoom ou filtre) pour appliquer la recherche. Pour revenir à la visualisation complète, appliquer \"Filtrer\" à vide."
		});
		if (featuresGraph_SearchBarOptions["or"] !== false) {
			introSteps.push({
				element: document.querySelector('#appli_graph_MODE_BUTTON_or'),
				intro: "Optionnel => Prédicat « OU de sélection » (mode inclusif). Ceci est le mode de recherche par défaut."
			});
		};
		if (featuresGraph_SearchBarOptions["and"] !== false) {
			introSteps.push({
				element: document.querySelector('#appli_graph_MODE_BUTTON_and'),
				intro: "Optionnel => Prédicat « ET » de sélection (mode exclusif)."
			});
		};
		if (featuresGraph_SearchBarOptions["selected"] !== false) {
			introSteps.push({
				element: document.querySelector('#appli_graph_MODE_BUTTON_selected'),
				intro: "Optionnel => Rechercher uniquement sur les éléments sélectionnés."
			});
		};
		if (featuresGraph_SearchBarOptions["zoom"] !== false) {
			introSteps.push({
				element: document.querySelector('#appli_graph_MODE_BUTTON_zoom'),
				intro: "Optionnel => Zoomer dans le graphe au lieu de filtrer le graphe."
			});
		};
		/* FROM AURBA PROJECT => TO GENERALIZE ? */
		/*
		if ($("#appli_graph_SEARCH_BUTTON_filter").length === 0) {
			introSteps.push({
				element: document.querySelector('#appli_graph_SEARCH_BUTTON'),
				intro: "Troisième étape : cliquer ici pour appliquer la recherche."
			});
		} else {
			introSteps.push({
				element: document.querySelector('#appli_graph_SEARCH_BUTTON_filter'),
				intro: "Filtrer les éléments."
			});
			introSteps.push({
				element: document.querySelector('#appli_graph_SEARCH_BUTTON'),
				intro: "Zoomer sur les éléments."
			});
		};
		*/
	};
	
	if ((getParamValue("blank") === undefined) && (featuresLoading_FORCING["blank"] === undefined)) {
		introSteps.push({
			element: document.querySelector('#appli_graph_cc_element'),
			intro: "Consulter le site internet de GREZI et les licences d\'utilisation."
		});
	};
	
	intro.setOptions({
		
		nextLabel: "Suivant",
		
		prevLabel: "Précédent",
		
		skipLabel: "Passer",
		
		doneLabel: "Fin",
		
		disableInteraction: true,
		
		showBullets: false,
		
		showProgress: true,
		
		showStepNumbers: false,
		
		steps: introSteps
	});
	
	intro.onexit(function() {
		if (featuresButtons_INTRO.includes("toggle_fullscreen")) {
			introHints = introJs();
			introHints.setOptions({
				"hintButtonLabel": "Compris",
				"hints": [{
					"element": document.querySelector('#toggle_fullscreen'),
					"hint": "Mettre en plein écran.",
					"hintPosition": "top-right"
				}]
			});
			introHints.addHints();
		};
	});

	intro.start();

};
