/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : CREATING SEARCH BAR
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/* Setting variables */

let chain_SearchBar = Promise.resolve();

let chain_SearchBarPHASING = Promise.resolve();

var toggleSearchBar_CustomOpening;

var toggleSearchBar_CustomClosing;

var zoomMode;

var nodeSelection;

var checkingInfoTag;

var checkingInfoBubble;

var widgetSearch;

/* Avoid to show input panel on mobile devices */

$("#appli_graph_SEARCH_BAR").on("DOMNodeInserted", function() {
	$(".input-8f2fe").attr("placeholder", " ");
	let widthSearchBar = $("#appli_graph_SEARCH_BAR").css("width");
	let isMobile = parseInt(widthSearchBar.replace("px",""))/$(window).width();
	if (isMobile > 0.5) {
		$(".input-8f2fe").attr("readonly", true);
	} else {
		$(".input-8f2fe").attr("readonly", false);
	};
});

$(window).resize(function () {
	let widthSearchBar = $("#appli_graph_SEARCH_BAR").css("width");
	let isMobile = parseInt(widthSearchBar.replace("px",""))/$(window).width();
	if (isMobile > 0.5) {
		$(".input-8f2fe").attr("readonly", true);
	} else {
		$(".input-8f2fe").attr("readonly", false);
	};
});

/* Main function */

function creatingSearchBar(KeepRemovedNodes=true,HideOnLoad=true) {

	/* Preparing the searching lists */
	
	var tagList = [];
	
	var collectionToTag = cy.collection();
	
	if (KeepRemovedNodes === true) {
		collectionToTag.merge(cy.nodes()).merge(cy_elementsBackup.nodes());
	} else {
		collectionToTag.merge(cy.nodes());
	};
	
	collectionToTag.forEach(function(elem) {
		if ((elem.data("searchTag") !== undefined) && (elem.data("searchTag") !== "")) {tagList.push(elem.data("searchTag"));};
	});
	
	if (tagList.length > 0) {
		
		/* Creating the searching dictionnary */
		
		var tagList_KEYS = [];
		
		var tagList_VALUES = {};
		
		for (i_search in tagList) {
			for (i_tag_index in tagList[i_search]) {
				tagList_KEYS.push(i_tag_index);
				var tagList_VALUES_keys = Object.keys(tagList_VALUES);
				if (!tagList_VALUES_keys.includes(i_tag_index)) {tagList_VALUES[i_tag_index] = [];};
				for (i_tag_value in tagList[i_search][i_tag_index]) {
					let valueTemp = tagList[i_search][i_tag_index][i_tag_value];
					if(valueTemp !== "") {tagList_VALUES[i_tag_index].push(valueTemp);};
				};
			};
		};

		for (i_key in tagList_KEYS) {
			if (tagList_VALUES[tagList_KEYS[i_key]].length === 0) {
				tagList_KEYS.splice(i_key,1);
			};
		};
	
		let tagList_KEYS_unique = [...new Set(tagList_KEYS)];
		
		var tagList_KEYS_sorted = tagList_KEYS_unique.sort();
		
		var tagList_VALUES_unique = {};
		
		for (i_value in tagList_VALUES) {
			let tagTEMP = [...new Set(tagList_VALUES[i_value])];
			var tagTEMP_sorted = tagTEMP.sort();
			tagTEMP_sorted = tagTEMP_sorted.filter(function (element) {
				return element !== undefined;
			});
			if (tagTEMP_sorted.length >= 1) {tagList_VALUES_unique[i_value] = {"noResultsHTML": "Saisissez une option valide", "options": tagTEMP_sorted};};
		};
		
		var tagList_FOR_SEARCH = tagList_VALUES_unique;
	
		tagList_FOR_SEARCH["nodes"] = [];
		
		for (i_for_search in tagList_KEYS_sorted) {
			tagList_FOR_SEARCH["nodes"].push({"value": tagList_KEYS_sorted[i_for_search], "children": tagList_KEYS_sorted[i_for_search]})
		};
		
		/* On Change Function */
		
		var onChangeSearch = function(newValue, oldValue) {
			/* nothing */
		};
		
		/* Config options */
		
		var configSearch = {
			onChange: onChangeSearch,
			initialList: "nodes",
			lists: tagList_FOR_SEARCH,
			placeholderHTML : "<img src=\"icon/plus-solid.svg\" style=\"height:10px;\"/>&nbsp;<i>Nouvelle recherche</i>",
			tokenSeparatorHTML : " ▸ "
		};
		
		/* Creating the search bar */
		
		if (widgetSearch === undefined) {
			widgetSearch = new AutoComplete('appli_graph_SEARCH_BAR', configSearch);
		} else {
			widgetSearch.destroy();
			widgetSearch = new AutoComplete('appli_graph_SEARCH_BAR', configSearch);
		};
		
		/* Post-processing */
		if ((HideOnLoad === true) || (alreadyLoaded === false  && $("#appli_graph_spinner").css("display") !== "none")) {$("#appli_graph_SEARCHING").css({"visibility": "hidden"})};
		
		$("#appli_graph_SEARCH_BUTTON").css("display","initial");
		$("#appli_graph_SEARCH_BUTTON_filter").css("display","initial"); /* FROM AURBA PROJECT => TO GENERALIZE */
		if (featuresGraph_SearchBarOptions["zoom"] === true) {$("#appli_graph_MODE_BUTTON_zoom").css("display","initial");};
		if (featuresGraph_SearchBarOptions["selected"] === true) {$("#appli_graph_MODE_BUTTON_selected").css("display","initial");};
		if (featuresGraph_SearchBarOptions["and"] === true) {$("#appli_graph_MODE_BUTTON_and").css("display","initial");};
		if (featuresGraph_SearchBarOptions["or"] === true) {$("#appli_graph_MODE_BUTTON_or").css("display","initial");};
		
		$("#appli_graph_SEARCHING .btn").height($("#appli_graph_SEARCH_BAR").height()-4);
		
		/* Make the height of the button following the one of the search bar */
		
		$(".autocomplete-container-7a26d").on('stylechanged',function(){
			$("#appli_graph_SEARCHING .btn").height($("#appli_graph_SEARCH_BAR").height()-4);
		});
		
	};
	
};

/* Functions for the predicate buttons */

$("#appli_graph_MODE_BUTTON_selected").on('click',function(){
	if ($("#appli_graph_MODE_BUTTON_selected").css("color") === "rgb(255, 255, 255)") {
		$("#appli_graph_MODE_BUTTON_selected").css("color","var(--main_color_01)");
		$("#appli_graph_MODE_BUTTON_selected > img").attr("src","icon/hand-pointer-solid.svg");
	} else {
		$("#appli_graph_MODE_BUTTON_selected").css("color","white");
		$("#appli_graph_MODE_BUTTON_selected > img").attr("src","icon/hand-pointer-solid-white.svg");
	};
});	

$("#appli_graph_MODE_BUTTON_zoom").on('click',function(){
	if ($("#appli_graph_MODE_BUTTON_zoom").css("color") === "rgb(255, 255, 255)") {
		$("#appli_graph_MODE_BUTTON_zoom").css("color","var(--main_color_01)");
		$("#appli_graph_MODE_BUTTON_zoom > img").attr("src","icon/search-plus-solid.svg");
	} else {
		$("#appli_graph_MODE_BUTTON_zoom").css("color","white");
		$("#appli_graph_MODE_BUTTON_zoom > img").attr("src","icon/search-plus-solid-white.svg");
	};
});			

$("#appli_graph_MODE_BUTTON_and").on('click',function(){
	$("#appli_graph_MODE_BUTTON_and").css("color","white");
	$("#appli_graph_MODE_BUTTON_or").css("color","var(--main_color_01)");
});

$("#appli_graph_MODE_BUTTON_or").on('click',function(){
	$("#appli_graph_MODE_BUTTON_and").css("color","var(--main_color_01)");
	$("#appli_graph_MODE_BUTTON_or").css("color","white");
});

/* Button search onclick function */

$("#appli_graph_SEARCH_BUTTON").on('click',function(){

	chain_SearchBar = chain_SearchBar.then(function(){
		return new Promise((resolve_SearchBar) => {
			if (toggleSearchBar_CustomOpening !== undefined) {
				resolve_SearchBar(toggleSearchBar_CustomOpening());
			} else {
				resolve_SearchBar("Done");
			};
		});
	});
	
	chain_SearchBar = chain_SearchBar.then(function(){
		return new Promise((resolve_SearchBar) => {
			
			$("#appli_graph_network_CONTAINER").css("pointer-events","none");
			
			if (featuresGraph["infoTags"] === true) {
				featuresGraph["infoTags"] = false;
				checkingInfoTag = true;
			} else {
				checkingInfoTag = false;
			};
			
			if (featuresGraph["infoBubbles"] === true) {
				featuresGraph["infoBubbles"] = false;
				checkingInfoBubble = true;
			} else {
				checkingInfoBubble = false;
			};
	
			if ($("#appli_graph_MODE_BUTTON_zoom").css("color") === "rgb(255, 255, 255)") {
				zoomMode = true;
			} else {
				zoomMode = false;
			};
			resolve_SearchBar("Done");
	
		});
	});
	
	chain_SearchBar = chain_SearchBar.then(function(){
		return new Promise((resolve_SearchBar) => {
			
			var widgetSearch_Values = widgetSearch.getValue();
			if ($("#appli_graph_MODE_BUTTON_selected").css("color") === "rgb(255, 255, 255)") {
				var onlySelection = true;
			} else {
				var onlySelection = false;
			};
			
			if (widgetSearch_Values.length > 0) {
				if (widgetSearch_Values[0].length > 1) {
					var booleanTEMP = true;
				} else {
					var booleanTEMP = false;
				};
			} else {
				var booleanTEMP = false;
			};

			if (booleanTEMP === true) {
			
				/* Opening the clusters */
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						listClusters = {};
						cy_elementsBackup = cy_elementsBackup.union(cy.remove(cy.edges()));
						cy_elementsBackup = cy_elementsBackup.union(cy.remove(cy.nodes()));
						cy_elementsBackup.restore();
						cy.nodes().forEach(function(elem) {
							if (elem.hasClass("cluster")) {
								if ((elem.style("border-width") !== "0px") && (elem.style("border-width") !== "0rem")) {
									elem.style("border-color",mainColor_01);
									elem.style("border-width","2rem");
								} else {
									elem.style("color",mainColor_01);
								};
								elem.selectify();
							};
						});
						setTimeout(function() {resolve_SearchBarPHASING("Done")}, 10);
					});
				});
				
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {

						/* Set the predicate */
						if ($("#appli_graph_MODE_BUTTON_and").css("color") === "rgb(255, 255, 255)") {
							var predicateSelection = "AND";
						} else {
							var predicateSelection = "OR";
						};
						
						function switchChecking(switchValue,comparisonValue,predicateValue){
							if (predicateValue === "AND") {return switchValue === comparisonValue;};
							if (predicateValue === "OR") {return switchValue >= 1;};
						};
					
						/* Updating the network */
						nodeSelection = cy.collection();
						cy.nodes().forEach(function(elem) {
							if ( (elem.data("searchTag") !== undefined) && (elem.data("searchTag") !== {}) && (elem.data("searchTag") !== "") ) {
								var switchSearch = 0;
								for (i in widgetSearch_Values) {
									var searchTag_TEMP = widgetSearch_Values[i][0]["value"];
									var valueTag_TEMP = widgetSearch_Values[i][1]["value"];
									var keysTag_TEMP = Object.keys(elem.data("searchTag"));
									if (keysTag_TEMP.includes(searchTag_TEMP)) {
										var currentTag_TEMP = elem.data("searchTag")[searchTag_TEMP];
										if (currentTag_TEMP.includes(valueTag_TEMP)) {
											switchSearch = switchSearch+1;
										};
									};
								};
								if (switchChecking(switchSearch,widgetSearch_Values.length,predicateSelection)) {
									if (onlySelection === false) {
										elem.addClass("searched");
										nodeSelection = nodeSelection.union(elem);
									} else {
										if (elem.selected()) {
											elem.addClass("searched");
											nodeSelection = nodeSelection.union(elem);
										};
									};
								};
							};
						});

						setTimeout(function() {resolve_SearchBarPHASING("Done")}, 10);
					});
				});
				
				/* Remove compounds without nodes */
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						cy.nodes().forEach(function(elem) {
							if (zoomMode === false) {
								let collectionToDelete = cy.collection();
								cy.nodes().forEach(function(elem) {
									if (!elem.isParent() && !elem.hasClass("searched") && !elem.hasClass("infoTag") && !elem.hasClass("infoBubble")) {
										collectionToDelete = collectionToDelete.union(elem);
									};
								});
								cy.nodes(".searched:compound").forEach(function(elem) {
									collectionToDelete = collectionToDelete.difference(elem.descendants());
									nodeSelection = nodeSelection.union(elem.descendants());
									elem.descendants().addClass("searched");
								});
								collectionToDelete.remove();
								cy.remove(cy.$("node:childless").difference(cy.$("node.searched")));
							};
						});
						setTimeout(function() {resolve_SearchBarPHASING("Done")}, 10);
					});
				});

				/* Postprocess layout */
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						if (zoomMode === false) {
							if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-cola.min.js")) {
								resolve_SearchBar(InitialLayout("euler_cola",true));
							} else {
								if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-fcose.min.js")) {
									resolve_SearchBar(fcoseLayout(false));
								} else {
									resolve_SearchBar("Done");
								};
							};
						} else {
							resolve_SearchBar("Done");
						};
						resolve_SearchBarPHASING("Done");
					});
				});
				
			} else {
				
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						cy_elementsBackup = cy_elementsBackup.union(cy.remove(cy.edges()));
						cy_elementsBackup = cy_elementsBackup.union(cy.remove(cy.nodes()));
						cy_elementsBackup.restore();
						setTimeout(function() {resolve_SearchBarPHASING("Done")}, 10);
					});
				});

				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						cy.nodes().forEach(function(elem) {
							if (elem.hasClass("cluster")) {
								if ((elem.style("border-width") !== "0px") && (elem.style("border-width") !== "0rem")) {
									elem.style("border-color",mainColor_01);
									elem.style("border-width","2rem");
								} else {
									elem.style("color",mainColor_01);
								};
								elem.selectify();
							};
							setTimeout(function() {resolve_SearchBarPHASING("Done")}, 10);
						});
					});
				});

				
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						if (onlySelection === true) {
							nodeSelection = cy.elements("node:selected");
							if (zoomMode === false) {
								let collectionToDelete = cy.collection();
								cy.nodes().forEach(function(elem) {
									if (!elem.isParent() && !elem.selected() && !elem.hasClass("infoTag") && !elem.hasClass("infoBubble")) {
										collectionToDelete = collectionToDelete.union(elem);
									};
								});
								cy.nodes(":compound:selected").forEach(function(elem) {
									collectionToDelete = collectionToDelete.difference(elem.children());
									elem.children().select();
								});
								collectionToDelete.remove();
								let chain_RemoveCompounds = Promise.resolve();
								let intervalRemoveCompounds = setInterval(function() {
									chain_RemoveCompounds = chain_RemoveCompounds.then(function(){
									return new Promise((resolve_RemoveCompounds) => {
											if (cy.$("node:childless").difference(cy.$("node:selected")).length>0) {
												cy.remove(cy.$("node:childless").difference(cy.$("node:selected")));
												resolve_RemoveCompounds("Done");
											} else {
												resolve_RemoveCompounds("Done");
												setTimeout(function() {resolve_SearchBarPHASING("Done")}, 10);
												clearInterval(intervalRemoveCompounds);
											};
										});
									});
								},5);
							};
						} else {
							nodeSelection = cy.elements();
							setTimeout(function() {resolve_SearchBarPHASING("Done")}, 10);
						};
					});
				});

				
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						if (Object.keys(dataGraph_POSITIONS).length === 0) {
							if (zoomMode === false) {
								if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-cola.min.js")) {
									resolve_SearchBar(InitialLayout("euler_cola",true));
								} else {
									if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-fcose.min.js")) {
										resolve_SearchBar(fcoseLayout(false));
									} else {
										resolve_SearchBar("Done");
									};
								};
							} else {
								resolve_SearchBar("Done");
							};
						} else {
							$.each(dataGraph_POSITIONS, function(index, value) {
								cy.getElementById(index).position(value);
								cy.getElementById(index).data("position",value);
							});
							resolve_SearchBar("Done");
						};
						setTimeout(function() {resolve_SearchBarPHASING("Done")}, 10);
					});
				});
				
			};

		});
	});
	
	chain_SearchBar = chain_SearchBar.then(function(){
		return new Promise((resolve_SearchBar) => {
			
			cy.nodes().unselect();
			cy.nodes().removeClass("searched");
			cy.edges().unselect();
			if (checkingInfoTag === true) {
				featuresGraph["infoTags"] = true;
				informationTags(cy.nodes());
				if ($("#toggle_tags").css("display") !== "none") {
					tagsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
					$("#toggle_tags > a").attr("data-original-title","Ne plus afficher les étiquettes");
				};
			} else {
				if ($("#toggle_tags").css("display") !== "none") {
					tagsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
					$("#toggle_tags > a").attr("data-original-title","Afficher les étiquettes");
				};
			};
			if (checkingInfoBubble === true) {
				featuresGraph["infoBubbles"] = true;
				informationBubbles(cy.nodes());
			};
			var modalContent_Summary = document.getElementById("summaryModal_body");
			modalContent_Summary.innerHTML = readNodes_for_modal(cy);
			setTimeout(function() {
				$("#appli_graph_network_CONTAINER").css("pointer-events","auto");
				if (zoomMode === false) {
					resolve_SearchBar(ForceStabilization(nodeSelection.union(cy.nodes(":compound"))));
				} else {
					resolve_SearchBar(ForceStabilization(nodeSelection));
				};
			}, 10);
			
		});
	});

	chain_SearchBar = chain_SearchBar.then(function(){
		return new Promise((resolve_SearchBar) => {
			if (toggleSearchBar_CustomClosing !== undefined) {
				resolve_SearchBar(toggleSearchBar_CustomClosing());
			} else {
				resolve_SearchBar("Done");
			};
		});
	});
		
});
