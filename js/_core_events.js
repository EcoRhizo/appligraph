/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE APPLI GRAPH : SET THE EVENT FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
SCRIPT STRUCTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
	* CORE.A : PREPROCESSING
	* CORE.B : TAP ON THE GRAPH
		* CORE.B.01 : Position indication if not set
		* CORE.B.02 : Double tap activation
	*
	* CORE.C : TAP ON A NODE
		* CORE.C.01 : Select when highlight
		* CORE.C.02 : Creating the modal screen
		* CORE.C.03 : Undo the clusters
		* CORE.C.04 : Filter according to the tags
	*
	* CORE.D : OVERING A NODE
		* CORE.D.01 : Highlight the path
		* CORE.D.02 : Create the tooltips
		* CORE.D.03 : Deal with the cursor style
		* CORE.D.04 : Updating the lateral screen
		* CORE.D.05 : Show the indirect edges
	*
	* CORE.E : OVER OUT A NODE
		* CORE.E.01 : Remove the highlight
		* CORE.E.02 : Remove the tooltip
		* CORE.E.03 : Hide the indirect edges
	*
	* CORE.F : ADDING A NODE
	* CORE.G : REMOVING A NODE
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.A : PREPROCESSING
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var popperTEMP = "";

var tapped = false;

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.B : TAP ON THE GRAPH
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

cy.on("tap", function() {
	
	/* CORE.B.01 : Position indication if not set */
	if (editorModeActivated === true) {
		var positionsTEMP = {};
		cy.$("node:compound").forEach(function(elem) {
			var positionFLOAT = elem.position();
			var positionINTEGER = {"x": parseInt(positionFLOAT["x"]), "y": parseInt(positionFLOAT["y"])};
			positionINTEGER["height"] = parseInt(elem.height())-32;
			positionINTEGER["width"] = parseInt(elem.width())-32;
			positionsTEMP[elem.id()] = positionINTEGER;
		});
		cy.$("node:childless").forEach(function(elem) {
			if ((!elem.hasClass("infoBubble")) && (!elem.hasClass("infoTag")) && (!elem.hasClass("compoundCorner")) && (!elem.hasClass("eh-handle"))) {
				var positionFLOAT = elem.position();
				var positionINTEGER = {"x": parseInt(positionFLOAT["x"]), "y": parseInt(positionFLOAT["y"])};
				positionsTEMP[elem.id()] = positionINTEGER;
			};
		});
		var formContent = document.getElementById("editor_get_positions");
		formContent.value = JSON.stringify(positionsTEMP,null,"\t");
	};
	
	/* CORE.B.02 : Double tap activation */
	if (editorModeActivated === false) {
		if (!tapped) {
			tapped = setTimeout(function(){
				tapped = null;
			},300);
		} else {
			cy.nodes().unselect();
			clearTimeout(tapped);
			tapped=null;
			ForceStabilization();
		};
	};
	
});

/* LINK TO POC NAVIGATION MODE

var x_mouse;

var y_mouse;

function getCursorXY(e) {
	x_mouse = (window.Event) ? e.pageX : event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
	y_mouse = (window.Event) ? e.pageY : event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
};

document.onmousedown = getCursorXY;

cy.on("dblclick", function(e) {
	if (!cy.animated()) {
		cy.animate({
			"duration": 500,
			"zoom": (cy.zoom()+0.5),
			"pan": {"x": cy.width()-x_mouse, "y": cy.height()-y_mouse},
			"easing": "ease"
		});
	};
});

*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.C : TAP ON A NODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

cy.on("tap", "node", function() {
	
	if (!this.hasClass("eh-handle")) {
	
		var networkNodes = this.data();
		
		/* CORE.C.01 : Select when highlight */
		if ((modeHighlighter !== "") && (!this.hasClass("infoBubble")) && (!this.hasClass("infoTag"))) {
			if (!this.isParent()) {
				cy.nodes().difference(".noHighlight").difference(".insensitiveNode").difference(this).select();
			};
			cy.$("node:selected").union(this).edgesWith("node:selected").select();
		};
		
		/* CORE.C.02 : Creating the modal screen */
		if (featuresGraph["infoBubbles"] === true) {
			var networkNodes_BIS = cy.getElementById(this.id().replace("_ib",""))[0].data();
			if (networkNodes["id"] === networkNodes_BIS["id"]) {
				networkNodes_BIS = undefined;
			};
		} else {
			var networkNodes_BIS = networkNodes;
		};
		if (networkNodes_BIS !== undefined) {
			if ( (networkNodes_BIS["titleComplete"] !== "") && (featuresGraph["modalSelection"] === true) && (!Object.keys(listClusters).includes(networkNodes_BIS["id"])) && (isLateral === "closing") && (networkNodes_BIS["titleComplete"] != undefined) ) {
				var modalContent_selectionHeader = document.getElementById("selectionModal_header");
				textTEMP = "";
				if (networkNodes_BIS["image"] !== undefined) {textTEMP = textTEMP.concat("<img class=\"lazyload\" data-src=\"",networkNodes_BIS["image"],"\"></img>");};
				if (networkNodes_BIS["label"] !== undefined) {textTEMP = textTEMP.concat(networkNodes_BIS["label"]);} else {textTEMP = textTEMP.concat("");};
				modalContent_selectionHeader.innerHTML = textTEMP;
				var modalContent_selectionHeader = document.getElementById("selectionModal_body");
				modalContent_selectionHeader.innerHTML = networkNodes_BIS["titleComplete"];
				$('#selectionModal').modal('show');
				if (featuresGraph["modalSelection"] === true) { document.body.style.cursor = idleCursor; };
			};
		};
		
		/* CORE.C.03 : Undo the clusters */
		if (Object.keys(listClusters).includes(networkNodes["id"])) {
			UndoClustering(this,true,false);
		};
		
		/* CORE.C.04 : Filter according to the tags */
		if ( (featuresGraph["featureSearchBar"] === true) && (this.hasClass("infoTag")) ) {
			let currentNodeTag = this.data("tagName");
			let currentNodeLabel = this.data("label");
			cy.nodes().difference(cy.nodes(":parent")).forEach(function (elem) {
				if (elem.hasClass("defaultNode")) {
					if (!Object.keys(elem.data("searchTag")).includes(currentNodeTag)) {
						elem.remove();
						cy.$("[id = "+"'"+elem.id()+"_ib"+"'"+"]").remove();
						cy.$("[id ^= "+"'"+elem.id()+"_tag"+"'"+"]").remove();
					} else {
						if (elem.data("searchTag")[currentNodeTag].indexOf(currentNodeLabel) === -1) {
							elem.remove();
							cy.$("[id = "+"'"+elem.id()+"_ib"+"'"+"]").remove();
							cy.$("[id ^= "+"'"+elem.id()+"_tag"+"'"+"]").remove();
						};
					};
				};
			});
		};
		
	};
	
});

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.D : OVERING A NODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

cy.on("mouseover", "node", function() {
	
	var elem = document.getElementById("appli_graph_main_screen");
	
	var networkNodes = this.data();
	
	/* CORE.D.01 : Highlight the path */
	if ((cy.nodes(".noHighlight").length === 0) && (modeHighlighter !== "") && (!this.hasClass("infoBubble")) && (!this.hasClass("infoTag")) && (!this.isParent())) {
		cy.elements().stop();
		cy.nodes().addClass("noHighlight");
		cy.edges().addClass("noHighlight");
		cy.nodes(".insensitiveNode").removeClass("noHighlight");
		cy.edges(".insensitiveEdge").removeClass("noHighlight");
		cy.edges(".indirectEdge").removeClass("noHighlight");
		this.removeClass("noHighlight");
		if ((modeHighlighter === "ascendency") | (modeHighlighter === "both") ) {this.predecessors().removeClass("noHighlight")};
		if ((modeHighlighter === "downdency") | (modeHighlighter === "both") ) {this.successors().removeClass("noHighlight")};
		cy.nodes().difference(cy.nodes(".noHighlight")).style("opacity",1);
		cy.nodes(".noHighlight").difference(cy.$("node:parent")).animate({"duration": 50, "style": {"opacity":cy.nodes(".noHighlight").difference(cy.$("node:parent")).style("opacity"), "opacity": 0.25}});
		cy.edges(".noHighlight").animate({"duration": 50, "style": {"opacity": cy.edges(".noHighlight").style("opacity"), "opacity": 0.25}});
	};

	/* CORE.D.02 : Create the tooltips */
	if ( (featuresGraph["tooltips"] === true) && (popperTEMP === "") && (this.data()["title"] != undefined) && (this.data()["title"] != "") ) {	
		popperTEMP = this.popper({
		  content: () => {
			let tooltipTEMP = document.createElement('div');
			tooltipTEMP.id = "popper";
			tooltipTEMP.innerHTML = this.data()["title"];
			document.body.appendChild(tooltipTEMP);
			return tooltipTEMP;
		  },
		  popper: {
				placement: 'left',
				modifiers: {
					flip: {behavior: ['left', 'bottom', 'top']},
					preventOverflow: {boundariesElement: elem}
				}
		  }
		});
	};

	/* CORE.D.03 : Deal with the cursor style */
	if ( (networkNodes["titleComplete"] !== "") && (featuresGraph["modalSelection"] === true) && (!Object.keys(listClusters).includes(this.data()["id"])) && (isLateral === "closing") && (this.data()["titleComplete"] != undefined) ) {
		if (featuresGraph["infoBubbles"] === false) {
			document.body.style.cursor = 'help';
		};
	};
	if (this.hasClass("infoBubble")) {
		var networkNodes_BIS = cy.getElementById(this.id().replace("_ib",""))[0].data();
		if (networkNodes_BIS["titleComplete"] !== undefined) {document.body.style.cursor = 'pointer'};
	};
	if (Object.keys(listClusters).includes(this.data()["id"])) {
		document.body.style.cursor = 'pointer';
	};
	if ( (featuresGraph["featureSearchBar"] === true) && (this.hasClass("infoTag")) ) {
		document.body.style.cursor = 'pointer';
	};
	
	/* CORE.D.04 : Updating the lateral screen */
	if (( (elem.style.width === "60%") || (elem.style.height === "60%") ) && (editorModeActivated === false) ) {
		var textTEMP = "";
		var networkNodes = this.data();
		if (networkNodes["titleComplete"] === undefined)
		{
			textTEMP = textTEMP.concat("<h1>Survolez les éléments pour plus d'informations</h1>");
			textTEMP = textTEMP.concat("<div class=\"lateral-hr\"><hr class=\"my-6\"></div>");
		} else {
			textTEMP = textTEMP.concat("<h1>");
			if (networkNodes["image"] !== undefined) {textTEMP = textTEMP.concat("<img class=\"lazyload\" data-src=\"",networkNodes["image"],"\"></img>");};
			if (networkNodes["label"] !== undefined) {textTEMP = textTEMP.concat(networkNodes["label"]);} else {textTEMP = textTEMP.concat("");};
			textTEMP = textTEMP.concat("</h1>");
			textTEMP = textTEMP.concat("<div class=\"lateral-hr\"><hr class=\"my-6\"></div>");
			textTEMP = textTEMP.concat(networkNodes["titleComplete"]);
		};
		var lateralContent = document.getElementById("tooltipLateral_body");
		lateralContent.innerHTML = textTEMP;
	};
	
	/* CORE.D.05 : Show the indirect edges */
	this.connectedEdges(".indirectEdge").forEach(function(elem) {
		elem.toggleClass("indirectEdge_hidden",false);
	});
	
});

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.E : OVER OUT A NODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

cy.on("mouseout", "node", function() {
	
	/* CORE.E.01 : Remove the highlight */
	if ((modeHighlighter !== "") && (!this.hasClass("infoBubble")) && (!this.hasClass("infoTag"))) {
		cy.elements().stop();
		cy.nodes().difference(cy.nodes(".noHighlight")).style("opacity",1);
		nodesNoHighlight = cy.nodes(".noHighlight");
		edgesNoHighlight = cy.edges(".noHighlight");
		cy.nodes(".noHighlight").removeClass("noHighlight");
		cy.edges(".noHighlight").removeClass("noHighlight");
		nodesNoHighlight.animate({
			"duration": 50,
			"style": {"opacity": nodesNoHighlight.style("opacity"), "opacity": 1}
		});
		edgesNoHighlight.animate({
			"duration": 50,
			"style": {"opacity": edgesNoHighlight.style("opacity"), "opacity": 1}
		});
	};
	document.body.style.cursor = idleCursor;
	
	/* CORE.E.02 : Remove the tooltip */
	if (popperTEMP !== "") {
		popperTEMP.destroy();
		document.getElementById("popper").remove();
		popperTEMP = "";
	};
	
	/* CORE.E.03 : Hide the indirect edges */
	this.connectedEdges(".indirectEdge").forEach(function(elem) {
		if ($("#toggle_ind_link").css("background-color") !== "rgb(255, 255, 255)") {elem.toggleClass("indirectEdge_hidden",true)};
	});
	
});

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.F : ADDING A NODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

cy.on("add", "node", function() {
	if (!this.hasClass("infoBubble") && !this.hasClass("infoTag")) {
		if (featuresGraph["infoBubbles"] === true) {informationBubbles(this);};
		if (featuresGraph["infoTags"] === true) {informationTags(this);};
	};
});

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.G : REMOVING A NODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

let chain_RemovingNodes = Promise.resolve();

cy.on("remove", "node", function() {
	if (!this.hasClass("infoBubble") && !this.hasClass("infoTag")) {
		var node_id = this.id();
		chain_RemovingNodes = chain_RemovingNodes.then(function(){
			return new Promise((resolve_RemovingNodes) => {
				cy.$("[id ^= "+"'"+node_id+"_ib"+"'"+"]").remove();
				cy.$("[id ^= "+"'"+node_id+"_tag"+"'"+"]").remove();
				resolve_RemovingNodes("Done");
			});
		});
	};
});

