/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
_VOID_TEMPLATES_COMPLETION.JS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

if (featuresGraph["animation"] === undefined) {featuresGraph["animation"] = false;};
if (featuresGraph["sound"] === undefined) {featuresGraph["sound"] = false;};
if (featuresGraph["physics"] === undefined) {featuresGraph["physics"] = false;};
if (featuresGraph["static"] === undefined) {featuresGraph["static"] = false;};
if (featuresGraph["compaction"] === undefined) {featuresGraph["compaction"] = 1;};
if (featuresGraph["padding"] === undefined) {featuresGraph["padding"] = 50;};
if (featuresGraph["fcoseOnLoad"] === undefined) {featuresGraph["fcoseOnLoad"] = false;};
if (featuresGraph["editorOnLoad"] === undefined) {featuresGraph["editorOnLoad"] = false;};
if (featuresGraph["mapOnLoad"] === undefined) {featuresGraph["mapOnLoad"] = false;};
if (featuresGraph["featureReload"] === undefined) {featuresGraph["featureReload"] = false;};
if (featuresGraph["featureClustering"] === undefined) {featuresGraph["featureClustering"] = false;};
if (featuresGraph["featureSearchBar"] === undefined) {featuresGraph["featureSearchBar"] = false;};
if (featuresGraph["tooltips"] === undefined) {featuresGraph["tooltips"] = false;};
if (featuresGraph["modalSelection"] === undefined) {featuresGraph["modalSelection"] = false;};
if (featuresGraph["infoBubbles"] === undefined) {featuresGraph["infoBubbles"] = false;};
if (featuresGraph["infoTags"] === undefined) {featuresGraph["infoTags"] = false;};
if (featuresGraph["backgroundCanvas"] === undefined) {featuresGraph["backgroundCanvas"] = "";};
if (featuresGraph["defaultIcons"] === undefined) {featuresGraph["defaultIcons"] = true;};
if (featuresGraph["customIcons"] === undefined) {featuresGraph["customIcons"] = {"page_icon": "","page_title": ""}};
if (featuresGraph["initialClustering"] === undefined) {featuresGraph["initialClustering"] = [];};
if (featuresGraph["description"] === undefined) {featuresGraph["description"] = "Pas de description renseignée sur ce gabarit.";};

if (featuresGraph_SearchBarOptions["zoom"] === undefined) {featuresGraph_SearchBarOptions["zoom"] = false;};
if (featuresGraph_SearchBarOptions["selected"] === undefined) {featuresGraph_SearchBarOptions["selected"] = false;};
if (featuresGraph_SearchBarOptions["and"] === undefined) {featuresGraph_SearchBarOptions["and"] = false;};
if (featuresGraph_SearchBarOptions["or"] === undefined) {featuresGraph_SearchBarOptions["or"] = false;};

if (featuresColors_FORCING["--main_color_01"] === undefined) {featuresColors_FORCING["--main_color_01"] = "";};
if (featuresColors_FORCING["--main_color_02"] === undefined) {featuresColors_FORCING["--main_color_02"] = "";};
if (featuresColors_FORCING["--main_color_03"] === undefined) {featuresColors_FORCING["--main_color_03"] = "";};
if (featuresColors_FORCING["--main_color_04"] === undefined) {featuresColors_FORCING["--main_color_04"] = "";};
if (featuresColors_FORCING["--main_color_05"] === undefined) {featuresColors_FORCING["--main_color_05"] = "";};
if (featuresColors_FORCING["--main_color_01_faded"] === undefined) {featuresColors_FORCING["--main_color_01_faded"] = "";};
if (featuresColors_FORCING["--main_color_03_faded"] === undefined) {featuresColors_FORCING["--main_color_03_faded"] = "";};
if (featuresColors_FORCING["--main_color_editor_01"] === undefined) {featuresColors_FORCING["--main_color_editor_01"] = "";};
if (featuresColors_FORCING["--main_color_editor_02"] === undefined) {featuresColors_FORCING["--main_color_editor_02"] = "";};



if (featuresButtons_FORCING["toggle_fullscreen"] === undefined) {featuresButtons_FORCING["toggle_fullscreen"] = "";};
if (featuresButtons_FORCING["toggle_graphs"] === undefined) {featuresButtons_FORCING["toggle_graphs"] = "false";};
if (featuresButtons_FORCING["toggle_lateralScreen"] === undefined) {featuresButtons_FORCING["toggle_lateralScreen"] = "false";};
if (featuresButtons_FORCING["toggle_map"] === undefined) {featuresButtons_FORCING["toggle_map"] = "false";};
if (featuresButtons_FORCING["toggle_legend"] === undefined) {featuresButtons_FORCING["toggle_legend"] = "false";};
if (featuresButtons_FORCING["toggle_tags"] === undefined) {featuresButtons_FORCING["toggle_tags"] = "false";};
if (featuresButtons_FORCING["toggle_stabilization"] === undefined) {featuresButtons_FORCING["toggle_stabilization"] = "false";};
if (featuresButtons_FORCING["toggle_physics"] === undefined) {featuresButtons_FORCING["toggle_physics"] = "false";};
if (featuresButtons_FORCING["toggle_fcose"] === undefined) {featuresButtons_FORCING["toggle_fcose"] = "false";};
if (featuresButtons_FORCING["toggle_ori_pos"] === undefined) {featuresButtons_FORCING["toggle_ori_pos"] = "false";};
if (featuresButtons_FORCING["toggle_ind_link"] === undefined) {featuresButtons_FORCING["toggle_ind_link"] = "false";};
if (featuresButtons_FORCING["toggle_highlighter"] === undefined) {featuresButtons_FORCING["toggle_highlighter"] = "false";};
if (featuresButtons_FORCING["toggle_modeEditor"] === undefined) {featuresButtons_FORCING["toggle_modeEditor"] = "false";};
if (featuresButtons_FORCING["toggle_summary"] === undefined) {featuresButtons_FORCING["toggle_summary"] = "false";};
if (featuresButtons_FORCING["toggle_save"] === undefined) {featuresButtons_FORCING["toggle_save"] = "false";};
if (featuresButtons_FORCING["toggle_information"] === undefined) {featuresButtons_FORCING["toggle_information"] = "";};
if (featuresButtons_FORCING["toggle_bug"] === undefined) {featuresButtons_FORCING["toggle_bug"] = "";};


if (featuresLoading_FORCING["eulerComputation"] === undefined) {featuresLoading_FORCING["eulerComputation"] = "false";};
if (featuresLoading_FORCING["showInformation"] === undefined) {featuresLoading_FORCING["showInformation"] = "false";};
if (featuresLoading_FORCING["clickToBegin"] === undefined) {featuresLoading_FORCING["clickToBegin"] = "false";};
if (featuresLoading_FORCING["blank"] === undefined) {featuresLoading_FORCING["blank"] = "false";};
