/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : GENERATING THE CANVAS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var background = new Image();
background.onload = () => {
	const bottomLayer = cy.cyCanvas();
	var canvas = bottomLayer.getCanvas();
	var ctxCanvas = canvas.getContext("2d");
	cy.on("render cyCanvas.resize", (evt) => {
		bottomLayer.resetTransform(ctxCanvas);
		bottomLayer.clear(ctxCanvas);
		bottomLayer.setTransform(ctxCanvas);
		ctxCanvas.save();
		ctxCanvas.drawImage(background, 0, 0);
	});
};
background.src = idGraph_path+featuresGraph["backgroundCanvas"];
