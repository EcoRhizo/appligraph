/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : INFO TAGS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/* Make the child position refresh on dragging a parent node */
cy.on("position", "node:parent", function() {
	this.descendants().forEach(function(descendant) {
		descendant.position(descendant.position());
	});
});

/* Information tags function : display tags near nodes with information in searchTag data */
function informationTags(nodesToTag) {
	var dictColorsTags_list = ["#CC4E5C","#048B9A","#689D71","#B666D2","#EF9B0F","#E9C9B1","#FDBFB7"];
	var dictColorsTags_generation = {};
	var dictColorsTags_counter = 0;
	if (featuresButtons_FORCING["toggle_legend"] !== "false") {
		$("#toggle_legend").css("display","initial");
		$("#toggle_legend_BR").css("display","initial");
		$("#toggle_legend_LI").css("display","list-item");
	};
	if (featuresButtons_FORCING["toggle_tags"] !== "false") {
		$("#toggle_tags").css("display","initial");
		$("#toggle_tags_BR").css("display","initial");
		$("#toggle_tags_LI").css("display","list-item");
	};
	nodesToTag.forEach(function(elem) {
		if (elem.data("searchTag") !== undefined) {
			if ( (Object.keys(elem.data("searchTag")).length >= 1) && (elem.hasClass("defaultNode")) ) {
				/* if (!elem.hasClass("cluster")) { */
					var promise_infoTag = new Promise(function(resolveInfoTag, rejectInfoTag) {
						var tagCOUNTER = 0;
						var intervalTag = setInterval(function() {
							tagITERATE = Object.keys(elem.data()["searchTag"])[tagCOUNTER];
							if (tagITERATE !== "Labels") {
								if (Object.keys(dictColorsTags_custom).includes(tagITERATE)) {
									dictColorsTags_generation[tagITERATE] = dictColorsTags_custom[tagITERATE];
								} else {
									if (dictColorsTags_generation[tagITERATE] === undefined) {
										dictColorsTags_generation[tagITERATE] = dictColorsTags_list[dictColorsTags_counter];
										dictColorsTags_counter = dictColorsTags_counter+1;
									};
								};
								if (cy.getElementById(elem.id()+"_tag_"+tagCOUNTER).length === 0) {
									cy.add({
										"group": "nodes",
										"selectable": false,
										"grabbable": false,
										"data": {
											"id": elem.id()+"_tag_"+tagCOUNTER,
											"tagName" : tagITERATE,
											"label": elem.data()["searchTag"][tagITERATE].join(" / ")
										}
									});
									cy.getElementById(elem.id()+"_tag_"+tagCOUNTER).style({"background-color": dictColorsTags_generation[tagITERATE]});
									cy.getElementById(elem.id()+"_tag_"+tagCOUNTER).addClass("infoTag_hidden");
									cy.getElementById(elem.id()+"_tag_"+tagCOUNTER).addClass("infoTag");
								};
							};
							tagCOUNTER = tagCOUNTER+1;
							if (tagCOUNTER === Object.keys(elem.data()["searchTag"]).length) {
								clearInterval(intervalTag);
								resolveInfoTag("");
							};
						}, 5);
					});
					promise_infoTag.then(function(valueInfoTag) {
						var promise_infoTag_BIS = new Promise(function(resolveInfoTag_BIS, rejectInfoTag_BIS) {
							var tagCOUNTER_BIS = 0;
							var intervalTag_BIS = setInterval(function() {
								tagITERATE = Object.keys(elem.data()["searchTag"])[tagCOUNTER_BIS];
								if ((cy.getElementById(elem.id()+"_tag_"+tagCOUNTER_BIS).length > 0) && (tagITERATE !== "Labels")) {
									if (tagCOUNTER_BIS === 0) {
										var nodeBB_TEMP = elem.boundingBox({"includeOverlays": false});
										cy.getElementById(elem.id()+"_tag_"+tagCOUNTER_BIS).position({"x": nodeBB_TEMP["x1"], "y": nodeBB_TEMP["y2"]});
										var codeCOUNTER = tagCOUNTER_BIS;
										elem.removeListener("position");
										elem.on("position", function() {
											var nodeBB_TEMP = this.boundingBox({"includeOverlays": false});
											cy.getElementById(elem.id()+"_tag_"+codeCOUNTER).position({"x": nodeBB_TEMP["x1"], "y": nodeBB_TEMP["y2"]});
										});
									} else {
										var currentTagWidth = cy.getElementById(elem.id()+"_tag_"+tagCOUNTER_BIS).outerWidth();
										var currentTagBB = cy.getElementById(elem.id()+"_tag_"+tagCOUNTER_BIS).boundingBox({"includeOverlays": false});
										var lastTagBB = cy.getElementById(elem.id()+"_tag_"+(tagCOUNTER_BIS-1)).boundingBox({"includeOverlays": false});
										var lastTagPosition = cy.getElementById(elem.id()+"_tag_"+(tagCOUNTER_BIS-1)).position();
										var nodeBB_TEMP = {"x1": lastTagBB["x1"]+(currentTagBB["w"]/2), "y2": lastTagPosition["y"]+25};
										cy.getElementById(elem.id()+"_tag_"+tagCOUNTER_BIS).position({"x": nodeBB_TEMP["x1"], "y": nodeBB_TEMP["y2"]});
										var codeCOUNTER = tagCOUNTER_BIS;
										cy.getElementById(elem.id()+"_tag_"+(codeCOUNTER-1)).removeListener("position");
										cy.getElementById(elem.id()+"_tag_"+(codeCOUNTER-1)).on("position", function() {
											var currentTagWidth = cy.getElementById(elem.id()+"_tag_"+codeCOUNTER).outerWidth();
											var currentTagBB = cy.getElementById(elem.id()+"_tag_"+codeCOUNTER).boundingBox({"includeOverlays": false});
											var lastTagBB = cy.getElementById(this.id()).boundingBox({"includeOverlays": false});
											var lastTagPosition = cy.getElementById(this.id()).position();
											var nodeBB_TEMP = {"x1": lastTagBB["x1"]+(currentTagBB["w"]/2), "y2": lastTagPosition["y"]+25};
											cy.getElementById(elem.id()+"_tag_"+codeCOUNTER).position({"x": nodeBB_TEMP["x1"], "y": nodeBB_TEMP["y2"]});
										});
									};
									cy.getElementById(elem.id()+"_tag_"+tagCOUNTER_BIS).removeClass("infoTag_hidden");
								};
								tagCOUNTER_BIS = tagCOUNTER_BIS+1;
								if (tagCOUNTER_BIS === Object.keys(elem.data()["searchTag"]).length) {
									clearInterval(intervalTag_BIS);
									resolveInfoTag_BIS("");
								};
							}, 5);
						});
						promise_infoTag_BIS.then(function(valueInfoTag_BIS) {
							var textTEMP = "<h1>Étiquettes :</h1>";
							for (tagTEMP in dictColorsTags_generation) {
								var tagTEMP_upper = tagTEMP;
								tagTEMP_upper = tagTEMP_upper.substr(0,1).toUpperCase()+tagTEMP_upper.substr(1)+".";
								textTEMP = textTEMP.concat("<p>"+"<svg width=\"45\" height=\"30\"><polygon points=\"0,30 30,30 45,15 30,0 0,0\"  style=\"fill:"+dictColorsTags_generation[tagTEMP]+"\"/></svg>"+" : "+tagTEMP_upper+"</p>");
							};
							var legendContent = document.getElementById("legendModal_body_TAGS");
							legendContent.innerHTML = textTEMP;
							var legendContent = document.getElementById("legendAccordion_body_TAGS");
							if (legendContent && legendContent !== "null" && legendContent != undefined) {legendContent.innerHTML = textTEMP;}; /* TO MODIFY WITH NEW FEATURE OF AURBA */
						});
					});
				/* }; */
			};			
		};
	});
};