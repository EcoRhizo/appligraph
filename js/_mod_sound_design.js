/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : SOUND DESIGN
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

if (featuresGraph["sound"] === true) {
	
	var soundSelect = new Howl({
		html5: true,
		preload: true,
		format: ["wav"],
		src: ["sound/soundSelect.wav"],
	});
	
	cy.on("select", "node", function() {
		if (!soundSelect.playing()) {soundSelect.play()};
	});
	
	cy.on("unselect", "node", function() {
		setTimeout(function(){
			if (cy.$("node:selected").length === 0) {
				if (!soundSelect.playing()) {soundSelect.play()};
			};
		},100);
	});
	
	var soundGrab = new Howl({
		html5: true,
		preload: true,
		format: ["wav"],
		src: ["sound/soundGrab.wav"],
		onplay: function() {
			var nodeGrab = cy.$("node:grabbed");
			if (nodeGrab.style("opacity") === "1") {nodeGrab.style("opacity",0.5)} else {nodeGrab.style("opacity",1)};
		},
		onfade: function () {
			soundGrab.loop(false);
			soundGrab.stop()
		}
	});
	
	cy.on("grabon", "node", function() {
		var nodeGrab = this.id();
		setTimeout(function(){
			if (cy.$("node:grabbed").id() === nodeGrab) {
				soundGrab.volume(1);
				soundGrab.loop(true);
				soundGrab.play();
			};
		},150);
	});
	
	cy.on("freeon", "node", function() {
		soundGrab.fade(1,0,100);
		this.style("opacity",1);
	});

};