/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
_VOID_TEMPLATES.JS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var featuresGraph_dictFun = {
	"animation": {"name": "Animation en introduction", "choice": "boolean"},
	"sound": {"name": "Effets sonores", "choice": "boolean"},
	"physics": {"name": "Physique des éléments", "choice": "boolean"},
	"static": {"name": "Éléments figés", "choice": "boolean"},
	"compaction": {"name": "Compaction du graphe", "choice": "undefined"},
	"padding": {"name": "Espace autour du graphe", "choice": "undefined"},
	"fcoseOnLoad": {"name": "Réorganisation du graphe au chargement", "choice": "boolean"},
	"editorOnLoad": {"name": "Menu éditeur activé au chargement", "choice": "boolean"},
	"mapOnLoad": {"name": "Cartographie activée au chargement", "choice": "boolean"},
	"featureReload": {"name": "Bouton de réinitialisation", "choice": "boolean"},
	"featureClustering": {"name": "Fonction de regroupement des éléments", "choice": "boolean"},
	"featureSearchBar": {"name": "Barre de recherche", "choice": "boolean"},
	"tooltips": {"name": "Pop-up au survol des éléments", "choice": "boolean"},
	"modalSelection": {"name": "Fiche descriptive au clic des éléments", "choice": "boolean"},
	"infoBubbles": {"name": "Bulles d\'information", "choice": "boolean"},
	"infoTags": {"name": "Étiquettes", "choice": "boolean"},
	"backgroundCanvas": {"name": "Canevas de fond", "choice": "undefined"},
	"defaultIcons": {"name": "Icônes par défaut", "choice": "boolean"},
	"customIcons": {"name": "Icônes personnalisées", "choice": "undefined"},
	"initialClustering": {"name": "Regroupement d\'éléments au chargement", "choice": "undefined"},
	"description": {"name": "Description du gabarit dans l\'application", "choice": "undefined"}
};

var featuresButtons_dictFun = {
	"toggle_fullscreen": {"name": "Afficher en plein écran","svg": "icon/arrows-alt.svg"},
	"toggle_graphs": {"name": "Retourner à la sélection du gabarit","svg": "icon/appli-graph.svg"},
	"toggle_lateralScreen": {"name": "Afficher le volet latéral pour plus de précisions sur les éléments","svg": "icon/eye.svg"},
	"toggle_map": {"name": "Basculer vers la carte géographique","svg": "icon/map.svg"},
	"toggle_legend": {"name": "Afficher la légende","svg": "icon/list-ul.svg"},
	"toggle_tags": {"name": "Affichage des étiquettes","svg": "icon/tag.svg"},
	"toggle_stabilization": {"name": "Recentrer la vue","svg": "icon/map-marker-alt.svg"},
	"toggle_physics": {"name": "Gestion de la physique","svg": "icon/shapes.svg"},
	"toggle_fcose": {"name": "Améliorer la disposition des éléments","svg": "icon/fcose.svg"},
	"toggle_ori_pos": {"name": "Retrouver les positions initiales","svg": "icon/ori_pos.svg"},
	"toggle_ind_link": {"name": "Afficher les liens indirects","svg": "icon/ind_link.svg"},
	"toggle_highlighter": {"name": "Mise en avant des éléments","svg": "icon/highlight_self.svg"},
	"toggle_modeEditor": {"name": "Mode éditeur","svg": "icon/pencil-alt.svg"},
	"toggle_summary": {"name": "Résumé du graphe","svg": "icon/align-left.svg"},
	"toggle_save": {"name": "Exporter le contenu du graphe","svg": "icon/file-alt.svg"},
	"toggle_information": {"name": "Informations sur l\'application","svg": "icon/information-alt.svg"},
	"toggle_bug": {"name": "Faire remonter un problème","svg": "icon/bug.svg"}
};

var featuresLoading_dictFun = {
	"eulerComputation": "Algorithme de disposition Euler",
	"showInformation": "Activer le tutoriel",
	"clickToBegin": "Cliquer pour commencer",
	"debug": "Mode debug",
	"blank": "Marque blanche"
};

var graphVersion = "";

var featuresGraph = {};

var featuresGraph_SearchBarOptions = {};

var featuresColors_FORCING = {};

var featuresColors_FILTER = "";

var featuresButtons_FORCING = {};

var featuresLoading_FORCING = {};

var scriptGraph_GET = [];

var viewsButtons = [];

var options_Modifier = [];

var options_Groups = [];

var dictColorsTags_custom = {};

var dataGraph_LEGEND = "";

var dataGraph_POSITIONS = {};

var dataGraph_GET = [];
