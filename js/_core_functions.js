/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE APPLI GRAPH : SET THE FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
SCRIPT STRUCTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
	* CORF.A : PREPROCESSING
		* CORF.A.01 : Tooltip preparation
		* CORF.A.02 : JQuery plugin to detect css changes
	*
	* CORF.B : IMPORT AND EXPORT FUNCTIONS
		* CORF.B.01 : Main function for the import
		* CORF.B.02 : Import nodes data
		* CORF.B.03 : Import edges data
	*
	* CORF.C : CORE FUNCTIONS FOR CYTOSCAPE
		* CORF.C.01 : Creating a specific view
		* CORF.C.02 : Force the network to stabilize
		* CORF.C.03 : Show lateral screen
		* CORF.C.04 : Generate the body of the content modal string
		* CORF.C.05 : Initial Layout
	*
	* CORF.D : FUNCTIONS LINKED TO THE BUTTONS
		* CORF.D.01 : Function to show the bug modal screen
		* CORF.D.02 : Fullscreen function
		* CORF.D.03 : Save modal as doc
		* CORF.D.04 : For the viewer button
		* CORF.D.05 : For the graph button
		* CORF.D.06 : To deal with device orientation
		* CORF.D.07 : Running fcose layout
		* CORF.D.08 : Go to initial positions
	*
*/


/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORF.A : GENERAL FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/


/*
--------------------------------
CORF.A.01 : Tooltip preparation
--------------------------------
*/

$(document).ready(function(){
	/* Initialize tooltip */
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'auto',
		delay : 0,
		template : '<div class="tooltip"><div class="tooltip-inner"></div></div>'
    });
});

$("[data-toggle='tooltip']").on('show.bs.tooltip', function(){
	setTimeout(function () {$("[data-toggle='tooltip']").tooltip('hide')},1000);
});

/*
--------------------------------
CORF.A.02 : JQuery plugin to detect css changes
-------------------------------- 
*/

(function() {
	orig = $.fn.css;
	$.fn.css = function() {
		var result = orig.apply(this, arguments);
		$(this).trigger('stylechanged');
		return result;
	}
})();


/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORF.B : IMPORT AND EXPORT FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
CORF.B.01 : Main function for the import
--------------------------------
*/

/** Function to convert an object from a json to a data variable suitable for a cy object
 * dataINPUT : object from a json
*/
function importNetwork(dataINPUT,cy_for_import) {

	let dataINPUT_nodes;

	let dataINPUT_edges;

	if ((Object.keys(dataINPUT)[0] === "nodes") && (Object.keys(dataINPUT)[1] === "edges")) {
		dataINPUT_nodes = dataINPUT["nodes"];
		dataINPUT_edges = dataINPUT["edges"];
	} else {
		dataINPUT_nodes = dataINPUT;
	};

	if ((dataINPUT_nodes !== undefined) && (dataINPUT_nodes.length > 0)) {getNodeData(dataINPUT_nodes,cy_for_import)};

	if ((dataINPUT_nodes !== undefined) && (dataINPUT_nodes.length > 0)) {getEdgeData(dataINPUT_nodes,"nodeData",cy_for_import,"sourcesNodes","direct")};

	if ((dataINPUT_edges !== undefined) && (dataINPUT_edges.length > 0)) {getEdgeData(dataINPUT_edges,"edgeData",cy_for_import,"","")};
	
	let rootsTEMP = cy_for_import.nodes().roots();
	
	/* Adjust the ranks */
	cy_for_import.elements().breadthFirstSearch({
		"root": rootsTEMP,
		"visit": function(v, e, u, i, depth) {
			v.data("rank",depth+1);
		}
	});
	
	/* Add the indirect edges */
	if ((dataINPUT_nodes !== undefined) && (dataINPUT_nodes.length > 0)) {getEdgeData(dataINPUT_nodes,"nodeData",cy_for_import,"sourcesNodesIndirect","indirect")};
	
	/* Force the children of the node that had a compound to be in that compound (if they have not a parent) */
	/*
	cy_for_import.nodes().forEach(function(elem) {
		if (elem.isChild()) {
			elem.successors().nodes().forEach(function(elem_child) {
				if (!elem_child.isChild()) {elem_child.move({"parent":elem.parent().id()});};
			});
		};
	});
	*/

};

/*
--------------------------------
CORF.B.02 : Import nodes data
--------------------------------
*/

/** Function to convert nodes attributes from a json object into a suitable node object and return a vis.DataSet object
 * data : object from a json
 * The attributes are hardcoded in the following function.
 * The "id" attribute is complete with all the sourcesNodes (based on the first parent encounter in the "sourcesNodes" attribute).
*/
function getNodeData(data,cy_core) {
	
	var networkNodes = [];

	data.forEach(function(elem, index, array) {
		
		let objectNodes = {
			"group": "nodes",
			"selectable": elem.selectable,
			"grabbable": elem.grabbable,
			"pannable": elem.pannable,
			"data": {
				"index": index,
				"id": elem.id,
				"lat": elem.lat,
				"lng": elem.lng,
				"label": elem.label,
				"title": elem.title,
				"titleComplete": elem.titleComplete,
				"other": elem.other,
				"image": elem.image,
				"style": elem.style,
				"group": elem.group,
				"insensitiveNode": elem.insensitiveNode,
				"searchTag": elem.searchTag,
				"parent": elem.parent,
				"sourcesNodes": elem.sourcesNodes,
				"sourcesNodesIndirect": elem.sourcesNodesIndirect
			}
		};
		
		if (elem.additionalData !== undefined) {
			for (dataOther in elem.additionalData) {
				objectNodes["data"][dataOther] = elem.additionalData[dataOther];
			};
		};
		
		if (elem.label !== undefined) {
			if (elem.searchTag === undefined) {
				objectNodes["data"]["searchTag"] = {"Labels": [objectNodes["data"]["label"]]};
			} else {
				objectNodes["data"]["searchTag"]["Labels"] = [objectNodes["data"]["label"]];
			};
		};
		
		if (elem.title !== undefined) { objectNodes["data"]["title"] = objectNodes["data"]["title"].replace(/src=\"/g,"src=\""+idGraph_path); };
		
		if (elem.titleComplete !== undefined) { objectNodes["data"]["titleComplete"] = objectNodes["data"]["titleComplete"].replace(/src=\"/g,"src=\""+idGraph_path); };
		
		if (elem.image !== undefined) {
			if (objectNodes["data"]["image"].indexOf("http") === -1) {
				objectNodes["data"]["image"] = idGraph_path+objectNodes["data"]["image"];
			};
		};
		
		networkNodes.push(objectNodes);
		
	});
	
	cy_core.add(networkNodes);

	cy_core.nodes().forEach(function(elem) {
		
		/* Edit the style of the nodes with a custom style */
		if ((elem.data("style") !== undefined) && (elem.data("style") !== "")) {
			if (typeof elem.data("style") === typeof "") {
				elem.style(JSON.parse(elem.data("style")));
			} else {
				elem.style(elem.data("style"));
			};
		};
		
		/* Make insensitive the node with that parameter */
		if (elem.data("insensitiveNode") === true) {elem.addClass("insensitiveNode")};
		
		/* Add to all nde the class defaultNode */
		elem.addClass("defaultNode");
		
	});
	
	return networkNodes;
	
};

/*
--------------------------------
CORF.B.03 : Import edges data
--------------------------------
*/

/** Function to convert edges attributes from a json object into a suitable edge object and return a vis.DataSet object
 * data : object from a json
 * The "from" and "to" attribute is complete with all the sources nodes (based on the first parent encounter in the "sourcesNodes" attribute).
*/
function getEdgeData(data,typeData,cy_core,sourceData,edgesType) {
	
	let networkEdges = [];

	function makeEdgesFromData(nodeFrom, nodeTo, edgesType) {
		/* 
			If edgesType is "nodeData" :
				nodeFrom = object of dataGraph_GET variable
				nodeTo = value of the key "sourceData" of dataGraph_GET variable => a string of a node id or an object
			If edgeType is "edgeData"
		*/

		let nodeFrom_TEMP;

		let nodeTo_TEMP;

		if (typeof nodeFrom === typeof "") {
			nodeFrom_TEMP = nodeFrom;
		} else {
			if (nodeFrom["id"] === undefined) {
				nodeFrom_TEMP = nodeFrom["from"];
			} else {
				nodeFrom_TEMP = nodeFrom["id"];
			};
		};

		if (typeof nodeTo === typeof "") {
			nodeTo_TEMP = nodeTo;
		} else {
			if (nodeTo["id"] === undefined) {
				nodeTo_TEMP = nodeTo["to"];
			} else {
				nodeTo_TEMP = nodeTo["id"];
			};
		};
		
		let from_SELECTOR = "[id = " + '"' + nodeFrom_TEMP + '"' + "]";
		
		let from_TEMP = cy_core.nodes(from_SELECTOR);
		
		let to_SELECTOR = "[id = " + '"' +  nodeTo_TEMP + '"' + "]";
		
		let to_TEMP = cy_core.nodes(to_SELECTOR);
		
		let idEdgeTemp;

		if (nodeTo["edge_id"] !== undefined) {
			idEdgeTemp = nodeTo["edge_id"];
		} else {
			idEdgeTemp = from_TEMP.id() + "_" + to_TEMP.id();
			let counter_idEdgeTemp = 0;
			while (cy.getElementById(idEdgeTemp).length > 0) {
				idEdgeTemp = from_TEMP.id() + "_" + to_TEMP.id() + "_" + counter_idEdgeTemp;
				counter_idEdgeTemp = counter_idEdgeTemp+1;
			};
		};

		if ((cy.getElementById(from_TEMP.id()).length !== 0) && (cy.getElementById(to_TEMP.id()).length !== 0)) {

			edgeTEMP = {
				"group": "edges",
				"data": {"id" : idEdgeTemp, "source": from_TEMP.id(), "target": to_TEMP.id()}
			};

			cy_core.add(edgeTEMP);
			
			if (typeof nodeTo !== typeof "") {
				cy_core.getElementById(idEdgeTemp).style(nodeTo["style"]);
				cy_core.getElementById(idEdgeTemp).data(nodeTo["data"]);
				if (cy_core.getElementById(idEdgeTemp).data("indirectEdge") === true) {
					cy_core.getElementById(idEdgeTemp).addClass("indirectEdge");
					cy_core.getElementById(idEdgeTemp).addClass("indirectEdge_hidden");
				};
			};
			
			if ((from_TEMP.data("insensitiveNode") === true) || (to_TEMP.data("insensitiveNode") === true)) {
				cy_core.getElementById(idEdgeTemp).addClass("insensitiveEdge");
			}
			
			if (edgesType === "indirect") {
				cy_core.getElementById(idEdgeTemp).addClass("indirectEdge");
				cy_core.getElementById(idEdgeTemp).addClass("indirectEdge_hidden");
			};

			networkEdges.push(edgeTEMP);

		};
		
	};

	if (typeData === "nodeData") {
		data.forEach(function(node, nodeIndex) {
			if (node[sourceData] !== undefined) {
				node[sourceData].forEach(function (connId, cIndex, conns) {
					if (((connId !== undefined) && (connId !== "")) || ((connId !== undefined) && (connId !== ""))) {
						makeEdgesFromData(node, connId, edgesType);
					};
				});
			};
		});
	};

	if (typeData === "edgeData") {
		data.forEach(function(edge, edgeIndex) {
			if ((edge["from"] !== undefined) && (edge["to"] !== undefined) && (edge["from"] !== "") && (edge["to"] !== "")) {
				makeEdgesFromData(edge, edge, edgesType);
			};
		});
	};

	return networkEdges;
	
};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORF.C : CORE FUNCTIONS FOR CYTOSCAPE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
CORF.C.01 : Creating a specific view
--------------------------------
*/

function seeView(idView="",selectorView="",groupsView="",leveledView=false) {
	
	let chain_SeeView = Promise.resolve();
	
	chain_SeeView = chain_SeeView.then(function(){
		return new Promise((resolve_SeeView) => {
	
			cy.nodes().unselect();
			
			$("#toggle_physics").css("pointer-events","auto");
			
			hierarchicalEnabling = false;
	
			$("#appli_graph_spinner").fadeIn("fast");
			$("#appli_graph_network_CONTAINER").fadeOut( "slow", function() {
				resolve_SeeView('');
			});
			
		});
	});
	
	chain_SeeView = chain_SeeView.then(function(){
		return new Promise((resolve_SeeView) => {
	
			if (Object.keys(listClusters).length > 0) {
				resolve_SeeView(ChainUndoClustering(false));
			} else {
				resolve_SeeView('');
			};
			
		});
	});
	
	chain_SeeView = chain_SeeView.then(function(){
		return new Promise((resolve_SeeView) => {
			
			listClusters = {};
			
			/*

			cy_elementsBackup.forEach(function(elem) {
				if (elem.isNode()) {
					if (cy.getElementById(elem.id()).inside() !== true) {elem.restore()};
				};
				if (elem.isEdge()) {
					if ((cy.getElementById(elem.source().id()).inside() === true) && (cy.getElementById(elem.target().id()).inside() === true)) {elem.restore()};
				};
			});

			*/

			cy_elementsBackup.restore(); /* TO ERASE LATER */
			
			mainLayout.stop();
	
			/*

			if (featuresGraph["static"] === false) {
				if (Object.keys(dataGraph_POSITIONS).length > 0) {
					resolve_SeeView(presetLayout());
				} else {
					if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-cola.min.js")) {
						resolve_SeeView(InitialLayout("euler_cola",false));
					} else {
						if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-fcose.min.js")) {
							resolve_SeeView(fcoseLayout());
						};
					};
				};
			} else {
				resolve_SeeView("");
			};

			*/

			resolve_SeeView(""); /* TO ERASE LATER */
			
		});
	});

	chain_SeeView = chain_SeeView.then(function(){
		return new Promise((resolve_SeeView) => {
			
			if ( (idView !== "") || (groupsView !== "") || (selectorView !== "") ) {
				var filterTEMP = "";
				if (idView !== "") {filterTEMP = filterTEMP + "node" + "[id = "+'"'+idView+'"'+"]"+",";};
				if (selectorView !== "") {filterTEMP = filterTEMP + "node" + selectorView+",";};
				if (groupsView !== "") {
					for (i_group in groupsView.split(",")) {
						filterTEMP = filterTEMP+"node"+"[group = "+'"'+groupsView.split(",")[i_group]+'"'+"]"+",";
					};
				};
				filterTEMP = filterTEMP.slice(0,-1);
				cy_elementsBackup = cy_elementsBackup.union(cy.remove(cy.nodes().difference(cy.filter(filterTEMP+",.infoBubble,.infoTag")).difference(cy.filter(filterTEMP).ancestors())));
			} else {
				if (featuresGraph["initialClustering"].length > 0) {
					GroupsToCluster(featuresGraph["initialClustering"],false);
				};
			};
			
			resolve_SeeView('');
		
		});
	});
	
	chain_SeeView = chain_SeeView.then(function(){
		return new Promise((resolve_SeeView) => {
			
			cy.nodes().unlock();
			
			/*

			if ( (idView !== "") || (groupsView !== "") || (selectorView !== "") ) {
				if (leveledView) {
					if ( (featuresGraph["physics"] === true) && (featuresGraph["static"] === false) ) {
						var clickEvent = new MouseEvent('click');
						document.getElementById("toggle_physics").dispatchEvent(clickEvent);
					};
					$("#toggle_physics").css("pointer-events","none");
					hierarchicalEnabling = true;
					resolve_SeeView(InitialLayout("breadthfirst",false,hierarchicalAngle));
				} else {
					hierarchicalEnabling = false;
					if (featuresGraph["static"] === false) {
						if (Object.keys(dataGraph_POSITIONS).length > 0) {
							resolve_SeeView(presetLayout());
						} else {
							if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-cola.min.js")) {
								resolve_SeeView(InitialLayout("euler_cola",false));
							} else {
								if (modulesList.includes("cytoscape/3.11.0/plugins/cytoscape-fcose.min.js")) {
									resolve_SeeView(fcoseLayout());
								};
							};
						};
					} else {
						resolve_SeeView("");
					};
				};
			} else {
				resolve_SeeView("");
			};

			*/

			resolve_SeeView(""); /* TO ERASE LATER */
		
		});
	});

	chain_SeeView = chain_SeeView.then(function(){
		return new Promise((resolve_SeeView) => {
				
			if (featuresGraph["static"] === true) {
				cy.$(".defaultNode").lock();
			};
			
			var modalContent_Summary = document.getElementById("summaryModal_body");
			if ( (idView !== "") || (groupsView !== "") || (selectorView !== "") ) {
				modalContent_Summary.innerHTML = readNodes_for_modal(cy);
			} else {
				modalContent_Summary.innerHTML = readNodes_for_modal(cy_elementsBackup);
			};
			
			/* if (featuresGraph["featureSearchBar"] === true) {creatingSearchBar()}; */
			
			setTimeout(function() {
				$("#appli_graph_spinner").fadeOut("fast");
				$("#appli_graph_network_CONTAINER").fadeIn("slow", function () {
					resolve_SeeView(ForceStabilization());
				});
			}, 500);
			
		});
	});

};

/*
--------------------------------
CORF.C.02 : Force the network to stabilize
--------------------------------
*/

if (alreadyLoaded === false) {
	document.getElementById("toggle_stabilization").addEventListener("click",ForceStabilization);
};

function ForceStabilization(elements=undefined) {
	return new Promise((resolve) => {
		$("#appli_graph_network_CONTAINER").css("pointer-events","none");
		cy.stop();
		var paddingTEMP = featuresGraph["padding"];
		if (elements === undefined) {
			if (cy.$("node:selected").length > 0) {
				var selectorTEMP = cy.$("node:selected:visible");
			} else {
				var selectorTEMP = cy.nodes(":visible");
			};
		} else {
			var selectorTEMP = elements;
		};
		if (selectorTEMP.length === 1) {paddingTEMP = 3*paddingTEMP;};
		if (typeof(mapActivated) === "undefined") {
			var stabilizationMode = true;
		} else {
			if (mapActivated === true) {
				var stabilizationMode = false;
			} else {
				var stabilizationMode = true;
			};
		};
		if (stabilizationMode === true) {
			cy.animate({ "duration": 500, "fit": {"eles": selectorTEMP, "padding": paddingTEMP}, "easingFunction": "ease", "complete": function(){$("#appli_graph_network_CONTAINER").css("pointer-events","initial");resolve("Done");}});
		} else {
			cyMap.fit(undefined, {"padding":150});
			$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
			resolve("Done");
		};
	});
};

/*
--------------------------------
CORF.C.03 : Show lateral screen
--------------------------------
*/

var ongoingProcessingLateralScreen = false;

function lateralScreen(mode="default") {

	return new Promise((resolve_function) => {

		if (mode === "Done" || mode === "closing") {
			var modeAdjust = "default";
		} else {
			modeAdjust = mode;
		};

		ongoingProcessingLateralScreen = true;
	
		var promise_lateralScreen = new Promise(function(resolve, reject) {
		  cy.nodes().unselect();
		  $("#appli_graph_network").fadeOut("fast");
		  $("#appli_graph_element").fadeOut("fast");
		  $("#appli_graph_buttons").fadeOut("fast");
		  var switchPath = "none";
		  var elem = document.getElementById("appli_graph_main_screen");
		  if ($(document).width() > $(document).height()) {
		  	var currentOrientation = "landscape";
		  } else {
		  	var currentOrientation = "portrait";
		  };
		  if (modeAdjust === "default") {
			  if (currentOrientation === "landscape") {
				  if (elem.style.width === "60%") {
					  switchPath = "closing";
					  featuresGraph["tooltips"] = tooltipsGraph_BCK;
					  var width = 60;
					  var id = setInterval(frame, 0.1);
					  function frame() {
						if (width == 100) {
						  resolve(switchPath);
						  clearInterval(id);
						} else {
						  width++;
						  elem.style.width = width + "%";
						};
					  };
				  } else {
					  switchPath = "opening";
					  featuresGraph["tooltips"] = false;
					  var width = 100;
					  var id = setInterval(frame, 0.1);
					  function frame() {
						if (width == 60) {
						  resolve(switchPath);
						  clearInterval(id);
						} else {
						  width--;
						  elem.style.width = width + "%";
						};
					  };
				  };
			  } else {
				  if (elem.style.height === "60%") {
				  	  switchPath = "closing";
					  featuresGraph["tooltips"] = tooltipsGraph_BCK;
					  var height = 60;
					  var id = setInterval(frame, 0.1);
					  function frame() {
						if (height == 100) {
						  resolve(switchPath);
						  clearInterval(id);
						} else {
						  height++;
						  elem.style.height = height + "%";
						};
					  };
				  } else {
				  	  switchPath = "opening";
					  featuresGraph["tooltips"] = false;
					  var height = 100;
					  var id = setInterval(frame, 0.1);
					  function frame() {
						if (height == 60) {
						  resolve(switchPath);
						  clearInterval(id);
						} else {
						  height--;
						  elem.style.height = height + "%";
						};
					  };
				  };
			  };
		 };
		 if (modeAdjust === "closing") {
			 switchPath = "closing";
			  if (currentOrientation === "landscape") {
				  featuresGraph["tooltips"] = tooltipsGraph_BCK;
				  elem.style.width = "100%";
				  resolve(switchPath);
			  } else {
				  featuresGraph["tooltips"] = tooltipsGraph_BCK;
				  elem.style.height = "100%";
				  resolve(switchPath);
			  };
		 };
		});
		  
		promise_lateralScreen.then(function(value) {
			isLateral = value;
			var promise_lateralScreen_2 = new Promise(function(resolve_2, reject_2) {
				if (value === "closing") {
					$("#appli_graph_buttons .btn").css("width", "20%");
					$("#graph_buttons_accordion").css("width", "20%");
					$("#appli_graph_buttons .btn span").css("display", "initial");
					$("#view_ALL img").css("display", "none");
					$("#group_ALL img").css("display", "none");
					$("#ungroup_ALL img").css("display", "none");
					$("#headingFilter button img").css("display", "none");
					$("#headingLegend button img").css("display", "none");
				};
				if (value === "opening") {
					$("#appli_graph_buttons .btn").css("width", "auto");
					$("#graph_buttons_accordion").css("width", "auto");
					$("#view_ALL img").css("display", "initial");
					$("#group_ALL img").css("display", "initial");
					$("#ungroup_ALL img").css("display", "initial");
					$("#headingFilter button img").css("display", "initial");
					$("#headingLegend button img").css("display", "initial");
					$("#appli_graph_buttons .btn span").css("display", "none");
				};
				if (hierarchicalAngle === 0) {
					hierarchicalAngle = -90;
				} else {
					hierarchicalAngle = 0;
				};
				if (hierarchicalEnabling) {
					InitialLayout("breadthfirst",false,hierarchicalAngle);
				};
				$("#appli_graph_buttons").fadeIn("slow");
				$("#appli_graph_element").fadeIn("slow");
				$("#appli_graph_network").fadeIn("slow", function (){
					cy.resize();
					resolve_2('');
				});
			});
			promise_lateralScreen_2.then(function(value_2) {
				setTimeout(function() {
					resolve_function(ForceStabilization());
				},50);
			});
		});

	});

};

/*
--------------------------------
CORF.C.04 : Generate the body of the content modal string
--------------------------------
*/

function readNodes_for_modal(cy_function) {
	
	var outputText = "";
	
	var sortingNodes = cy_function.nodes().sort(function(ele1,ele2) {
		return ele1.data("index") > ele2.data("index");
	});
	
	sortingNodes.forEach(function(ele){

		if (ele.data("rank") === undefined) {ele.data("rank",1)};
		
		var nodeTEMP = ele.data();
		
		if (ele.isChild()) {
			var rankTEMP = nodeTEMP.rank+1;
		} else {
			var rankTEMP = nodeTEMP.rank;
		};
		
		if (rankTEMP > 4) {outputText = outputText.concat("<ul><li>");} else {outputText = outputText.concat("<h",rankTEMP,">");};
		
		if (nodeTEMP.image !== undefined) {outputText = outputText.concat("<img class=\"lazyload\" data-src=\"",nodeTEMP.image,"\"></img>");};
		
		if (nodeTEMP.label !== undefined) {outputText = outputText.concat(nodeTEMP.label);} else {outputText = outputText.concat("");};
		
		if (rankTEMP > 4) {outputText = outputText.concat("</li></ul>");} else {outputText = outputText.concat("</h",rankTEMP,">");};
		
		outputText = outputText.concat("<br>");
		
		if (nodeTEMP.titleComplete !== undefined) {outputText = outputText.concat("<p>",nodeTEMP.titleComplete,"</p>","<br>");};

	},{"order": "id"});
	
	return outputText;
	
};

/*
--------------------------------
CORF.C.05 : Initial Layout
--------------------------------
*/

function InitialLayout(mode,fit,angle=0) {
	
	return new Promise((resolve_done) => {

		if (typeof(mapActivated) === "undefined") {
			var stabilizationMode = true;
		} else {
			if (mapActivated === true) {
				var stabilizationMode = false;
			} else {
				var stabilizationMode = true;
			};
		};
		if (stabilizationMode === true) {
	
			var cy_noBubbles = cy.elements().difference(cy.$(".infoBubble")).difference(cy.$(".infoTag"));
			
			cy_noBubbles.nodes().unlock();
			
			mainLayout.stop();
			
			if (mode === "breadthfirst") {
				var bb = cy_noBubbles.nodes().boundingBox();
				mainLayout = cy_noBubbles.layout({
					"name": "breadthfirst",
					"spacingFactor": 1,
					"roots": cy_noBubbles.nodes().roots(),
					"transform": function (node,position) {
						var x = position["x"];
						var y = position["y"];
						var c_x = bb["x1"]+((bb["x2"]-bb["x1"])/2);
						var c_y = bb["y1"]+((bb["y2"]-bb["y1"])/2);
						var radians = (Math.PI / 180) * angle;
						var cos = Math.cos(radians);
						var sin = Math.sin(radians);
						var nx = (cos * (x - c_x)) + (sin * (y - c_y)) + c_x;
						var ny = (cos * (y - c_y)) - (sin * (x - c_x)) + c_y;
						return {"x": nx, "y": ny};					
					},
					"stop": function() {
						if (fit === true) {ForceStabilization()};
						cy_noBubbles.nodes().lock();
						resolve_done("Done");
					}
				});
				mainLayout.run();
			};
			
			if ( (mode === "euler_cola") || (mode === "cola") ) {
				var promise_LayoutEuler = new Promise(function(resolve, reject) {
					if (featuresEulerComputation === "true") {
						if (mode === "euler_cola") {
							mainLayout = cy_noBubbles.layout({
								"name": "euler",
								"randomize": true,
								"animate": false,
								"stop": function(){resolve('');}
							});
							mainLayout.run();
						} else {
							resolve('');
						};
					} else {
						resolve('');
					};
				});
				promise_LayoutEuler.then(function(value) {
					var promise_LayoutCola = new Promise(function(resolve, reject) {
						if (mode === "euler_cola") {
							mainLayout = cy_noBubbles.layout({
								"name": "cola",
								"randomize": false,
								"fit": false,
								"animate": true,
								"avoidOverlap": true,
								"nodeSpacing": function(node) { return 10; },
								"edgeLength": function(edge) {
									var coeff = featuresGraph["compaction"];
									var lengthTEMP = (coeff*350)-(coeff*75*edge.source().data("rank"));
									if (lengthTEMP<coeff*50) {lengthTEMP=coeff*50};
									return lengthTEMP;
								},
								"nodeDimensionsIncludeLabels": true,
								"stop": function(){resolve('');},
								"infinite": false
							});
							mainLayout.run();
						} else {
							resolve('');
						};
					});
					promise_LayoutCola.then(function(value) {
						mainLayout = cy_noBubbles.layout({
							"name": "cola",
							"randomize": false,
							"fit": false,
							"animate": true,
							"avoidOverlap": true,
							"nodeSpacing": function(node) { return 10; },
							"edgeLength": function(edge) {
								var coeff = featuresGraph["compaction"];
								var lengthTEMP = (coeff*350)-(coeff*75*edge.source().data("rank"));
								if (lengthTEMP<coeff*50) {lengthTEMP=coeff*50};
								return lengthTEMP;
							},
							"nodeDimensionsIncludeLabels": true,
							"ready": function(){
								if (fit === true) {ForceStabilization()};
								resolve_done("Done");
								},
							"infinite": featuresGraph["physics"]
						});
						mainLayout.run();
					});
				});
			};

		} else {
			cyMap.fit(undefined, {"padding":150});
			$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
			resolve_done("Done");
		};
		
	});

};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORF.D : FUNCTIONS LINKED TO THE BUTTONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
CORF.D.01 : Function to show the bug modal screen
-------------------------------- 
*/

function displayBug_iframe(){
	$("#form_bug_report_IFRAME").css({"display": "initial"});
};

/*
--------------------------------
CORF.D.02 : Fullscreen function
--------------------------------
*/

$('#toggle_fullscreen').click(function(){
  /* if already full screen; exit */
  /* else go fullscreen */
  if (
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement
  ) {
	if (document.exitFullscreen) {
	  document.exitFullscreen();
	} else if (document.mozCancelFullScreen) {
	  document.mozCancelFullScreen();
	} else if (document.webkitExitFullscreen) {
	  document.webkitExitFullscreen();
	} else if (document.msExitFullscreen) {
	  document.msExitFullscreen();
	}
  } else {
	element = $('#page-top').get(0);
	if (element.requestFullscreen) {
	  element.requestFullscreen();
	} else if (element.mozRequestFullScreen) {
	  element.mozRequestFullScreen();
	} else if (element.webkitRequestFullscreen) {
	  element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
	} else if (element.msRequestFullscreen) {
	  element.msRequestFullscreen();
	}
  }
});


/*
--------------------------------
CORF.D.03 : Save modal as doc
--------------------------------
*/

$('#toggle_save').click(function (){
   var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
		"xmlns:w='urn:schemas-microsoft-com:office:word' "+
		"xmlns='http://www.w3.org/TR/REC-html40'>"+
		"<head><meta charset='utf-8'><title>EcoRhizo : export de graphe interactif</title></head><body>";
   var footer = "</body></html>";
   var sourceHTML = header+document.getElementById("summaryModal_body").innerHTML+footer;
   
   var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
   var fileDownload = document.createElement("a");
   document.body.appendChild(fileDownload);
   fileDownload.href = source;
   fileDownload.download = 'graphe.doc';
   fileDownload.click();
   document.body.removeChild(fileDownload);
});

/*
--------------------------------
CORF.D.04 : For the viewer button
--------------------------------
*/

$('#toggle_lateralScreen').click(function() {
	if (ongoingProcessingLateralScreen === false) {
		ongoingProcessingLateralScreen = true;
		if (editorModeActivated === false) {
			chain_MainLateralScreen = chain_MainLateralScreen.then(lateralScreen.bind("default"));
			chain_MainLateralScreen = chain_MainLateralScreen.then(function(){
				return new Promise((resolve_MainLateralScreen) => {
					ongoingProcessingLateralScreen = false;
					resolve_MainLateralScreen("default");
				});
			});
		} else {
			chain_MainLateralScreen = chain_MainLateralScreen.then(function(){
				return new Promise((resolve_MainLateralScreen) => {
					resolve_MainLateralScreen("closing");
				});
			});
			chain_MainLateralScreen = chain_MainLateralScreen.then(editorModeActivation.bind("closing"));
		};
	};
});

/*
--------------------------------
CORF.D.05 : For the graph button
--------------------------------
*/

$('#toggle_graphs').click(function() {
	cy.destroy();
	lateralScreen("closing");
	$("#appli_graph_network").css("visibility","hidden");
	$("#appli_graph_element").css("display","none");
	$("#appli_graph_cc_element").css("display","none");
	$("#appli_graph_buttons").css("display","none");
	$("#appli_graph_SEARCHING").css("display","none");
	$("#_talking_button_fullscreen").css("display","none");
	$("#_talking_button_pass").css("display","none");
	$("#_talking_button_suspend").css("display","none");
	$("#_talking_button_launch").css("display","none");
});

/*
--------------------------------
CORF.D.06 : To deal with device orientation
--------------------------------
*/

window.addEventListener("orientationchange", function() {
	windowCustomResize();
});

/*
--------------------------------
CORF.D.07 : Running fcose layout
--------------------------------
*/

function fcoseLayout(withFit=true){
	
	return new Promise((resolve) => {
		
		$("#appli_graph_network_CONTAINER").css("pointer-events","none");

		if (typeof(mapActivated) === "undefined") {
			var stabilizationMode = true;
		} else {
			if (mapActivated === true) {
				var stabilizationMode = false;
			} else {
				var stabilizationMode = true;
			};
		};
		if (stabilizationMode === true) {

			var cy_noBubbles = cy.elements().difference(cy.$(".infoBubble")).difference(cy.$(".infoTag"));
			
			cy.$(".compoundCorner").remove();

			cy_noBubbles.nodes().unlock();

			mainLayout.stop();
			
			var paddingTEMP = featuresGraph["padding"];

			mainLayout = cy_noBubbles.layout({
				"name": "fcose",
				"quality": "default",
				"padding": paddingTEMP,
				"randomize": false,
				"animate": true,
				"fit": withFit,
				"animationDuration": 1000,
				"nodeDimensionsIncludeLabels": true,
				"nodeSeparation": 250,
				"nodeRepulsion": node => 500,
				"idealEdgeLength": function(edge) {if (edge.data("edgeLength") !== undefined) {return edge.data("edgeLength")} else {return 50};},
				"stop": () => {
					$("#appli_graph_network_CONTAINER").css("pointer-events","auto");
					resolve("Done");
				}
			});

			mainLayout.run();

		} else {
			cyMap.fit(undefined, {"padding":150});
			$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
			resolve("Done");
		};
	
	});
	
};

$('#toggle_fcose').click(fcoseLayout);

/*
--------------------------------
CORF.D.08 : Go to initial positions
--------------------------------
*/

function presetLayout(){
	
	return new Promise((resolve) => {

		$("#appli_graph_network_CONTAINER").css("pointer-events","none");

		if (typeof(mapActivated) === "undefined") {
			var stabilizationMode = true;
		} else {
			if (mapActivated === true) {
				var stabilizationMode = false;
			} else {
				var stabilizationMode = true;
			};
		};
		if (stabilizationMode === true) {

			var cy_noBubbles = cy.elements().difference(cy.$(".infoBubble")).difference(cy.$(".infoTag"));
			
			cy.$(".compoundCorner").remove();

			cy_noBubbles.nodes().unlock();

			mainLayout.stop();
			
			var paddingTEMP = featuresGraph["padding"];

			mainLayout = cy_noBubbles.layout({
				"name": "preset",
				"animate": true,
				"fit": true,
				"padding": paddingTEMP,
				"animationDuration": 1000,
				"positions": function(node){
					return node.data("position");
				},
				"stop": () => {
					$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
					resolve("Done");
				}
			});

			mainLayout.run();

		} else {
			cyMap.fit(undefined, {"padding":150});
			$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
			resolve("Done");
		};
	
	});
	
};

$('#toggle_ori_pos').click(presetLayout);
