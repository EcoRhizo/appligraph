<!DOCTYPE html>
<html lang="fr">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="___REWRITE_META_DESCRIPTION___">
	<meta name="author" content="___REWRITE_META_AUTHOR___">
	<meta name="git_repository" content="___REWRITE_META_GIT_REPOSITORY___">
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="pragma" content="no-cache" />

	<link id="page_icon" rel="icon" href="___REWRITE_META_PAGE_ICON___" />

	<title>___REWRITE_META_TITLE___</title>
	
	<?php

		$output = "";
		
		$g = (isset($_GET["directId"])) ? $_GET["directId"] : "";
		
		if ($g !== "") {
		
			/* Retriving graph path */
			$g = explode("/",$g);
			$g = $g[count($g)-1];
			$g = explode("\\",$g);
			$g = $g[count($g)-1];
			$g_path = "";
			$it = new RecursiveTreeIterator(new RecursiveDirectoryIterator("_TEMPLATES", RecursiveDirectoryIterator::SKIP_DOTS));
			foreach($it as $path) {
				if (preg_match('/'.$g.'$'.'/',$path)) {
					$g_path = substr($path,strpos($path,'_TEMPLATES')+6);
				};
			};
			if ($g_path == "") {
				$idGraph = $g;
			} else {
				$idGraph = $g_path;
			};
			
		};
		
		if (($g !== "") && (file_exists("../grezi_old/_TEMPLATES/" . $idGraph . "/title.js"))) {
			
			/* Retriving title.js */
			if ($file = fopen("../grezi_old/_TEMPLATES/" . $idGraph . "/title.js", "r")) {
				while(!feof($file)) {
					$textperline = fgets($file);
					if (strrpos($textperline,"subtitle_GRAPH") > 0) {
						$subtitle_GRAPH = $textperline;
						$subtitle_GRAPH = str_replace('var subtitle_GRAPH = "','',$subtitle_GRAPH);
						$subtitle_GRAPH = str_replace('";','',$subtitle_GRAPH);
						$subtitle_GRAPH = str_replace('\\"','',$subtitle_GRAPH);
					} else {
						if (strrpos($textperline,"title_GRAPH") > 0) {
							$title_GRAPH = $textperline;
							$title_GRAPH = str_replace('var title_GRAPH = "','',$title_GRAPH);
							$title_GRAPH = str_replace('";','',$title_GRAPH);
							$title_GRAPH = str_replace('\\"','',$title_GRAPH);
						};
					};
				};
				fclose($file);
			};

			/* Canonical URL */
			$output = $output . "<link rel=\"canonical\" href=\"___FULL_PATH___/appli_graph/index.php?directId=" . $g . "\" />";
			
			/* Open Graph Protocol */
			$output = $output . "<meta property=\"og:locale\" content=\"fr_FR\" />";
			$output = $output . "<meta property=\"og:type\" content=\"article\" />";
			$output = $output . "<meta property=\"og:title\" content=\"" . $title_GRAPH . "\" />";
			$output = $output . "<meta property=\"og:description\" content=\"" . $subtitle_GRAPH . "\" />";
			$output = $output . "<meta property=\"og:url\" content=\"___FULL_PATH___/appli_graph/index.php\" />";
			$output = $output . "<meta property=\"og:site_name\" content=\"GREZI\" />";
			$output = $output . "<meta property=\"og:image\" content=\"___FULL_PATH___/appli_graph/_TEMPLATES/" . $idGraph . "/img/card.png\" />";

			/* Twitter Card */
			$output = $output . "<meta name=\"twitter:card\" content=\"summary\" />";
			$output = $output . "<meta name=\"twitter:site\" content=\"@GREZI\" />";
			$output = $output . "<meta name=\"twitter:title\" content=\"" . $title_GRAPH . "\" />";
			$output = $output . "<meta name=\"twitter:description\" content=\"" . $subtitle_GRAPH . "\" />";
			$output = $output . "<meta name=\"twitter:image\" content=\"___FULL_PATH___/appli_graph/_TEMPLATES/" . $idGraph . "/img/card.png\" />";
		
		} else {

			/* Canonical URL */
			$output = $output . "<link rel=\"canonical\" href=\"___FULL_PATH___/appli_graph/index.php\" />";
		
			/* Open Graph Protocol */
			$output = $output . "<meta property=\"og:locale\" content=\"fr_FR\" />";
			$output = $output . "<meta property=\"og:type\" content=\"article\" />";
			$output = $output . "<meta property=\"og:title\" content=\"___REWRITE_OG_TITLE___\" />";
			$output = $output . "<meta property=\"og:description\" content=\"___REWRITE_OG_DESCRIPTION___\" />";
			$output = $output . "<meta property=\"og:url\" content=\"___FULL_PATH___/appli_graph/index.php\" />";
			$output = $output . "<meta property=\"og:site_name\" content=\"___REWRITE_OG_SITE_NAME___\" />";
			$output = $output . "<meta property=\"og:image\" content=\"___FULL_PATH___/appli_graph/___REWRITE_OG_IMAGE___\" />";

			/* Twitter Card */
			$output = $output . "<meta name=\"twitter:card\" content=\"summary\" />";
			$output = $output . "<meta name=\"twitter:site\" content=\"___REWRITE_TWITTER_SITE___\" />";
			$output = $output . "<meta name=\"twitter:title\" content=\"___REWRITE_TWITTER_TITLE___\" />";
			$output = $output . "<meta name=\"twitter:description\" content=\"___REWRITE_TWITTER_DESCRIPTION___\" />";
			$output = $output . "<meta name=\"twitter:image\" content=\"___FULL_PATH___/appli_graph/___REWRITE_TWITTER_IMAGE___\" />";

		};
		
		echo $output;
		
	?>
	
	<!-- Bootstrap core CSS -->
	<link href="dist/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Bootstrap plugin CSS -->
	<link href="dist/bootstrap-select/1.13.9/css/bootstrap-select.css" rel="stylesheet">

	<!-- Custom styles of GREZI -->
	<link href="css/_custom.css?___VERSION_CACHE___" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/_custom_graph.css?___VERSION_CACHE___" rel="stylesheet">
	
	<!-- Custom styles for this template -->
	
	<!-- On calling the URL params : 
	
		- Can NOT be force in config config.js file :
	
			- debug (true / folder_name) : Show a list of all accessible graphs (globally (true) or in a specific folder (folder_name)) and don't check data version
			
			- ids (strings separate by a ',') : Graphs to show in choice modal screen (which are in the ../_TEMPLATES/_appli_graph folder
			
			- directId (string) : Graph to show on load without showing the choice modal screen
			
			- passwd (string) : Submit a password in case of ___PASSWORD_TEMPLATES___ data option
			
		- Can be force in config config.js file :

			- eulerComputation (true / false) : The first physic computation of the graph use euler plugin or not (not recommended on mobile devices)
			
			- showInformation (true / false) : Open the information modal screen when a graph is loaded
			
			- clickToBegin (true / false) : Place or not a mask of "click to begin" action
			
			- blank (true) : Use the app in a blank mark mode
			
			- toggle_fullscreen (true / false) : Activate or not this feature button,
			
			- toggle_graphs (true / false) : Activate or not this feature button,
			
			- toggle_information (true / false) : Activate or not this feature button,

			- toggle_map (true / false) : Activate or not this feature button,
			
			- toggle_legend (true / false) : Activate or not this feature button,
			
			- toggle_stabilization (true / false) : Activate or not this feature button,
			
			- toggle_physics (true / false) : Activate or not this feature button,
			
			- toggle_fcose (true / false) : Activate or not this feature button,
			
			- toggle_ori_pos (true / false) : Activate or not this feature button,
			
			- toggle_ind_link (true / false) : Activate or not this feature button,
			
			- toggle_highlighter (true / false) : Activate or not this feature button,
			
			- toggle_lateralScreen (true / false) : Activate or not this feature button,
			
			- toggle_summary (true / false) : Activate or not this feature button,
			
			- toggle_save (true / false) : Activate or not this feature button,
			
			- toggle_bug (true / false) : Activate or not this feature button,
	
	-->
	

</head>

<body id="page-top" style="display:none;overflow:hidden;" onload="revealBody()">

	<div id="appli_graph">
	
		<!-- Talking screen (introduction) -->
		<div id="_talking_introduction" style="display:none;z-index:99999;"></div>
		
		<!-- Talking screen (main) -->
		<div id="_talking_main_screen">
			<div id="_talking_frame" class="row" style="display:none;">
				<div id="_talking_image" class="col-0"></div>
				<div id="_talking_body" class="col-12">
					<div id="_talking_title"></div>
					<span id="_talking_typed"></span>
					<span id="_talking_next_word">&nbsp;Cliquez pour continuer</span>
				</div>
			</div>
		</div>
  
		<!-- Lateral screen -->
		<div id="appli_graph_lateral_screen" class="col-xl-12 col-lg-12">
			<div id="appli_graph_lateral_body">
				<div id="tooltipLateral_body_CONTAINER">
					<div id="tooltipLateral_body"></div>
				</div>
			</div>
		</div>

		<!-- Main screen -->
		<div id="appli_graph_main_screen" class="col-xl-12 col-lg-12">
		
			<!-- Graph screen -->
			<div id="appli_graph_network_CONTAINER"><div id="appli_graph_network"></div></div>
			
			<!-- Animation Buttons -->
			<div id="_talking_button" style="display:none;">
				<button id="_talking_button_fullscreen" class="btn btn-primary mt-2"><img class="lazyload" width="25" height="25" data-src="icon/arrows-alt.svg"></button>
				<button id="_talking_button_pass" class="btn btn-primary mt-2">Accès direct</button>
				<button id="_talking_button_suspend" class="btn btn-primary mt-2">Suspendre</button>
				<button id="_talking_button_launch" class="btn btn-primary mt-2">Lancer l'animation</button>
			</div>
			
			<!-- View Buttons -->
			<div id="appli_graph_buttons" class="mt-2 pr-2">
				<button href="#" id="view_ALL" type="button" class="btn btn-primary mb-1 mainViewButtons" style="text-align:center;">
					<span><b>REINITIALISER</b></span>
					<img class="lazyload" width="25" height="25" data-src="icon/redo-alt.svg"></img>
				</button>
				<br id="view_ALL_br">
				<button href="#" id="group_ALL" type="button" class="btn btn-primary mb-1 mainViewButtons" style="text-align:center;">
					<span><b>TOUT GROUPER</b></span>
					<img class="lazyload" width="25" height="25" data-src="icon/object-group.svg"></img>
				</button>
				<br id="group_ALL_br">
				<button href="#" id="ungroup_ALL" type="button" class="btn btn-primary mb-1 mainViewButtons" style="text-align:center;">
					<span><b>TOUT OUVRIR</b></span>
					<img class="lazyload" width="25" height="25" data-src="icon/object-ungroup.svg"></img>
				</button>
				<br id="ungroup_ALL_br">
				<div id="appli_graph_buttons_CONTENT"></div>
				<div id="appli_graph_buttons_LEGEND"></div>
			</div>
			
			<!-- Functions Buttons -->
			<div id="appli_graph_element" class="mt-2 pl-2">
				<button href="#" id="toggle_fullscreen" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></a></button>
				<br id="toggle_fullscreen_BR">
				<hr style="margin-top:0rem;margin-bottom:.25rem;margin-top:.25rem;padding-bottom:.25rem;">
				<button href="#" id="toggle_graphs" type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#graphChoiceModal"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_graphs_BR">
				<button href="#" id="toggle_lateralScreen" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_lateralScreen_BR">
				<button href="#" id="toggle_map" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_map_BR">
				<hr style="margin-top:0rem;margin-bottom:.25rem;margin-top:.25rem;padding-bottom:.25rem;">
				<button href="#" id="toggle_legend" type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#legendModal"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_legend_BR">
				<button href="#" id="toggle_tags" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_tags_BR">
				<button href="#" id="toggle_stabilization" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_stabilization_BR">
				<button href="#" id="toggle_physics" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_physics_BR">
				<button href="#" id="toggle_fcose" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_fcose_BR">
				<button href="#" id="toggle_ori_pos" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_ori_pos_BR">
				<button href="#" id="toggle_ind_link" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_ind_link_BR">
				<button href="#" id="toggle_highlighter" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_highlighter_BR">
				<button href="#" id="toggle_modeEditor" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_modeEditor_BR">
				<button href="#" id="toggle_summary" type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#summaryModal"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_summary_BR">
				<button href="#" id="toggle_save" type="button" class="btn btn-primary mb-1"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_save_BR">
				<hr style="margin-top:0rem;margin-bottom:.25rem;margin-top:.25rem;padding-bottom:.25rem;">
				<button href="#" id="toggle_information" type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#informationModal"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_information_BR">
				<button href="#" id="toggle_bug" type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#bugModal" onclick="$('#bug_report_appli').html('');"><a class="tooltip-text"><img class="lazyload" width="25" height="25"></img></a></button>
				<br id="toggle_bug_BR">
			</div>
			
			<!-- Search bar -->
			<div id="appli_graph_SEARCHING">
					<div id="appli_graph_BUTTONS_BOX">
						<button href="#" id="appli_graph_SEARCH_BUTTON" type="button" class="btn btn-primary mb-1"><img class="lazyload" width="25" height="25" data-src="icon/search.svg"></img></button>
						<button href="#" id="appli_graph_MODE_BUTTON_zoom" type="button" class="btn btn-primary mb-1"><img class="lazyload" width="25" height="25" data-src="icon/search-plus-solid.svg"></img><span>Zoomer</span></button>
						<button href="#" id="appli_graph_MODE_BUTTON_selected" type="button" class="btn btn-primary mb-1"><img class="lazyload" width="25" height="25" data-src="icon/hand-pointer-solid.svg"></img><span>Sélection</span></button>
						<button href="#" id="appli_graph_MODE_BUTTON_and" type="button" class="btn btn-primary mb-1">ET</button>
						<button href="#" id="appli_graph_MODE_BUTTON_or" type="button" class="btn btn-primary mb-1">OU</button>
					</div>
					<div id="appli_graph_SEARCH_BAR"></div>
			</div>
			
			<!-- Creative Commons -->
			<div id="appli_graph_cc" style="display:none;">
				<div  id="appli_graph_cc_element" class="mb-2 pl-2">
					<a href="https://grezi.fr" target="_blank"><img class="licences_img lazyload" data-src="img/grezi.webp"></img></a>
					<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank"><img class="licences_img lazyload" alt="Licence Creative Commons BY SA 4.0" data-src="img/creative_commons_logo.webp" /></a>
					<a href="https://lsc.encommuns.org/" target="_blank"><img class="licences_img lazyload" alt="Legal Service For Commons, soutenir juridiquement les communs" data-src="img/logo_lsc.webp" /></a>
					<a rel="license" href="http://www.gnu.org/licenses/agpl-3.0.fr.html" target="_blank"><img class="licences_img lazyload" alt="Licence GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007" data-src="img/agpl_v3_logo.webp" /></a>
				</div>
			</div>
			
			<!-- Spinner -->
			<div id="appli_graph_spinner">
				<div class="d-flex align-items-center justify-content-center" style="flex-wrap:wrap;padding:5%;position:relative;height:100%;width:100%">
					<div class="spinner-border" role="status">
						<span class="sr-only">Loading...</span>
					</div>
					<div style="flex-basis:100%;"></div>
					<div id="appli_graph_progessbar"></div>
				</div>
			</div>
			
		</div>
		
		<!-- Modal screen GRAPH CHOICE -->
		<div class="modal fade" id="graphChoiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<a class="logo_title" href="https://grezi.fr" target="_blank"><img class="lazyload" height="40px" data-src="img/grezi.webp"></img></a>
					<h4 class="modal-title" id="myModalLabel">Accueil</h4>
				</div>
				<div class="modal-body">
					<h1>Une nouvelle version de GREZI est en cours de préparation !</h1>
					<p>Elle sera disponible sur ce <a href="https://gitlab.com/grezi" target="_blank">dépôt git</a> avec plusieurs gabarits libres d'utilisation. Retrouvez également le commun GREZI sur le <a href="https://wiki.resilience-territoire.ademe.fr/wiki/GREZI" target="_blank">wiki de l'AAC de l'ADEME</a>.</p>
					<div class="modal-hr"><hr class="my-6"></div>
					<!-- <div id="appli_graph_TemplateGraphs"></div> -->
					<p>Bienvenue sur le commun numérique <b>GREZI</b>. Libre et open-source, il s'agit d'un <b>outil de graphes narratifs et interactifs dédiés au développement des résiliences territoriales</b>. Actuellement l'application est en version <b>beta</b>. Vos retours par <a href="mailto:contact@grezi.fr">mail</a> ou sur le <a href="https://gitlab.com/EcoRhizo/appligraph" target="_blank">GitLab</a> (où se trouve le code source) seront particulièrement appréciés.</p>
					<p>Cette application s'utilise à travers un gabarit, c'est-à-dire une version personnalisée de <b>GREZI</b> avec un échantillon des fonctionnalités disponibles.</p>
					<p>Pour en savoir plus sur ce bien commun, consultez le <a href="https://grezi.fr" target="_blank">site internet</a>.</p>
					___CUSTOM_TEXT_graph_choice_1___
					<div class="mt-4 mb-4 pl-2"s style="text-align:right;">
						<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank"><img class="licences_img lazyload" alt="Licence Creative Commons BY SA 4.0" data-src="img/creative_commons_logo.webp" /></a>
						<a href="https://lsc.encommuns.org/" target="_blank"><img class="licences_img lazyload" alt="Legal Service For Commons, soutenir juridiquement les communs" data-src="img/logo_lsc.webp" /></a>
						<a rel="license" href="http://www.gnu.org/licenses/agpl-3.0.fr.html" target="_blank"><img class="licences_img lazyload" alt="Licence GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007" data-src="img/agpl_v3_logo.webp" /></a>
					</div>
					<div class="modal-hr"><hr class="my-6"></div>
					___CUSTOM_TEXT_graph_choice_2___
					<div class="mt-4 mb-5 ">
						<p>Qu'est-ce qu'un gabarit ?</p>
						<div id="div_template_image">
						<img id="template_image" class="lazyload" data-src="img/template_explanation.svg" alt="© GREZI template explanation">
						</div>
					</div>
					<div class="modal-hr"><hr class="my-6"></div>
					<p>Pour charger directement un gabarit, sélectionnez-le dans la liste ci-dessous :</p>
					<div id="appli_graph_IdsGraphs"></div>
				</div>
			</div>
			</div>	
		</div>
	  
		<!-- Modal screen INFORMATION -->
		<div class="modal fade" id="informationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<a class="logo_title" href="https://grezi.fr" target="_blank"><img class="lazyload" height="40px" data-src="img/grezi.webp"></img></a>
					<h4 class="modal-title" id="myModalLabel">Informations sur l'application</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div id="appli_graph_description"></div>
					<div id="appli_graph_credentials" style="display:none;">
						<div class="modal-hr"><hr class="my-6"></div>
						<p>Quelques principes d'utilisation :</p>
						<ul>
							<li style="margin:.5em 10% .5em 0" id="info_tooltip">Survolez certains éléments pour visualiser des informations additionnelles,</li>
							<li style="margin:.5em 10% .5em 0" id="info_tooltipModal">Cliquez sur certains cercles pour visualiser des informations additionnelles,</li>
							<li style="margin:.5em 10% .5em 0" id="info_seeView">Cliquez sur les boutons en haut à droite pour visualiser seulement certaines parties du graphe,</li>
							<li style="margin:.5em 10% .5em 0" id="info_searchBar">Utiliser la barre de recherche en bas à droite pour filtrer certains éléments,</li>
							<li style="margin:.5em 10% .5em 0" id="info_reload">Cliquez sur « REINITIALISER » (ou sur <img class="lazyload" data-src="icon/redo-alt.svg"></img>) pour recharger le graphe dans le même état que lors de son chargement.</li>
							<li style="margin:.5em 10% .5em 0" id="info_clustering">Plusieurs options permettent de regrouper les cercles en un cercle unique bordé de blanc en fonction de leur parenté :
								<ul class="mb-0">
									<li style="margin:.5em 10% .5em 0">Cliquez sur « TOUT GROUPER » (ou sur <img class="lazyload" data-src="icon/object-group.svg"></img>) pour regrouper iterativement tous les cercles,</li>
									<li style="margin:.5em 10% .5em 0">Cliquez sur « TOUT OUVRIR » (ou sur <img class="lazyload" data-src="icon/object-ungroup.svg"></img>) pour dégrouper iterativement tous les cercles,</li>
									<li style="margin:.5em 10% .5em 0">Faites un clic droit sur un cercle pour choisir d'en faire un groupe avec ses descendants ( <img class="lazyload" data-src="icon/highlight_downdency_white.svg"></img> ) ou de le regrouper avec son parents ( <img class="lazyload" data-src="icon/highlight_ascendency_white.svg"></img> ).</li>
								</ul>
							</li>
							<li style="margin:.5em 10% .5em 0">Cliquez sur les cercles aux bords épais pour déployer de nouveaux cercles,</li>
							<li style="margin:.5em 10% .5em 0">Double-cliquez n'importe où dans le canevas pour recentrer le graphe sur la sélection,</li>
							<li style="margin:.5em 10% .5em 0">
								Les boutons en haut à gauche permettent respectivement de :
								<ul class="mb-0">
									<li id="toggle_fullscreen_LI" style="margin:.5em 10% .5em 0">mettre en plein écran : <img class="lazyload" data-src="icon/arrows-alt.svg"></img></li>
									<li id="toggle_graphs_LI" style="margin:.5em 10% .5em 0">revenir à la sélection des graphes : <img class="lazyload" data-src="icon/appli-graph.svg"></img></li>
									<li id="toggle_lateralScreen_LI" style="margin:.5em 10% .5em 0">afficher un volet latéral pour visualiser les informations des cercles : <img class="lazyload" data-src="icon/eye.svg"></img></li>
									<li id="toggle_map_LI" style="margin:.5em 10% .5em 0">passer en mode cartographique : <img class="lazyload" data-src="icon/map.svg"></img></li>
									<li id="toggle_legend_LI" style="margin:.5em 10% .5em 0">afficher la légende des éléments du graphe : <img class="lazyload" data-src="icon/list-ul.svg"></img></li>
									<li id="toggle_tags_LI" style="margin:.5em 10% .5em 0">afficher ou ne plus afficher les étiquettes du graphe : <img class="lazyload" data-src="icon/tag.svg"></img></li>
									<li id="toggle_stabilization_LI" style="margin:.5em 10% .5em 0">recentrer le graphe sur la sélection : <img class="lazyload" data-src="icon/map-marker-alt.svg"></img></li>
									<li id="toggle_physics_LI" style="margin:.5em 10% .5em 0">activer et désactiver la physique du graphe (sauf si mode de visualisation hiérarchique) : <img class="lazyload" data-src="icon/shapes.svg"></img></li>
									<li id="toggle_fcose_LI" style="margin:.5em 10% .5em 0">réorganiser le graphe : <img class="lazyload" data-src="icon/fcose.svg"></img></li>
									<li id="toggle_ori_pos_LI" style="margin:.5em 10% .5em 0">retrouver les positions initiales : <img class="lazyload" data-src="icon/ori_pos.svg"></img></li>
									<li id="toggle_ind_link_LI" style="margin:.5em 10% .5em 0">afficher ou masquer les liens indirects : <img class="lazyload" data-src="icon/ind_link.svg"></img></li>
									<li id="toggle_highlighter_LI" style="margin:.5em 10% .5em 0">mettre en avant des éléments (plusieurs modalités) : <img class="lazyload" data-src="icon/highlight_self.svg"></img></li>
									<li id="toggle_modeEditor_LI" style="margin:.5em 10% .5em 0">activer le mode édition : <img class="lazyload" data-src="icon/pencil-alt.svg"></img></li>
									<li id="toggle_summary_LI" style="margin:.5em 10% .5em 0">afficher un résumé des informations du graphe : <img class="lazyload" data-src="icon/align-left.svg"></img></li>
									<li id="toggle_save_LI" style="margin:.5em 10% .5em 0">sauvegarder dans un fichier .doc les informations du graphe : <img class="lazyload" data-src="icon/file-alt.svg"></img></li>
									<li id="toggle_information_LI" style="margin:.5em 10% .5em 0">afficher la fenêtre d'information : <img class="lazyload" data-src="icon/information-alt.svg"></img></li>
									<li id="toggle_bug_LI" style="margin:.5em 10% .5em 0">faire remonter un bug au développeur : <img class="lazyload" data-src="icon/bug.svg"></img></li>
								</ul>
							</li>
						</ul>
						<div class="modal-hr"><hr class="my-6"></div>
						<div class="container pt-4 pb-4">
						  <p style="text-align:center;">
								<!-- Logos -->
								<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank"><img class="licences_img" alt="Licence Creative Commons BY SA 4.0" src="img/creative_commons_logo.webp" /></a>
								<a href="https://lsc.encommuns.org/" target="_blank"><img class="licences_img" alt="Legal Service For Commons, soutenir juridiquement les communs" src="img/logo_lsc.webp" /></a>
								<a rel="license" href="http://www.gnu.org/licenses/agpl-3.0.fr.html" target="_blank"><img class="licences_img" alt="Licence GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007" src="img/agpl_v3_logo.webp" /></a>
								<hr class="my-6" style="border-width:0">
								<!-- Licence Creative Commons -->
								Les contenus graphiques de <a xmlns:cc="https://creativecommons.org/ns#" href="https://grezi.fr" property="cc:attributionName" rel="cc:attributionURL" target="_blank">GREZI</a> sont mis à disposition selon les termes de la <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">licence Creative Commons Attribution BY SA 4.0 International</a>.
								<hr class="my-6" style="border-width:0">
								<!-- LSC -->
								Le nom <a xmlns:cc="https://creativecommons.org/ns#" href="https://grezi.fr" property="cc:attributionName" rel="cc:attributionURL" target="_blank">GREZI</a> est protégé par la <a rel="license" href="https://lsc.encommuns.org/" target="_blank">LSC</a> le plaçant au rang de <a href="https://wiki.lafabriquedesmobilites.fr/wiki/Communs/Qu%27est_ce_qu%27un_commun_%3F" target="_blank">commun</a>.
								<hr class="my-6" style="border-width:0">								
								<!-- Licence AGPL -->
								Le code informatique de <a xmlns:cc="https://creativecommons.org/ns#" href="https://grezi.fr" property="cc:attributionName" rel="cc:attributionURL" target="_blank">GREZI</a> est protégé par la licence <a href="http://www.gnu.org/licenses/agpl-3.0.fr.html" target="_blank">GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007</a>.
								<hr class="my-6" style="border-width:0">
								<!-- Licence Data -->
								Les données visualisées dans les graphes peuvent être issues de sources différentes. Référez-vous à la fenêtre "informations" du graphe visualisé pour obtenir les sources des données mobilisées.
								<hr class="my-6" style="border-width:0">___CUSTOM_TEXT_information_modal___
								<!-- Low-tech message -->
								A chaque consultation de cette page vous téléchargez potentiellement à nouveau un certain nombre de fichiers. Afin de limiter le trafic internet, vous pouvez trouver le code source <a href="https://gitlab.com/EcoRhizo/appligraph" target="_blank">ici</a>. A noter qu'une version low-tech du site est en cours de développement.
								<hr class="my-6" style="border-width:0">
								<!-- Open Source message -->
								Ce site est construit sur les technologies html5, js et css à l'aide des librairies Open Source suivantes :
								</p>
								<div class="row align-items-center no-gutters">
									<div class="col-lg-6">
										<ul class="mb-0">
											<li style="margin:.5em 10% .5em 0">Bootstrap</li>
											<li style="margin:.5em 10% .5em 0">Jquery</li>
											<li style="margin:.5em 10% .5em 0">Cytoscape (et plugins associés)</li>
											<li style="margin:.5em 10% .5em 0">Introjs</li>
											<li style="margin:.5em 10% .5em 0">Lazysizes</li>
										</ul>
									</div>
									<div class="col-lg-6">
										<ul class="mb-0">
											<li style="margin:.5em 10% .5em 0">Typedjs</li>
											<li style="margin:.5em 10% .5em 0">Font Awesome</li>
											<li style="margin:.5em 10% .5em 0">Autocompletejs</li>
											<li style="margin:.5em 10% .5em 0">Lodash</li>
											<li style="margin:.5em 10% .5em 0">Howler</li>
										</ul>
									</div>
								</div>
						</div>
					</div>
				</div>
				<div class="modal-quit">
					<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Quitter</button>
				</div>
			</div>
			</div>
		</div>

		<!-- Modal screen SUMMARY -->
		<div class="modal fade" id="summaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<a class="logo_title" href="https://grezi.fr" target="_blank"><img class="lazyload" height="40px" data-src="img/grezi.webp"></img></a>
					<h4 class="modal-title" id="myModalLabel">Résumé des informations présentes sur le graphe</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div id="summaryModal_body"></div>
				</div>
				<div class="modal-quit">
					<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Quitter</button>
				</div>
			</div>
			</div>	
		</div>
		
		<!-- Modal screen SELECTION -->
		<div class="modal fade" id="selectionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<a class="logo_title" href="https://grezi.fr" target="_blank"><img class="lazyload" height="40px" data-src="img/grezi.webp"></img></a>
					<h4 class="modal-title" id="myModalLabel"><div id="selectionModal_header"></div></h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div id="selectionModal_body"></div>
				</div>
				<div class="modal-quit">
					<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Quitter</button>
				</div>
			</div>
			</div>	
		</div>
		
		<!-- Modal screen BUG REPORT -->
		<div class="modal fade" id="bugModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<a class="logo_title" href="https://grezi.fr" target="_blank"><img class="lazyload" height="40px" data-src="img/grezi.webp"></img></a>
					<h4 class="modal-title" id="myModalLabel">Rapport de l'application</h4>
				</div>
				<div class="modal-body">
					<div id="bug_report_appli"></div>
					___REWRITE_BUG_MODAL_01___
					___CUSTOM_TEXT_bug_modal___
					___REWRITE_BUG_MODAL_02___
					<div id="form_bug_report_IFRAME"><iframe name="form_bug_report_MAIL"></iframe></div>
				</div>
				<div class="modal-quit">
					<button id="bugModal_QuitButton" type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Quitter</button>
				</div>
			</div>
			</div>	
		</div>
		
		<!-- Modal screen LEGEND -->
		<div class="modal fade" id="legendModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<a class="logo_title" href="https://grezi.fr" target="_blank"><img class="lazyload" height="40px" data-src="img/grezi.webp"></img></a>
					<h4 class="modal-title" id="myModalLabel">Légende</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div id="legendModal_body_CUSTOM"></div>
					<div id="legendModal_body_TAGS"></div>
				</div>
				<div class="modal-quit">
					<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Quitter</button>
				</div>
			</div>
			</div>	
		</div>
		
		<!-- Modal screen DATA_UPLOAD -->
		<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<a class="logo_title" href="https://grezi.fr" target="_blank"><img class="lazyload" height="40px" data-src="img/grezi.webp"></img></a>
					<h4 class="modal-title" id="myModalLabel">Chargement de données</h4>
				</div>
				<div class="modal-body">
					<div id="uploadFile" style="display:none">
						<p>
							<select id="select_data_type" class="selectpicker" title="Type de données" data-width="100%" data-style="btn-selectpicker" form="form_data_type" name="select_data_type_FORM[]">
								<!-- <option value="json_advanced">Fichier .json (format GREZI avancé)</option> -->
								<!-- <option value="json_simple">Fichier .json (format GREZI simple)</option> -->
								<!-- <option value="json">Fichier .json</option> -->
								<option value="csv">Tableur .csv</option>
								<!-- <option value="php">Requête php (stockée dans un .txt) (format GREZI avancé)</option> -->
							</select>
						</p>
						<p id="upload_information" style="display:none"></p>
						<p id="upload_specs" style="display:none"></p>
						<div id ="upload_file" style="display:none;">
							<p><b>Éléments : </b><input id="uploadFileNodes" type="file" accept=".json, .csv, .txt"></p>
							<p><b>Liens <i>(optionnel)</i> : </b><input id="uploadFileEdges" type="file" accept=".json, .csv, .txt"></p>
						</div>
					</div>
					<div id="passwordFile" style="display:none">
						<p>
							<form id="form_password_file" onsubmit="return false;">
								<div class="form-group">
									<input type="password" id="form_password_file_input" class="md-textarea form-control" placeholder="Saisissez votre mot de passe"></input>
								</div>
							</form>
						</p>
						<p id="password_information" style="display:none"></p>
						<br><br>
						<p id="password_specs" style="display:none"></p>
						<br><br>
					</div>
					<div id="semappsFile" style="display:none">
						<p>
							<form id="form_semapps_file" onsubmit="return false;">
								<div class="form-group">
									<input id="form_semapps_file_input" class="md-textarea form-control" placeholder="Saisissez l'adresse url d'un SemApps"></input>
								</div>
							</form>
						</p>
						<p id="semapps_information" style="display:none"></p>
						<br><br>
						<p id="semapps_specs" style="display:none"></p>
						<br><br>
					</div>
				</div>
				<div class="modal-quit">
					<button class="btn btn-primary mb-1" id="upload_button_submit" style="display:none">Charger le fichier de données</button>
					<button class="btn btn-primary mb-1" id="password_button_submit" style="display:none">Confirmer</button>
					<button class="btn btn-primary mb-1" id="semapps_button_submit" style="display:none">Confirmer</button>
				</div>
			</div>
			</div>	
		</div>
		
		<!-- Modal screen OTHER -->
		<div class="modal fade" id="otherModal" tabindex="-1" role="dialog" aria-labelledby="otherModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<a class="logo_title" href="https://grezi.fr" target="_blank"><img class="lazyload" height="40px" data-src="img/grezi.webp"></img></a>
					<h4 class="modal-title" id="otherModalLabel">Divers</h4>
				</div>
				<div class="modal-body">
					<div id="otherModal_body"></div>
				</div>
				<div class="modal-quit">
					<button id="otherModal_QuitButton" type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Quitter</button>
				</div>
			</div>
			</div>	
		</div>
		
	</div>
	
	<div id="appli_graph_mask"><h3>Cliquer pour commencer</h3></div>

  <!-- Jquery core JavaScript -->
  <script src="dist/jquery/3.4.1/js/jquery.min.js"></script>
  
  <!-- Bootstrap core JavaScript -->
  <script src="dist/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
  
  <!-- Bootstrap plugin JavaScript -->
  <script src="dist/bootstrap-select/1.13.9/js/bootstrap-select.js"></script>

  <!-- Lazysizes core JavaScript -->
  <script src="dist/lazysizes/5.2.0/lazysizes.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/___main_graph.js?___VERSION_CACHE___"></script>

  <!-- Matomo Code [OPTIONAL] -->
  ___REWRITE_MATOMO_CODE___
  <!-- End Matomo Code -->


</body>

</html>
