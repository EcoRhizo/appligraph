// export default class Ouiz {

// 	constructor() {
// 		this.reset();
// 	};

// 	async reset() {
// 		this.doc = {};
// 		this.graphs = [];
// 		this.network = {nodes: [], edges: []};
// 	};

// 	async load(url) {
// 		let documentLoaderType = 'xhr';
// 		await jsonld.useDocumentLoader(documentLoaderType/*, options*/);
// 		this.doc = await jsonld.documentLoader(url, function(err) {if(err) {alert(err);};});
// 		this.doc.jsonld = JSON.parse(this.doc.document);
// 		delete this.doc.document;
// 		await this.ldpToGraph(this.doc);
// 		/* console.log(this); */
// 		return this;
// 	};

// 	async ldpToGraph(doc) {
// 		let graph;
// 		if (Array.isArray(doc.jsonld.nodes) && Array.isArray(doc.jsonld.edges) && doc.jsonld.nodes.length > 0) {
// 			graph = doc.jsonld;
// 		} else if (Array.isArray(doc.jsonld["ldp:contains"]) && doc.jsonld["ldp:contains"].length > 0) {
// 			graph = await this.pairToGraph(doc);
// 		}else{
// 			graph = await this.oneItemToGraph(doc);
// 		};
// 		this.graphs.push(graph);
// 	};

// 	async pairToGraph(doc) {
// 		let graph = {nodes: [], edges: []};
// 		let items = doc.jsonld;["ldp:contains"];
// 		graph.nodes = items
// 		return graph;
// 	};

// 	async oneItemToGraph(doc) {
// 		let graph = {nodes: [], edges: []};
// 		let item = doc.jsonld;
// 		graph.nodes.push(item);
// 		return graph;
// 	};

// 	async toCytoscape(modelist,white_or_black_list) {
// 		this.white_or_black_list = white_or_black_list || [];
// 		/* console.log(this.graphs, this.network); */
// 		for await (let g of this.graphs){
// 			/* console.log("G",g); */
// 			this.network.nodes.push(g.nodes);
// 			this.network.edges.push(g.edges);
// 		};
// 		for await (let n of this.network.nodes) {
// 			if (n.built == undefined) {
// 				await this.buildEdges(n,modelist);
// 			};
// 		};
// 		return this;
// 	};

export default class Ouiz {
  constructor() {
    this.reset()
  }

  async reset(){
    this.doc = {}
    this.graphs = []
    this.network = {nodes: [], edges: []}
  }

  async load(url){
    let documentLoaderType = 'xhr'
    await jsonld.useDocumentLoader(documentLoaderType/*, options*/);
    this.doc = await jsonld.documentLoader(url, function(err) {
      if(err) {
        alert(err)
      }
    })
    this.doc.jsonld = JSON.parse(this.doc.document)
    delete this.doc.document
    await this.ldpToGraph(this.doc)
    /* console.log(this) */
    return this
  }

  async ldpToGraph(doc){
    let graph

    if (Array.isArray(doc.jsonld.nodes) && Array.isArray(doc.jsonld.edges) && doc.jsonld.nodes.length > 0){
      graph = doc.jsonld

    }else if (Array.isArray(doc.jsonld["ldp:contains"]) && doc.jsonld["ldp:contains"].length > 0){
      graph = await this.pairToGraph(doc)
    }else{
      graph = await this.oneItemToGraph(doc)
    }
    this.graphs.push(graph)
    /* console.log(this) */

  }

  async pairToGraph(doc){
    let graph = {nodes: [], edges: []}
    let items = doc.jsonld["ldp:contains"]
    graph.nodes = items
    return graph
  }

  async oneItemToGraph(doc){
    let graph = {nodes: [], edges: []}
    let item = doc.jsonld
    graph.nodes.push(item)
    return graph
  }

  async toCytoscape(modelist,white_or_black_list,traductor){
    this.white_or_black_list = white_or_black_list || []
    /* console.log(this.graphs, this.network) */
    for await (let g of this.graphs){
      /* console.log("G",g) */
      Array.prototype.push.apply(this.network.nodes, g.nodes);
      Array.prototype.push.apply(this.network.edges, g.edges);
    }
    for await (let n of this.network.nodes) {
      if(n.built == undefined){
        await this.buildEdges(n,modelist,traductor)
      }
    }
    return this
  }


	async buildEdges(n,modelist,traductor){
		for (const [k, v] of Object.entries(n)) {
			await this.parse(n,k,v,modelist,traductor)
		};
		n.built = true;
	};

	async parse(n, k, v, modelist, traductor){

		if (typeof v == "string") {

			v = v.trim();

			let edgeLength = undefined;

			let ifTest;
			if (modelist === "whitelist") {ifTest = this.white_or_black_list.includes(k);};
			if (modelist === "blacklist") {ifTest = !this.white_or_black_list.includes(k);};
			if(ifTest && v.length > 0){

				var indexO = this.network.nodes.findIndex(x => x.id==v);

				if(indexO === -1){

					let ob =   {id: v, additionalData: {}, style: {}, mass: 1}
					if (v.length > 20 ){
						ob.label = v.substring(0,20)+"..";
						ob.title = v;
					} else {
						ob.label = v;
					};

					if (v.startsWith('http')){
						ob.style.backgroundColor = "#7FD1B9";
						if (v.length > 50 ){
							let lab = v.endsWith('/') ? v.slice(0, -1) : v;
							ob.label = lab.substr(lab.lastIndexOf('/') + 1);
							ob.label = ob.label.length > 20 ? ob.label.substring(0,20)+".." : ob.label;
							ob.label = "->"+ob.label;
							ob.title = v;
						} else {
							ob.label = v;
						};
					} else {
						ob.property = true;
					};

					ob.built = true;
					
					if(ob.property === true) {
						if(this.network.nodes.some(e => (e.id === n.id))) {
							let nodeTemp = this.network.nodes.find(e => e.id === n.id);
							if (nodeTemp.additionalData !== undefined) {nodeTemp.additionalData[k] = v;};
						};
					} else {
						this.network.nodes.push(ob);
					};

				} else {
					this.network.nodes[indexO].mass == undefined ? this.network.nodes[indexO].mass=1 : this.network.nodes[indexO].mass+=3;
				};

				let o = this.network.nodes.find(n => n.id == v);
				if(o !== undefined) {
					o.mass+= 3;
					edgeLength = o.mass * 20;
					let edge = {from: n.id, to: o.id, data: {label: k}};
					if (edgeLength != undefined){
						edge.data.edgeLength = edgeLength;
					}
					this.network.edges.push(edge);
				};

			} else {

				n.additionalData = {};
				Object.entries(n).forEach(([key, value]) => {
					if (key !== "additionalData") {
						if (traductor !== undefined) {
							if (traductor[key] !== undefined) {
								n[traductor[key]] = value;
							} else {
								n.additionalData[key] = value;
							};
						} else {
							n.additionalData[key] = value;
						};
					};
				});

			};

		} else if (Array.isArray(v)) {

			v.forEach((_v) => {
				this.parse(n,k,_v,modelist);
			});

		} else {

			let ifTest;
			if (modelist === "whitelist") {ifTest = this.white_or_black_list.includes(k);};
			if (modelist === "blacklist") {ifTest = !this.white_or_black_list.includes(k);};
			if(ifTest && typeof v == "object") {

				var indexOBJ = this.network.nodes.findIndex(x => x.id==v.id);
				if(indexOBJ === -1){
					/* console.log("ADDING",n.id, typeof v,k, v) */
					this.network.nodes.push(v);
					/* console.log("ADDING edge",n.id, k, v.id) */
					this.network.edges.push({from: n.id, to: v.id, data: {label: k}});
				};

			} else if(ifTest && typeof v == "number") {

				/* console.log("TODO",n.id, typeof v,k, v) */

			} else {

				/* console.log("TODO",n.id, typeof v,k, v) */

			};

		};

	};

};
