# GREZI / Readme

<img src="https://img.shields.io/liberapay/patrons/EcoRhizo.svg?logo=liberapay">


**THIS VERSION IS NOW OBSOLETE AND WILL NOT BE MAINTANED. PLEASE REFER TO THE NEW GREZI GIT HERE :**
```
https://gitlab.com/grezi/grezi-app
```

## Introduction

This git corresponds to « GREZI » a numeric common project available at https://app.grezi.fr

For more details : https://grezi.fr

Datas must be placed in in the folder **« \_TEMPLATES »**.

Templates are in **« template »** folder.

Documentation will be deploy step by step, function of the needs of the users. Lots of comments are in the scripts themselves as the application is at its beginnings.

The codes available in this repository are under the **GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007** (see the LICENCE file).

All the other contents (.txt, .png, .svg...) are available under **Creative Commons Attribution BY SA 4.0 International Public License** ( https://creativecommons.org/licenses/by-sa/4.0/legalcode ).

The name « GREZI » is protected by the **Legal Service For Commonsm** association ( https://lsc.encommuns.org/ ).

Some dependencies of this repository are used under similar open source licence. It concerns the following items :
- Bootstrap,
- Jquery,
- Cytoscape (and associated plugins),
- Introjs,
- Lazysizes,
- Typedjs,
- Font Awesome,
- Mapbox-gl,
- OpenStreetMap,
- Autocompletejs,
- Lodash,
- Howler.

More dependencies can be used in specific templates.

## Basic usage

This application is designed to be used as an independant package.

Just download the repository and open the index.php with a compatible navigator such as Mozilla Firefox. You will need a php server generator such as Wamp or Laragon.

By opening this .php, default graphs will be loaded. You can modify the list by modify the variable **defaultIDS** in  the script **custom_graph.js** (in the **js** folder).

You can add url parameters to control some options :
* debug : show a list of all accessible graphs (only when deploy on a servor),
* ids : automatically update the **defaultIDS** variable,
* directId : directly open the graph,
* passwd : submit a password to the **\_password_data.php** script,
* eulerComputation : if the physics of the graph is activated and positions are not set, it will perform a euler layout befor the cola layout (computational intense),
* showInformation : force to enable or disable the app tutorial on launch,
* clickToBegin : show a click to begin screen before loading the graph, usefull when the app is implemented in an iframe,
* blank : use the app in a blank mark mode
* toggle_XXX : force to enable or disable a functionnality (see the **config.js** script configuration in the template).

_N.B : For ids and directId, the names to set are related to the folders containing the graphs configuration placed in the **« \_TEMPLATES »** folder._

## Advanced usage

In order to use some feature, changes must be done in several scripts :
* In **\_\_\_main_graph.php** script, the variable **defaultIDS** can be modified, to show different templates in the welcome modal screen, as follow :
    > 
    > var defaultIDS = "\_\_\_TEMPLATE_FOLDER_NAME\_\_\_,\_\_\_TEMPLATE_FOLDER_NAME\_\_\_,\_\_\_TEMPLATE_FOLDER_NAME\_\_\_";
    > 
* In **\_password_data.php** script, the variable **$passwd_dict** must be completed as an array, to retrive data needed by templates, as follow :
    > 
    >   $passwd_dict = array (
    >     "\_\_\_PASSWORD\_\_\_" => array("\_\_\_TEMPLATE_FOLDER_NAME\_\_\_","\_\_\_URL\_\_\_")
    >   );
    > 
* In **\_mod_map_mode.js** script, you must set the Mapbox token if you want to use this feature. The token has to be set in the **tokenMapbox** variable.


## Graphs configuration

There is two ways to create a graph :
* By using the editor mode functionnality (that must be activated),
* Duplicate the **« template »** folder in **« \_TEMPLATES »**.

### Editor mode

Here are the different possibilities in the editor mode :

* Main functionnalities :
    * Save the current graph (an advanced mode is here to save the data in order to be directly used in the **config.js** script),
    * Load a graph,
    * Erase the selection,
    * Restore the last suppression,
    * Set aside the selection,
* Create / modify an element :
    * Set its label,
    * Set its tooltip description,
    * Set its modal screen description,
* Create an new link (by drag and drop),
* Advanced functionnalities :
    * See the detailed json of a node and modify it,
    * Get the positions of all the elements (can be directly used in the **config.js** script),
    * Some precise positions modificators.

### Graph configuration structuration

The **« template »** folder is composed by :

* **config.js** : script containing all the options and data of a graph (MANDATORY),
* **title.js** : script containing informations shown in the graph list of the app (OPTIONAL),
* **js** folder : contains custom scripts, espacially the script **data_typing.js** (that control the animations) (OPTIONAL),
* **img** folder : contains the card shown in the graph list of the app and the different images used in the graph (OPTIONAL).

The **config.js** is the main config file of a template. It contains a very important variable at the end of the file : **dataGraph_GET**. This variable can be :
* A GREZI formated nodes description : a list (array) of nodes each describe by a dictionnary (object). See the **config.js** file in the **template_ex** folder for more details,
* A string containing a url of a php file that sending a GREZI formated nodes list description,
* A dictionnary of different format (only "csv" is working for now). For each format, there is a list of files,
A dictionnary of a unique element with **\_\_\_CUSTOM_TEMPLATES\_\_\_** for the key and a config file in the element (that can be empty). It loads an upload file menu when GREZI loads the template,
* A string equal to **\_\_\_PASSWORD_TEMPLATES\_\_\_** that load the password file menu when GREZI loads the template.

### Working templates

To help you to create new templates, you can find some basic examples in **\_TEMPLATES** folder :
* **template_ex** : an empty template with descriptions of each possible options,
* **template_void** : an empty template made to be duplicated to make new ones,
* **template_void_absolute** : an empty template made to be duplicated to make new ones without any variable set in the config.js file and no other optional files,
* **template_csv** : a template that load a csv file,
* **template_upload** : a template with the **\_\_\_CUSTOM_TEMPLATES\_\_\_** configuration,
* **template_password** : a template with the **\_\_\_PASSWORD_TEMPLATES\_\_\_** configuration.

